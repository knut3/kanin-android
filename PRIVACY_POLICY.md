## Rabbit: Privacy policy

Welcome to the Rabbit card game app for Android!

This is an open source Android app developed by Knut Børte Liestøl. The source code is freely available on BitBucket; the app is also available on Google Play.

I am quite sure privacy will not be a concern in this app. I do not, and do not intend to gather any user information other than authentication data and statistics about the game.

Usernames and passords are handled according to good standards, using a secure connection and not storing raw passwords in the database.

### Explanation of permissions requested in the app

The list of permissions required by the app can be found in the `AndroidManifest.xml` file.

<br/>

| Permission | Why it is required |
| :---: | --- |
| `android.permission.ACCESS_NETWORK_STATE` | This is required to detect when you lose internet connection and when you reconnect. |
| `android.permission.INTERNET` | Required to contact the server, which in turn connects you with your game opponents. |

 <hr style="border:1px solid gray">

If you find any security vulnerability that has been inadvertently caused by me, or have any question regarding how the app protectes your privacy, please send me an email, and I will surely try to fix it/help you.

Yours sincerely,  
Knut Børte Liestøl
Asker, Norway.  
knut3punkt@gmail.com