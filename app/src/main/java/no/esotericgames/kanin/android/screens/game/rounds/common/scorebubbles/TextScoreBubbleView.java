package no.esotericgames.kanin.android.screens.game.rounds.common.scorebubbles;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.screens.common.ResourceManager;
import no.esotericgames.kanin.android.screens.game.rounds.common.scorebubbles.ScoreBubbleView;

public class TextScoreBubbleView extends ScoreBubbleView {

    private final String text;
    private final Paint textPaint;
    private float textExactVerticalCenter;

    public TextScoreBubbleView(int score, String text, ResourceManager resourceManager) {
        super(score, resourceManager, true);
        this.text = text;

        int textColor;
        if (score < 0){
            textColor = resourceManager.getColor(R.color.negative_score_text);
        }
        else{
            textColor = resourceManager.getColor(R.color.positive_score_text);
        }

        textPaint = new Paint();
        textPaint.setColor(textColor);
        textPaint.setAntiAlias(true);
        textPaint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    protected void setContentAlpha(int alpha) {
        textPaint.setAlpha(alpha);
    }

    @Override
    protected void setContentSize(PointF bubbleSize) {
        textPaint.setTextSize(bubbleSize.x / text.length());
        Rect textBounds = new Rect();
        textPaint.getTextBounds(this.text,0, this.text.length(), textBounds);
        textExactVerticalCenter = textBounds.exactCenterY();
    }

    @Override
    protected void setContentPosition(PointF bubbleCenter) {

    }

    @Override
    protected void drawContent(Canvas canvas) {
        canvas.drawText(text, centerPosition.x, centerPosition.y-textExactVerticalCenter, textPaint);
    }
}
