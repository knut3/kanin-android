package no.esotericgames.kanin.android.screens.common.canvasinterfaces;

import android.graphics.Canvas;

public interface IDrawable {
    void draw(Canvas canvas);
}
