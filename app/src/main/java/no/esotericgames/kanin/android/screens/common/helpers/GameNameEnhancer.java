package no.esotericgames.kanin.android.screens.common.helpers;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.screens.common.ResourceManager;

public class GameNameEnhancer {
    private final ResourceManager resourceManager;

    public GameNameEnhancer(ResourceManager resourceManager) {
        this.resourceManager = resourceManager;
    }

    public String getEnhanced(String gameName){
        String lastChar = gameName.substring(gameName.length()-1);
        if (lastChar.equals("s") || lastChar.equals("x") || lastChar.equals("z")){
            return resourceManager.getString(R.string.game_name_suffix_username_ending_with_s, gameName);
        }
        else return resourceManager.getString(R.string.game_name_suffix_general_case, gameName);
    }
}
