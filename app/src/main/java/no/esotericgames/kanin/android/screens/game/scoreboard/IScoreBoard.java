package no.esotericgames.kanin.android.screens.game.scoreboard;

public interface IScoreBoard {
    void addScore(int score);
    void addRoundScore(int score);
    void addTotalScore(int score);
    int getRoundScore();
}
