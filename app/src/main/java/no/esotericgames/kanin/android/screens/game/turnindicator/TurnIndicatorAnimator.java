package no.esotericgames.kanin.android.screens.game.turnindicator;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.animation.LinearInterpolator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import no.esotericgames.kanin.android.models.Player;
import no.esotericgames.kanin.android.screens.game.scoreboard.ScoreBoardInfo;

public class TurnIndicatorAnimator {
    private enum SegmentType { STRAIGHT, CURVED }
    private static float DURATION_MULTIPLIER = 1.5f;
    private final View drawingView;
    private final float density;
    private TurnIndicatorView turnIndicator;
    private Map<String, ScoreBoardInfo.Seat> activeSeats;
    private ScoreBoardInfo.Seat currentSeat;
    private AnimatorSet previousAnimation;
    private float tableSideArchLengthOneDegree;
    private final TurnIndicatorView.TableEdge left = TurnIndicatorView.TableEdge.LEFT;
    private final TurnIndicatorView.TableEdge bottom = TurnIndicatorView.TableEdge.BOTTOM;
    private final TurnIndicatorView.TableEdge right = TurnIndicatorView.TableEdge.RIGHT;
    private final TurnIndicatorView.TableEdge top = TurnIndicatorView.TableEdge.TOP;

    public TurnIndicatorAnimator(View drawingView, float density) {
        this.drawingView = drawingView;
        this.density = density;
    }

    public void initialize(TurnIndicatorView turnIndicator, Map<String, ScoreBoardInfo.Seat> activeOpponentSeats, Player me, String firstTurnUsername){
        this.turnIndicator = turnIndicator;
        activeSeats = activeOpponentSeats;
        activeSeats.put(me.getUsername(), ScoreBoardInfo.Seat.SOUTH);
        currentSeat = activeSeats.get(firstTurnUsername);
        setInitialIndicatorPosition(currentSeat);
        tableSideArchLengthOneDegree = turnIndicator.getTableSideArchLengthOneDegree();
    }

    public void moveTo(String username, int delay_ms, Runnable actionOnEnd){
        if (previousAnimation != null && previousAnimation.isRunning()){
            previousAnimation.end();
            // speed up turn indicator
            DURATION_MULTIPLIER -= (DURATION_MULTIPLIER / 4);
        }

        List<Animator> animators = new ArrayList<>(3);
        ScoreBoardInfo.Seat endSeat = activeSeats.get(username);
        if (endSeat == currentSeat){
            return;
        }
        List<Segment> journey = getJourney(currentSeat, endSeat);

        Segment first = journey.get(0);
        if (first.type == SegmentType.STRAIGHT)
            first.startValue = turnIndicator.getStraightEdgePosition();
        else if (first.type == SegmentType.CURVED && first.edge == turnIndicator.getCurrentEdge()) {
            first.startValue = turnIndicator.getSideAngle();
            first.endValue = first.startValue + first.endValue;
        }

        for (int i = 0; i < journey.size(); i++){
            Segment segment = journey.get(i);

            ValueAnimator animator;

            if (segment.type == SegmentType.STRAIGHT){
                float segmentLength = turnIndicator.getStraightEdgeLength(segment.startValue, segment.endValue);
                int duration = getSegmentDuration(segmentLength);
                animator = getStraightEdgeAnimator(segment.edge, segment.startValue, segment.endValue, duration);
            }
            else{
                float segmentLength = tableSideArchLengthOneDegree * Math.abs(segment.endValue - segment.startValue);
                int duration = getSegmentDuration(segmentLength);
                animator = getSideAngleAnimator(segment.edge, segment.startValue, segment.endValue, duration);
            }

            animators.add(animator);
        }

        AnimatorSet set = new AnimatorSet();
        set.setInterpolator(new LinearInterpolator());
        set.setStartDelay(delay_ms);
        set.playSequentially(animators);
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                currentSeat = endSeat;
                if (actionOnEnd != null)
                    actionOnEnd.run();
            }
        });
        set.start();
        previousAnimation = set;
    }

    private void setInitialIndicatorPosition(ScoreBoardInfo.Seat seat){
        switch (seat){
            case WEST:
                turnIndicator.setStraightEdgePosition(0, bottom);
                turnIndicator.setSideAngle(getCurveSideAngle(bottom, left), left);
                break;
            case EAST:
                turnIndicator.setStraightEdgePosition(100, bottom);
                turnIndicator.setSideAngle(getCurveSideAngle(bottom, right), right);
                break;
            case SOUTH:
                turnIndicator.setStraightEdgePosition(getStraightEdgePercentage(seat), bottom);
                break;
            default:
                turnIndicator.setStraightEdgePosition(getStraightEdgePercentage(seat), top);
                break;
        }
    }

    private int getSegmentDuration(float segmentLength){
        return (int)(segmentLength / density * DURATION_MULTIPLIER);
    }

    private TurnIndicatorView.TableEdge getEdge(ScoreBoardInfo.Seat seat){
        TurnIndicatorView.TableEdge tableEdge = null;
        switch (seat){
            case SOUTH:
                tableEdge = bottom;
                break;
            case WEST:
                tableEdge = left;
                break;
            case NORTH_WEST:
            case NORTH:
            case NORTH_EAST:
                tableEdge = top;
                break;
            case EAST:
                tableEdge = right;
                break;
        }

        return tableEdge;
    }

    private float getStraightEdgePercentage(ScoreBoardInfo.Seat seat){
        float result;
        switch (seat){
            case SOUTH:
            case NORTH:
                result = 50;
                break;
            case NORTH_WEST:
                result = 25;
                break;
            case NORTH_EAST:
                result = 75;
                break;
            default:
                throw new IllegalArgumentException("Seat is not straight edge");
        }
        return result;
    }

    private float getCurveSideAngle(TurnIndicatorView.TableEdge from, TurnIndicatorView.TableEdge to){
        if (from == bottom){
            if (to == left)
                return 135;
            else if (to == right)
                return -135;
        }
        else if (from == top){
            if (to == left)
                return -45;
            else if (to == right)
                return 45;
        }
        else if (from == left){
            if (to == bottom)
                return -135;
            else if (to == top)
                return 45;
        }
        else if (from == right){
            if (to == bottom)
                return 135;
            else if (to == top)
                return -45;
        }

        throw new IllegalArgumentException("getCurveSideAngle only applies to bottom or top to east or west");
    }

    private List<Segment> getJourney(ScoreBoardInfo.Seat start, ScoreBoardInfo.Seat end){
        List<Segment> journey = new ArrayList<>(3);

        TurnIndicatorView.TableEdge startEdge = getEdge(start);
        TurnIndicatorView.TableEdge endEdge = getEdge(end);

        if (startEdge == endEdge){
            journey.add(new Segment(SegmentType.STRAIGHT, top, getStraightEdgePercentage(end)));
        }
        else if (startEdge == bottom){
            journey = getJourneyFromBottom(endEdge, end);
        }
        else if (startEdge == left){
            journey = getJourneyFromLeft(endEdge, end);
        }
        else if (startEdge == right){
            journey = getJourneyFromRight(endEdge, end);
        }
        else{
            journey = getJourneyFromTop(start, endEdge, end);
        }

        return journey;

    }

    private List<Segment> getJourneyFromBottom(TurnIndicatorView.TableEdge endSide, ScoreBoardInfo.Seat endSeat){
        List<Segment> journey = new ArrayList<>(3);

        if (endSide == left) {
            journey.add(new Segment(SegmentType.STRAIGHT, bottom,0));
            float endAngle = getCurveSideAngle(bottom, left);
            journey.add(new Segment(SegmentType.CURVED, left, endAngle));
        }
        else if (endSide == right){
            journey.add(new Segment(SegmentType.STRAIGHT, bottom,100));
            float endAngle = getCurveSideAngle(bottom, right);
            journey.add(new Segment(SegmentType.CURVED, right, endAngle));
        }
        else if (endSeat == ScoreBoardInfo.Seat.NORTH_EAST){
            journey.add(new Segment(SegmentType.STRAIGHT, bottom,100));
            journey.add(new Segment(SegmentType.CURVED, right, -180));
            journey.add(new Segment(SegmentType.STRAIGHT, top, 100, getStraightEdgePercentage(endSeat)));

        }
        else {
            journey.add(new Segment(SegmentType.STRAIGHT, bottom, 0));
            journey.add(new Segment(SegmentType.CURVED, left, 180));
            journey.add(new Segment(SegmentType.STRAIGHT, top, 0, getStraightEdgePercentage(endSeat)));
        }

        return journey;
    }

    private List<Segment> getJourneyFromLeft(TurnIndicatorView.TableEdge endSide, ScoreBoardInfo.Seat endSeat){
        List<Segment> journey = new ArrayList<>(3);


        if (endSide == bottom){
            journey.add(new Segment(SegmentType.CURVED, left, getCurveSideAngle(left, bottom)));
            journey.add(new Segment(SegmentType.STRAIGHT, bottom, 0, getStraightEdgePercentage(ScoreBoardInfo.Seat.SOUTH)));
        }
        else if (endSide == right){
            journey.add(new Segment(SegmentType.CURVED, left, getCurveSideAngle(left, top)));
            journey.add(new Segment(SegmentType.STRAIGHT, top, 0, 100));
            journey.add(new Segment(SegmentType.CURVED, right, getCurveSideAngle(top, right)));
        }
        else{
            float topPercentage = getStraightEdgePercentage(endSeat);
            journey.add(new Segment(SegmentType.CURVED, left, getCurveSideAngle(left, top)));
            journey.add(new Segment(SegmentType.STRAIGHT, top, 0, topPercentage));
        }

        return journey;
    }

    private List<Segment> getJourneyFromRight(TurnIndicatorView.TableEdge endSide, ScoreBoardInfo.Seat endSeat){
        List<Segment> journey = new ArrayList<>(3);


        if (endSide == bottom){
            journey.add(new Segment(SegmentType.CURVED, right, getCurveSideAngle(right, bottom)));
            journey.add(new Segment(SegmentType.STRAIGHT, bottom, 100, getStraightEdgePercentage(ScoreBoardInfo.Seat.SOUTH)));
        }
        else if (endSide == left){
            journey.add(new Segment(SegmentType.CURVED, right, getCurveSideAngle(right, bottom)));
            journey.add(new Segment(SegmentType.STRAIGHT, bottom, 100, 0));
            journey.add(new Segment(SegmentType.CURVED, left, getCurveSideAngle(bottom, left)));
        }
        else{
            float topPercentage = getStraightEdgePercentage(endSeat);
            journey.add(new Segment(SegmentType.CURVED, right, getCurveSideAngle(right, top)));
            journey.add(new Segment(SegmentType.STRAIGHT, top, 100, topPercentage));
        }

        return journey;
    }

    private List<Segment> getJourneyFromTop(ScoreBoardInfo.Seat startSeat, TurnIndicatorView.TableEdge endSide, ScoreBoardInfo.Seat endSeat){
        List<Segment> journey = new ArrayList<>(3);

        if (endSide == left) {
            journey.add(new Segment(SegmentType.STRAIGHT, top,0));
            float endAngle = getCurveSideAngle(top, left);
            journey.add(new Segment(SegmentType.CURVED, left, endAngle));
        }
        else if (endSide == right){
            journey.add(new Segment(SegmentType.STRAIGHT, top,100));
            float endAngle = getCurveSideAngle(top, right);
            journey.add(new Segment(SegmentType.CURVED, right, endAngle));
        }
        else if (startSeat == ScoreBoardInfo.Seat.NORTH_WEST){
            journey.add(new Segment(SegmentType.STRAIGHT, top,0));
            journey.add(new Segment(SegmentType.CURVED, left, -180));
            journey.add(new Segment(SegmentType.STRAIGHT, bottom, 0, getStraightEdgePercentage(endSeat)));

        }
        else {
            journey.add(new Segment(SegmentType.STRAIGHT, top, 100));
            journey.add(new Segment(SegmentType.CURVED, right, 180));
            journey.add(new Segment(SegmentType.STRAIGHT, bottom, 100, getStraightEdgePercentage(endSeat)));
        }

        return journey;
    }

    private ValueAnimator getStraightEdgeAnimator(TurnIndicatorView.TableEdge side,
                                                  float startPercentage, float endPercentage, int duration_ms){
        ValueAnimator positionAnimator =
                ValueAnimator.ofFloat(startPercentage, endPercentage);
        positionAnimator.setDuration(duration_ms);
        positionAnimator.addUpdateListener(animator -> {
            turnIndicator.setStraightEdgePosition((float)animator.getAnimatedValue(), side);
            drawingView.postInvalidate();
        });
        return positionAnimator;
    }

    private ValueAnimator getSideAngleAnimator(TurnIndicatorView.TableEdge edge, float startAngle, float endAngle, int duration_ms){
        ValueAnimator sideAngleAnimator =
                ValueAnimator.ofFloat(startAngle, endAngle);
        sideAngleAnimator.setDuration(duration_ms);
        sideAngleAnimator.addUpdateListener(animator -> {
            turnIndicator.setSideAngle((float)animator.getAnimatedValue(), edge);
            drawingView.postInvalidate();
        });
        return sideAngleAnimator;
    }

    private class Segment{
        SegmentType type;
        TurnIndicatorView.TableEdge edge; // used for curve
        Float startValue;
        float endValue; // rotation angle or length percentage

        public Segment(SegmentType type, TurnIndicatorView.TableEdge edge, float endValue) {
            this.type = type;
            this.edge = edge;
            this.startValue = 0f;
            this.endValue = endValue;
        }

        public Segment(SegmentType type, TurnIndicatorView.TableEdge edge, float startValue, float endValue) {
            this.type = type;
            this.edge = edge;
            this.startValue = startValue;
            this.endValue = endValue;
        }
    }
}
