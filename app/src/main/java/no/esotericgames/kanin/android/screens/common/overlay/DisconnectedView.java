package no.esotericgames.kanin.android.screens.common.overlay;

import android.content.Context;
import android.widget.LinearLayout;

import no.esotericgames.kanin.android.R;

public class DisconnectedView extends LinearLayout {

    public DisconnectedView(Context context) {
        super(context);
        inflate(context, R.layout.disconnected_view,this);

    }
}
