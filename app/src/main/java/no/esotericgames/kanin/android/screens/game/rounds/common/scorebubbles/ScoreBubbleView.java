package no.esotericgames.kanin.android.screens.game.rounds.common.scorebubbles;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IAnimatable;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IFadeable;
import no.esotericgames.kanin.android.screens.common.ResourceManager;

public abstract class ScoreBubbleView implements IDrawable, IAnimatable, IFadeable {
    protected PointF centerPosition, size;
    private float rotation = 0;
    private final Paint bgPaint, strokePaint;
    private final boolean drawStroke;


    public ScoreBubbleView(int score, ResourceManager resourceManager, boolean drawStroke) {
        this.drawStroke = drawStroke;
        int bgColor, strokeColor;
        if (score < 0){
            bgColor = resourceManager.getColor(R.color.negative_score_bg);
            strokeColor = resourceManager.getColor(R.color.negative_score_text);
        }
        else{
            bgColor = resourceManager.getColor(R.color.positive_score_bg);
            strokeColor = resourceManager.getColor(R.color.positive_score_text);
        }
        bgPaint = new Paint();
        bgPaint.setColor(bgColor);
        bgPaint.setAntiAlias(true);
        bgPaint.setStyle(Paint.Style.FILL);
        strokePaint = new Paint();
        strokePaint.setColor(strokeColor);
        strokePaint.setAntiAlias(true);
        strokePaint.setStyle(Paint.Style.STROKE);

    }

    @Override
    public void draw(Canvas canvas) {
        canvas.save();
        canvas.rotate(rotation, centerPosition.x, centerPosition.y);
        canvas.drawCircle(centerPosition.x, centerPosition.y, size.x/2, bgPaint);
        if (drawStroke)
            canvas.drawCircle(centerPosition.x, centerPosition.y, size.x/2, strokePaint);
        drawContent(canvas);
        canvas.restore();
    }

    @Override
    public void setAlpha(int alpha){
        bgPaint.setAlpha(alpha);
        strokePaint.setAlpha(alpha);
        setContentAlpha(alpha);
    }

    @Override
    public int getAlpha(){
        return bgPaint.getAlpha();
    }

    @Override
    public void setPosition(PointF center) {
        if (size == null)
            throw new RuntimeException("Size must be set before position");
        this.centerPosition = center;
        setContentPosition(center);
    }

    @Override
    public PointF getPosition() {
        return centerPosition;
    }

    @Override
    public void setSize(PointF size) {
        this.size = size;
        setContentSize(size);
    }

    @Override
    public PointF getSize() {
        return size;
    }

    @Override
    public void setRotation(float angle) {
        this.rotation = angle;
    }

    @Override
    public float getRotation() {
        return rotation;
    }

    protected abstract void setContentAlpha(int alpha);

    protected abstract void setContentSize(PointF bubbleSize);

    protected abstract void setContentPosition(PointF bubbleCenter);

    protected abstract void drawContent(Canvas canvas);
}
