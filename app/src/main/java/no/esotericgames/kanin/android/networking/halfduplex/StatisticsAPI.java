package no.esotericgames.kanin.android.networking.halfduplex;

import java.util.List;

import no.esotericgames.kanin.android.models.EarnedTarotCardStatistic;
import no.esotericgames.kanin.android.models.GameResultModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface StatisticsAPI {

    @GET("statistics/earned-magick-cards")
    Call<List<EarnedTarotCardStatistic>> getEarnedMagickCards(@Query("accessToken") String accessToken);
}
