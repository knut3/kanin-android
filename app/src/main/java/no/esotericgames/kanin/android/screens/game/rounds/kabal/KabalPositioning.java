package no.esotericgames.kanin.android.screens.game.rounds.kabal;

import android.graphics.PointF;

import java.util.Dictionary;
import java.util.Hashtable;

import no.esotericgames.kanin.android.models.Card;

public class KabalPositioning {
    public String[] suits = {Card.Suits.CLUBS, Card.Suits.DIAMONDS, Card.Suits.SPADES, Card.Suits.HEARTS};
    private final float spaceBetweenCards;
    private final PointF cardSize, playingSurfaceCenter;

    public float allStacksWidth, allStacksHeight;
    public float allStacksLeft, allStacksTop;

    private final Dictionary<String, PointF> topStackPositions, bottomStackPositions;

    public KabalPositioning(PointF cardSize, PointF playingSurfaceCenter) {
        this.cardSize = cardSize;
        this.playingSurfaceCenter = playingSurfaceCenter;

        topStackPositions = new Hashtable<>();
        bottomStackPositions = new Hashtable<>();
        spaceBetweenCards = cardSize.x / 10;

        calculateSpaceNeededForAllStacks();
        calculateStackPositions();
    }

    public PointF getPilePosition(Card card){
        PointF stackPosition;
        if (card.value >= 7){
            stackPosition = topStackPositions.get(card.suit);
        }
        else{
            stackPosition = bottomStackPositions.get(card.suit);
        }

        PointF returnedPos = new PointF(stackPosition.x, stackPosition.y);

        return returnedPos;
    }

    private void calculateSpaceNeededForAllStacks(){
        allStacksWidth = (cardSize.x * 4) + (spaceBetweenCards * 3);
        allStacksHeight = (cardSize.y * 2) + spaceBetweenCards;
    }

    private void calculateStackPositions(){
        allStacksLeft = playingSurfaceCenter.x - (allStacksWidth / 2);
        allStacksTop = playingSurfaceCenter.y - (allStacksHeight / 2);
        float bottomStacksTop = allStacksTop + cardSize.y + spaceBetweenCards;


        for (int i = 0; i < suits.length; i++){
            String suit = suits[i];
            float relativeLeft = (spaceBetweenCards * i) + (cardSize.x * i);
            PointF topStackPos = new PointF();
            topStackPos.x = allStacksLeft + relativeLeft;
            topStackPos.y = allStacksTop;
            topStackPositions.put(suit, topStackPos);

            PointF bottomStackPos = new PointF();
            bottomStackPos.x = allStacksLeft + relativeLeft;
            bottomStackPos.y = bottomStacksTop;
            bottomStackPositions.put(suit, bottomStackPos);

        }

    }
}
