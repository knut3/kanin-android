package no.esotericgames.kanin.android.networking.duplex;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;

import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;

import java.util.ArrayList;
import java.util.List;

import no.esotericgames.kanin.android.common.Constants;
import no.esotericgames.kanin.android.common.SharedPreferenceKeys;

public class SignalrService extends Service {

    public interface Listener{
        void onServerMaintenancePlanned(int duration_min);
        void onMessageFromAdmin(String message);
    }

    // Binder given to clients
    private final IBinder binder = new LocalBinder();
    private HubConnection hubConnection;
    private final List<Listener> listeners = new ArrayList<>(2);

    public class LocalBinder extends Binder {
        HubConnection getConnection() {
            return hubConnection;
        }

        void addListener(Listener l){
            listeners.add(l);
        }

        void removeListener(Listener l){
            listeners.remove(l);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        String accessToken = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(SharedPreferenceKeys.ACCESS_TOKEN, "");

        hubConnection = HubConnectionBuilder.create(Constants.GAME_HUB_URL)
                .withHeader("accessToken", accessToken)
                .build();
        hubConnection.on("serverMaintenance", this::onServerMaintenance, Integer.class);
        hubConnection.on("messageFromAdmin", this::onMessageFromAdmin, String.class);
    }


    private void onServerMaintenance(int duration) {
        for (Listener l : listeners)
            l.onServerMaintenancePlanned(duration);
    }

    private void onMessageFromAdmin(String message) {
        for (Listener l : listeners)
            l.onMessageFromAdmin(message);
    }

    @Override
    public void onDestroy() {
        if (hubConnection != null)
            hubConnection.stop();
        super.onDestroy();
    }
}