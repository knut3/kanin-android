package no.esotericgames.kanin.android.networking.halfduplex;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import no.esotericgames.kanin.android.common.Constants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiService {
    private final Retrofit retrofit;

    public ApiService() {
        Gson gson = new GsonBuilder().setLenient().create();

        this.retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public UserAPI getUserAPI(){
        return retrofit.create(UserAPI.class);
    }
}
