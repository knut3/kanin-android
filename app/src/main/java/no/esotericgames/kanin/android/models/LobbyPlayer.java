package no.esotericgames.kanin.android.models;

public class LobbyPlayer {
    public String name;
    public boolean isBot;
    public String intelligence;

    public LobbyPlayer(String name) {
        this.name = name;
        isBot = false;
    }

    public LobbyPlayer(BotModel bot) {
        name = bot.name;
        intelligence = bot.intelligence;
        isBot = true;
    }
}
