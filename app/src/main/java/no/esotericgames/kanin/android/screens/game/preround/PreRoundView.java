package no.esotericgames.kanin.android.screens.game.preround;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.view.animation.AccelerateInterpolator;

import java.util.Timer;
import java.util.TimerTask;

import no.esotericgames.kanin.android.common.BaseObservable;

public class PreRoundView extends BaseObservable<PreRoundView.Listener> {

    public interface Listener{
        void preRoundViewEnded();
    }

    public static final int PRELOAD_TEXT_DURATION = 1000;
    public static final int POSTLOAD_TEXT_DURATION = 5000;
    private final View view;
    private final Activity activity;
    private final float textSize = 100;

    private final String preLoadText;
    private final int preLoadTextWidth;
    private float preLoadTextPos_x;
    private float mainTitlePos_y;
    private float subTitlePos_y;

    private final boolean isNewRound;
    private boolean hasViewDimensions;
    private volatile boolean isInPostLoadState;
    private boolean isInViewPostLoadTextState;

    private PostLoadText postLoadText;

    private Rect screenRect;

    private final Paint backgroundPaint;
    private final Paint mainTitlePaint;
    private final Paint subTitlePaint;

    private final Timer timer;

    public PreRoundView(boolean isNewRound, int backgroundColor, View view, String preLoadText) {
        this.view = view;
        this.activity = (Activity) view.getContext();
        this.isNewRound = isNewRound;
        this.preLoadText = preLoadText;
        hasViewDimensions = false;
        isInPostLoadState = false;
        isInViewPostLoadTextState = false;
        mainTitlePaint = new Paint();
        mainTitlePaint.setColor(Color.WHITE);
        mainTitlePaint.setTextSize(textSize);
        subTitlePaint = new Paint();
        subTitlePaint.setColor(Color.WHITE);
        subTitlePaint.setTextSize(textSize/2);
        preLoadTextWidth = (int) mainTitlePaint.measureText(preLoadText);
        backgroundPaint = new Paint();
        backgroundPaint.setStyle(Paint.Style.FILL);
        backgroundPaint.setColor(backgroundColor);
        timer = new Timer();
    }

    public void setScreenDimensions(int width, int height){
        screenRect = new Rect(0, 0, width, height);

        preLoadTextPos_x = width/2 - preLoadTextWidth/2;
        mainTitlePos_y = height/2 - textSize;
        subTitlePos_y = mainTitlePos_y + textSize * 1.5f;

        if (!hasViewDimensions)
            timer.schedule(new IntroducePostLoadTextIfLoaded() , PRELOAD_TEXT_DURATION);

        hasViewDimensions = true;
    }

    public void setPostLoadText(PostLoadText postLoadText){
        postLoadText.mainTitleWidth = (int) mainTitlePaint.measureText(postLoadText.mainTitle);
        postLoadText.mainTitleX = -postLoadText.mainTitleWidth;
        postLoadText.subTitleWidth = (int) subTitlePaint.measureText(postLoadText.subTitle);
        postLoadText.subTitleX = -postLoadText.subTitleWidth;
        this.postLoadText = postLoadText;
        isInPostLoadState = true;
    }

    public void draw(Canvas canvas){
        if (!hasViewDimensions) return;
        canvas.drawRect(screenRect, backgroundPaint);
        canvas.drawText(preLoadText, preLoadTextPos_x, mainTitlePos_y, mainTitlePaint);
        if (isInViewPostLoadTextState){
            canvas.drawText(postLoadText.mainTitle, postLoadText.mainTitleX, mainTitlePos_y, mainTitlePaint);
            canvas.drawText(postLoadText.subTitle, postLoadText.subTitleX, subTitlePos_y, subTitlePaint);
        }
    }

    private class IntroducePostLoadTextIfLoaded extends TimerTask {

        @Override
        public void run() {
            if (!isInPostLoadState) {
                timer.schedule(new IntroducePostLoadTextIfLoaded(), 1000);
                return;
            }

            if (!isNewRound)
                activity.runOnUiThread(PreRoundView.this::removePreLoadText);
            else
                activity.runOnUiThread(()->{
                    removePreLoadText();
                    introducePostLoadText();
                });
        }
    }

    private class FadeOutAll extends TimerTask {

        @Override
        public void run() {
            activity.runOnUiThread(()->{

                ValueAnimator fadeAnimator = ValueAnimator.ofInt(255, 0);
                fadeAnimator.setInterpolator(new AccelerateInterpolator());
                fadeAnimator.addUpdateListener(animator->{
                    int alpha = (int)animator.getAnimatedValue();
                    mainTitlePaint.setAlpha(alpha);
                    backgroundPaint.setAlpha(alpha);
                    view.invalidate();
                });
                fadeAnimator.addListener(new AnimatorListenerAdapter() {

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        for (Listener l : getListeners())
                            l.preRoundViewEnded();
                    }
                });

                fadeAnimator.start();
            });
        }
    }

    private void removePreLoadText(){
        ValueAnimator preLoadTextAnimator =
                ValueAnimator.ofFloat(preLoadTextPos_x, screenRect.right);
        preLoadTextAnimator.addUpdateListener(animator -> {
            preLoadTextPos_x = (float)animator.getAnimatedValue();
            view.invalidate();
        });
        if (!isNewRound) {
            preLoadTextAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    new FadeOutAll().run();
                }
            });
        }
        preLoadTextAnimator.start();
    }

    private void introducePostLoadText() {
        isInViewPostLoadTextState = true;

        ValueAnimator mainTitleAnimator =
                ValueAnimator.ofInt(postLoadText.mainTitleX, screenRect.centerX() - postLoadText.mainTitleWidth/2);
        mainTitleAnimator.addUpdateListener(animator->{
            postLoadText.mainTitleX = (int)animator.getAnimatedValue();
            view.invalidate();
        });

        ValueAnimator subTitleAnimator =
                ValueAnimator.ofInt(postLoadText.subTitleX, screenRect.centerX() - postLoadText.subTitleWidth/2);
        subTitleAnimator.addUpdateListener(animator->{
            postLoadText.subTitleX = (int)animator.getAnimatedValue();
        });

        AnimatorSet set = new AnimatorSet();
        set.playTogether(mainTitleAnimator, subTitleAnimator);
        set.start();
        timer.schedule(new FadeOutAll(), POSTLOAD_TEXT_DURATION);
    }
}
