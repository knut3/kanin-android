package no.esotericgames.kanin.android.models;

public class CurrentScore {
    public String username;
    public int roundScore;
    public int totalScore;
}
