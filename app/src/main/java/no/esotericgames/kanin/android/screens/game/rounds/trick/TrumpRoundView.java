package no.esotericgames.kanin.android.screens.game.rounds.trick;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.GameState;
import no.esotericgames.kanin.android.models.Card;
import no.esotericgames.kanin.android.models.Player;
import no.esotericgames.kanin.android.screens.common.helpers.ToastMaster;
import no.esotericgames.kanin.android.screens.common.ResourceManager;
import no.esotericgames.kanin.android.screens.game.common.ViewAnimator;
import no.esotericgames.kanin.android.screens.game.rounds.common.CardView;
import no.esotericgames.kanin.android.screens.game.rounds.common.MyCardsView;
import no.esotericgames.kanin.android.screens.game.turnindicator.TurnIndicatorAnimator;

public class TrumpRoundView extends TrickRoundView{
    private static final float SPACE_BETWEEN_CARDS_FRACTION = 0.1f;
    private CardView[] trumpDrawCards;
    private TrumpDrawInfoView infoTextView;
    private final String firstToPlay;
    private float cards_y;
    private float cardHeight;

    public TrumpRoundView(Point screenSize, GameState gameState,
                          Player me, ViewAnimator cardAnimator,
                          TurnIndicatorAnimator turnIndicatorAnimator,
                          ResourceManager resourceManager, ToastMaster toastMaster) {
        super(screenSize, gameState, me, cardAnimator, turnIndicatorAnimator, resourceManager, toastMaster);
        firstToPlay = gameState.nextToPlay;

        if (!gameState.round.hasStarted){
            trumpDrawCards = getTrumpDrawCards();
            infoTextView = new TrumpDrawInfoView(
                    new PointF(super.playingSurfaceModel.center.x, cards_y/2),
                    cardHeight/4,
                    resourceManager.getString(R.string.trump_draw_info, gameState.playerToPerformTrumpDraw));
            state = State.PICKING_TRUMP;
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (state == State.PICKING_TRUMP){
            infoTextView.draw(canvas);
            for (CardView card : trumpDrawCards)
                card.draw(canvas);
        }
    }

    public boolean hasRoundStarted(){
        return state != State.PICKING_TRUMP;
    }

    public Card getTrumpDrawHit(float x, float y){
        for (CardView cardView : trumpDrawCards){
            if (cardView.isClicked(x, y))
                return cardView.getCard();
        }

        return null;
    }

    public void trumpSuitSelected(String trumpSuit, int index) {
        Drawable aceOfTrumpDrawable = resourceManager.getCardDrawable(new Card(trumpSuit, 14));
        CardView selectedCard = trumpDrawCards[index];
        selectedCard.setDrawable(aceOfTrumpDrawable);
        // move to front
        CardView currentFront = trumpDrawCards[3];
        trumpDrawCards[index] = currentFront;
        trumpDrawCards[3] = selectedCard;

        showcaseTrumpSuit(selectedCard, ()-> {
            setFirstToPlay(firstToPlay);
            announceFirstToPlay(firstToPlay, false);
            trumpDrawCards = null;
            infoTextView = null;
        });
    }


    private CardView[] getTrumpDrawCards(){
        cardHeight = (playingSurfaceModel.bottom - playingSurfaceModel.top) / 3;
        float cardWidth = cardHeight / MyCardsView.CARD_WIDTH_HEIGHT_RATIO;
        float space = cardWidth * SPACE_BETWEEN_CARDS_FRACTION;
        float totalWidth = cardWidth * 4 + space * 3;
        float leftCard_x = playingSurfaceModel.center.x - totalWidth/2;
        cards_y = playingSurfaceModel.center.y - cardHeight/2;
        trumpDrawCards = new CardView[4];
        Drawable cardBack = resourceManager.getDrawable(R.drawable.card_back);
        for (int i = 0; i < 4; i++){
            CardView card = new CardView(new Card("", i), cardBack);
            card.setSize(new PointF(cardWidth, cardHeight));
            float x = leftCard_x + i * (cardWidth+space);
            card.setPosition(new PointF(x, cards_y));
            trumpDrawCards[i] = card;
        }
        return trumpDrawCards;
    }

    private void showcaseTrumpSuit(CardView cardView, Runnable actionOnEnd){

        float finalCardHeight = screenSize.y / 2f;
        PointF finalCardSize = new PointF(
                finalCardHeight / MyCardsView.CARD_WIDTH_HEIGHT_RATIO,
                finalCardHeight
        );
        PointF cardDestination = new PointF(
                playingSurfaceModel.center.x - finalCardSize.x/2,
                playingSurfaceModel.center.y - finalCardSize.y/2
        );
        viewAnimator.animate(cardView, cardDestination, finalCardSize,
                1100, 400, ()-> fadeOutTrumpDraw(actionOnEnd));
    }

    private void fadeOutTrumpDraw(Runnable actionOnEnd){
        ValueAnimator fade = ValueAnimator.ofInt(255, 0);
        fade.setDuration(400);
        fade.addUpdateListener(animator-> {
            int currentAlpha = (int)animator.getAnimatedValue();
            infoTextView.setAlpha(currentAlpha);
            for (CardView cardView : trumpDrawCards)
                cardView.setAlpha(currentAlpha);
            viewAnimator.getDrawingView().postInvalidate();
        });
        fade.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                actionOnEnd.run();
            }
        });
        fade.setStartDelay(400);
        fade.start();
    }
}
