package no.esotericgames.kanin.android.screens.registration;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.networking.halfduplex.UserAPI;
import no.esotericgames.kanin.android.models.LoginModel;
import no.esotericgames.kanin.android.common.SharedPreferenceKeys;
import no.esotericgames.kanin.android.screens.common.BaseActivity;
import no.esotericgames.kanin.android.screens.common.helpers.ScreensNavigator;
import no.esotericgames.kanin.android.screens.login.LoginActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends BaseActivity {

    private static final int USERNAME_MAX_LENGTH = 11;
    private static final int PASSWORD_MAX_LENGTH = 11;

    private static final String TAG = "RegistrationActivity";
    private ScreensNavigator screensNavigator;
    private UserAPI userAPI;
    private TextInputEditText edUsername;
    private TextInputEditText edPassword1;
    private TextInputEditText edPassword2;
    private TextInputLayout layoutUsername;
    private TextInputLayout layoutPassword2;
    private Button btnRegister;
    private Button btnGoToLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        screensNavigator = getCompositionRoot().getScreensNavigator();
        userAPI = getActivityCompositionRoot().getUserApi();

        setContentView(R.layout.activity_registration);

        edUsername = findViewById(R.id.edRegistrationUsername);
        edPassword1 = findViewById(R.id.edRegistrationPassword1);
        edPassword2 = findViewById(R.id.edRegistrationPassword2);
        layoutUsername = findViewById(R.id.layoutRegistrationUsername);
        layoutPassword2 = findViewById(R.id.layoutRegistrationPassword2);

        RemoveErrorMessage removeErrorMessages = new RemoveErrorMessage(layoutUsername, layoutPassword2);
        edUsername.addTextChangedListener(removeErrorMessages);
        edPassword1.addTextChangedListener(removeErrorMessages);
        edPassword2.addTextChangedListener(removeErrorMessages);

        btnRegister = findViewById(R.id.btnRegistrationRegister);
        btnRegister.setOnClickListener(v -> tryRegister());

        btnGoToLogin = findViewById(R.id.btnRegistrationLogin);
        btnGoToLogin.setOnClickListener(v -> screensNavigator.toLogin());


    }

    private void tryRegister() {

        String username = edUsername.getText().toString();
        String password1 = edPassword1.getText().toString();
        String password2 = edPassword2.getText().toString();

        if (username.isEmpty()){
            layoutUsername.setError(getString(R.string.registration_error_no_username));
            return;
        }

        if (username.length() > USERNAME_MAX_LENGTH){
            layoutUsername.setError(getString(R.string.registration_error_username_too_long, USERNAME_MAX_LENGTH));
            return;
        }

        if (password1.isEmpty()){
            layoutPassword2.setError(getString(R.string.registration_error_no_password));
            return;
        }

        if (password1.length() > PASSWORD_MAX_LENGTH){
            layoutPassword2.setError(getString(R.string.registration_error_password_too_long, PASSWORD_MAX_LENGTH));
            return;
        }

        if (!password1.equals(password2)){
            layoutPassword2.setError(getString(R.string.registration_error_passwords_inequal));
            return;
        }

        userAPI.isAvailable(username).enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()){
                    boolean usernameAvailable = response.body();
                    if (usernameAvailable){
                        register(new LoginModel(username, password1));
                    }
                    else{
                        layoutUsername.setError(getString(R.string.registration_error_username_taken));
                    }
                }
                else {
                    onFailure(call, new Exception());
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.d(TAG, t.getMessage());
                Toast.makeText(RegistrationActivity.this, getString(R.string.login_error_unknown), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void register(LoginModel model){
        userAPI.register(model).enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (!response.isSuccessful()){
                        Toast.makeText(RegistrationActivity.this,
                                "Request failed with HTTP status code " + response.code(),
                                Toast.LENGTH_LONG).show();
                        return;
                    }

                    Log.d("RegisterAPIcall", "AccessToken" + response.body());
                    SharedPreferences preferences = getCompositionRoot().getSharedPreferences();
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString(SharedPreferenceKeys.ACCESS_TOKEN, response.body());
                    editor.putString(SharedPreferenceKeys.USERNAME, model.getUsername());
                    editor.apply();

                    screensNavigator.toMainMenu(true);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    Log.e("RegisterAPIcall", t.getMessage());
                    Toast.makeText(RegistrationActivity.this, getString(R.string.login_error_unknown), Toast.LENGTH_LONG).show();
                }
            });
    }
}