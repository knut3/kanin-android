package no.esotericgames.kanin.android.screens.game.turnindicator;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;

public class TurnIndicatorView implements IDrawable {

    public enum TableEdge {BOTTOM, LEFT, TOP, RIGHT}
    private final Drawable drawable;
    private PointF size, centerPosition;
    private Rect rect;
    private float rotation;
    private TableEdge currentEdge;
    private final RectF tableRect;
    private final float tableRoundingRadius;
    private float straightEdgePercentage, curveSideAngle;
    private final PointF leftCurvePivot, rightCurvePivot;
    private final double lenghtOfOnePercentOfStraightEdge;
    private boolean shouldDraw = true;

    public TurnIndicatorView(Drawable drawable, RectF tableRect, float tableRoundingRadius) {
        this.drawable = drawable;
        this.tableRect = tableRect;
        this.tableRoundingRadius = tableRoundingRadius;
        leftCurvePivot = new PointF(tableRect.left+tableRoundingRadius, tableRect.centerY());
        rightCurvePivot = new PointF(tableRect.right-tableRoundingRadius, tableRect.centerY());
        lenghtOfOnePercentOfStraightEdge = (tableRect.width() - 2 * tableRoundingRadius) / 100;
    }

    public void setSize(PointF size) {
        this.size = size;
    }

    @Override
    public void draw(Canvas canvas) {
        if (!shouldDraw) return;

        if (currentEdge == TableEdge.LEFT){
            canvas.save();
            canvas.rotate(curveSideAngle, leftCurvePivot.x, leftCurvePivot.y);
        }
        else if (currentEdge == TableEdge.RIGHT){
            canvas.save();
            canvas.rotate(curveSideAngle, rightCurvePivot.x, rightCurvePivot.y);
        }
        canvas.save();
        canvas.rotate(rotation, centerPosition.x, centerPosition.y);
        drawable.setBounds(rect);
        drawable.draw(canvas);
        canvas.restore();
        if (currentEdge == TableEdge.LEFT || TableEdge.RIGHT == currentEdge){
            canvas.restore();
        }
    }

    public void hide() {
        shouldDraw = false;
    }

    public TableEdge getCurrentEdge(){
        return currentEdge;
    }

    public void setStraightEdgePosition(float percentage, TableEdge edge){
        this.currentEdge = edge;
        straightEdgePercentage = percentage;
        curveSideAngle = 0;
        float edgeWidth = tableRect.width() - 2*tableRoundingRadius;
        float x = tableRect.left + tableRoundingRadius + getDecimalFraction(percentage) * edgeWidth;

        float y;
        if (edge == TableEdge.TOP){
            rotation = 180;
            y = tableRect.top;
        }
        else {
            rotation = 0;
            y = tableRect.bottom;
        }
        setPosition(new PointF(x, y));
    }

    public float getStraightEdgePosition(){
        if (currentEdge == TableEdge.LEFT)
            return 0;
        else if (currentEdge == TableEdge.RIGHT)
            return 100;

        return straightEdgePercentage;
    }

    public void setSideAngle(float angle, TableEdge edge){
        if (TableEdge.LEFT != edge && edge != TableEdge.RIGHT)
            throw new IllegalArgumentException("Angle is concerned with left or right side of table");

        currentEdge = edge;
        curveSideAngle = angle;

    }

    public float getSideAngle(){
        return curveSideAngle;
    }

    public float getTableSideArchLengthOneDegree(){
        return (float) (2 * Math.PI * tableRoundingRadius / 360);
    }

    public float getStraightEdgeLength(float percentage1, float percentage2){
        float lengthInPercentage = Math.abs(percentage1-percentage2);
        return (float) (lenghtOfOnePercentOfStraightEdge * lengthInPercentage);
    }

    private void setPosition(PointF position) {
        if (size == null) throw new RuntimeException("Size needs to be set");

        centerPosition = position;
        int x = (int)(position.x - size.x/2);
        int y = (int)(position.y - size.y/2);
        int width = x+(int)size.x;
        int height = y+(int)size.y;
        rect = new Rect(x, y, width, height);
    }

    private float getDecimalFraction(float percentage){
        return percentage / 100;
    }
}
