package no.esotericgames.kanin.android.networking.duplex;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionState;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public abstract class BaseSignalRRepository<ListenerType extends BaseSignalRRepository.Listener> implements SignalrService.Listener {

    private static final String TAG = "BaseSignalRRepository";

    public interface Listener{

        void onConnectedToServer();

        void onServerMaintenanceInProgress(int duration_min);

        void onMessageFromAdmin(String message);
    }

    private final Context bindingContext;
    private final Intent signalrServiceIntent;
    protected final Handler uiHandler;
    private Disposable connectedSubscription;
    protected static final int RETRY_COUNT = 2;
    protected ListenerType listener = null;
    private SignalrService.LocalBinder binder;
    protected HubConnection hubConnection;
    protected CompositeDisposable disposables = new CompositeDisposable();

    private final ServiceConnection serviceConnection = new ServiceConnection(){
        @Override
        public void onServiceConnected(ComponentName name, IBinder service){
            binder = (SignalrService.LocalBinder) service;
            hubConnection = binder.getConnection();
            binder.addListener(BaseSignalRRepository.this);
            setUpRpc();
            if (hubConnection.getConnectionState() == HubConnectionState.CONNECTED)
                listener.onConnectedToServer();
            else if (hubConnection.getConnectionState() == HubConnectionState.DISCONNECTED)
                start();
        }
        @Override
        public void onServiceDisconnected(ComponentName name){
            hubConnection = null;
            listener = null;
        }
    };

    public BaseSignalRRepository(Context bindingContext, Handler uiHandler) {
        this.bindingContext = bindingContext;
        this.uiHandler = uiHandler;
        signalrServiceIntent = new Intent(bindingContext, SignalrService.class);
    }

    //----------Events----------------------------------------------

    private void connectedToServer(){
        listener.onConnectedToServer();

        connectedSubscription.dispose();
    }

    // --------- Events from SignalRService ------------------

    @Override
    public void onServerMaintenancePlanned(int duration_min) {
        uiHandler.post(()-> listener.onServerMaintenanceInProgress(duration_min));
    }

    @Override
    public void onMessageFromAdmin(String message) {
        uiHandler.post(()-> listener.onMessageFromAdmin(message));
    }

    //----------- Public--------------------------

    public void tryReconnect(){
        if (hubConnection.getConnectionState() == HubConnectionState.DISCONNECTED)
            start();
    }

    public void bind(ListenerType listener){
        this.listener = listener;
        bindingContext.bindService(signalrServiceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }


    public void unbind(){
        if (binder != null) binder.removeListener(this);
        if (hubConnection != null) removeRpc();
        bindingContext.unbindService(serviceConnection);
    }

    //-----------Protected--------------------------------

    protected void setUpRpc(){}

    protected void removeRpc(){}

    protected void noAction(){}

    protected void logError(Throwable err) {
            Log.e(TAG, err.getMessage());
    }

    //-----------Private-----------------------------------

    private void start() {
        connectedSubscription = hubConnection.start()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(RETRY_COUNT)
                .subscribe(this::connectedToServer, this::logError);
    }

}
