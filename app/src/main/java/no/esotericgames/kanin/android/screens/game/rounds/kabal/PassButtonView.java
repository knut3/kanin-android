package no.esotericgames.kanin.android.screens.game.rounds.kabal;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;
import no.esotericgames.kanin.android.screens.common.ResourceManager;

public class PassButtonView implements IDrawable {
    private PointF centerPosition;
    private float radius;
    private String text;
    private Paint bgPaint, strokePaint, textPaint;
    private final float textExactCenter_y;


    public PassButtonView(PointF position, float diameter, ResourceManager resourceManager) {
        this.radius = diameter/2;
        this.centerPosition = new PointF(position.x + radius, position.y + radius);
        text = "Pass";

        bgPaint = new Paint();
        bgPaint.setStyle(Paint.Style.FILL);
        bgPaint.setColor(resourceManager.getColor(R.color.positive_score_bg));

        strokePaint = new Paint();
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setStrokeWidth(diameter/11);
        strokePaint.setAntiAlias(true);
        strokePaint.setColor(Color.BLACK);

        textPaint = new Paint();
        textPaint.setTextSize(diameter/4);
        textPaint.setAntiAlias(true);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextAlign(Paint.Align.CENTER);
        Rect bounds = new Rect();
        textPaint.getTextBounds(text, 0, text.length(), bounds);
        textExactCenter_y = bounds.exactCenterY();
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawCircle(centerPosition.x, centerPosition.y, radius, bgPaint);
        canvas.drawCircle(centerPosition.x, centerPosition.y, radius, strokePaint);
        canvas.drawText(text,
                centerPosition.x, centerPosition.y - textExactCenter_y, textPaint);

    }

    public boolean isClicked(float x, float y){
        if (x < centerPosition.x - radius) return false;
        if (x > centerPosition.x + radius) return false;
        if (y < centerPosition.y - radius) return false;
        if (y > centerPosition.y + radius) return false;

        return true;
    }
}
