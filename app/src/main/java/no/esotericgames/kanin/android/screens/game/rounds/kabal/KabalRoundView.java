package no.esotericgames.kanin.android.screens.game.rounds.kabal;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.GameState;
import no.esotericgames.kanin.android.models.KabalPassModel;
import no.esotericgames.kanin.android.models.Card;
import no.esotericgames.kanin.android.models.Player;
import no.esotericgames.kanin.android.screens.common.helpers.ToastMaster;
import no.esotericgames.kanin.android.screens.game.common.InfoDrawerContentUpdater;
import no.esotericgames.kanin.android.screens.game.rounds.common.MyCardsView;
import no.esotericgames.kanin.android.screens.game.rounds.common.PlayingSurfaceModel;
import no.esotericgames.kanin.android.screens.common.ResourceManager;
import no.esotericgames.kanin.android.screens.game.rounds.common.TableCard;
import no.esotericgames.kanin.android.screens.game.common.ViewAnimator;
import no.esotericgames.kanin.android.screens.game.rounds.common.RoundView;
import no.esotericgames.kanin.android.screens.game.rounds.common.scorebubbles.ScoreBubblesView;
import no.esotericgames.kanin.android.screens.game.scoreboard.IScoreBoard;
import no.esotericgames.kanin.android.screens.game.turnindicator.TurnIndicatorAnimator;

public class KabalRoundView extends RoundView {

    private final float passScoreBubbleMaxRadius;

    private KabalPositioning positioning;
    private final PassButtonView passButtonView;
    private final List<Player> allPlayers;
    private final SpotLightView spotLightView;

    public KabalRoundView(Point screenSize, GameState gameState,
                          Player me, ViewAnimator cardAnimator,
                          TurnIndicatorAnimator turnIndicatorAnimator,
                          ResourceManager resourceManager, ToastMaster toastMaster,
                          SpotLightView spotLightView) {
        super(screenSize, gameState, me, cardAnimator, turnIndicatorAnimator,  resourceManager, toastMaster);

        passButtonView = getPassButton();
        allPlayers = new ArrayList<>(Arrays.asList(gameState.opponents));
        allPlayers.add(me);
        passScoreBubbleMaxRadius = screenSize.y / 6f;
        this.spotLightView = spotLightView;

    }

    public boolean isValidPassButtonClick(float x, float y){
        if (!passButtonView.isClicked(x, y)) return false;

        if (state != State.MY_TURN) {
            toastMaster.showFailureToast(resourceManager.getString(R.string.toast_not_your_turn));
            return false;
        }

        for (Card card : myCardsView.getCards()){
            if (isValidCardToThrow(card)){
                toastMaster.showFailureToast(resourceManager.getString(R.string.toast_cabal_pass_not_allowed));
                return false;
            }
        }

        state = State.OPPONENT_TURN;
        return true;
    }

    public void pass(KabalPassModel model){
        int nextToPlayDelay_ms = 800;
        scoreBubblesView.createKabalPassScoreBubble(model.passer, passScoreBubbleMaxRadius, null);
        setNextToPlay(model.nextToPlay, nextToPlayDelay_ms);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        passButtonView.draw(canvas);
        if (spotLightView != null && spotLightView.getShouldDraw())
            spotLightView.draw(canvas);
    }

    @Override
    protected boolean useCardsLeftAsRoundScore() {
        return true;
    }

    @Override
    public void onOpponentThrowingCard(String opponentName) {
        addRoundScore(-1, opponentName);
    }

    @Override
    protected boolean isValidCardToThrow(Card card) {
        if (card.value == 7){
            if (card.suit.equals(Card.Suits.CLUBS)) return true;
            else return tableContainsSuitAbove(Card.Suits.CLUBS, 6);
        }
        else if (card.value == 14)
            return isValidAceToThrow_MightChangeValueToOne(card);
        else if (card.value > 7)
            return tableContains(card.suit, card.value-1);
        else if (card.value == 6)
            return tableContainsSuitAbove(card.suit, 7);
        else return tableContains(card.suit, card.value+1);
    }

    @Override
    protected String getInvalidCardReason(Card card) {
        if (tableCards.size() == 0)
            return resourceManager.getString(R.string.illegal_move_kabal_first_move);

        String suitIcon = InfoDrawerContentUpdater.getSuitIconHtmlCode(card.suit);
        if (card.value == 14)
            return resourceManager.getString(R.string.illegal_move_kabal_ace, suitIcon);

        int valueNeeded;
        if (card.value < 6)
            valueNeeded = card.value + 1;
        else if (card.value == 6)
            valueNeeded = 8;
        else valueNeeded = card.value - 1;

        String valueNeededString = ""+valueNeeded;
        if (valueNeeded == 11) valueNeededString = "J";
        else if (valueNeeded == 12) valueNeededString = "Q";

        return resourceManager.getString(R.string.illegal_move_kabal_default, suitIcon, valueNeededString);
    }

    @Override
    protected PointF getTableCardPosition(Card card, String owner, PlayingSurfaceModel playingSurface) {
        if (positioning == null)
            positioning = new KabalPositioning(getTableCardSize(playingSurface), playingSurface.center);

        return positioning.getPilePosition(card);
    }

    @Override
    protected PointF getTableCardSize(PlayingSurfaceModel playingSurface) {
        float surfaceHeight = playingSurface.bottom - playingSurface.top;
        float height = surfaceHeight / 3f;
        float width = height / MyCardsView.CARD_WIDTH_HEIGHT_RATIO;
        return new PointF(width, height);
    }

    @Override
    protected boolean shouldUseDefaultTableCardRotation() {
        return true;
    }

    @Override
    protected void cleanupAfterThrownCard(Card thrownCard) {
        removeUnnecessaryCardFromTable(thrownCard);
    }

    @Override
    protected void drawOnTable(Canvas canvas) {

    }

    public void endKabalRound(Runnable actionOnEnd) {
        int spotLightIntro_ms = 3000;
        int delayBetweenPlayers = 1000;
        float maxRadius = screenSize.y / 24f;
        List<Player> playersWithCardsLeft = new ArrayList<>(allPlayers.size()-1);

        for (Player player : allPlayers){
            IScoreBoard scoreBoard = player.getUsername().equals(me.getUsername())
                    ? myScoresView
                    : scoreBoardsView.getScoreBoardView(player.getUsername());

            if (scoreBoard.getRoundScore() > 0)
                playersWithCardsLeft.add(player);
        }

        int delay = spotLightIntro_ms;

        PointF previousScoreCardCenter = null;

        for (int i = 0; i < playersWithCardsLeft.size(); i++){
            Player player = playersWithCardsLeft.get(i);
            IScoreBoard scoreBoard = player.getUsername().equals(me.getUsername())
                    ? myScoresView
                    : scoreBoardsView.getScoreBoardView(player.getUsername());


            Runnable onEnd = i == (playersWithCardsLeft.size()-1)
                    ? actionOnEnd
                    : null;

            scoreBubblesView.createKabalEndCardCountScoreBubbles(scoreBoard.getRoundScore(),
                    player.getUsername(), delay, maxRadius, onEnd);

            PointF scoreBoardCenter = player.getUsername().equals(me.getUsername())
                    ? myScoresView.getCenterPosition()
                    : scoreBoardsView.getScoreBoardCenter(player.getUsername());

            if (i == 0){
                PointF startPos = new PointF(screenSize.x/2f, screenSize.y/2f);
                spotLightView.setRadius(0);
                spotLightView.animateTo(startPos, scoreBoardCenter, screenSize.y/5f, spotLightIntro_ms, 0);
            }
            else{
                spotLightView.animateTo(previousScoreCardCenter, scoreBoardCenter, null, delayBetweenPlayers, delay);
            }

            previousScoreCardCenter = scoreBoardCenter;
            delay += delayBetweenPlayers + scoreBoard.getRoundScore() * ScoreBubblesView.MS_BETWEEN_BUBBLES;
        }

    }


    private PassButtonView getPassButton(){
        float diameter = tableView.getRect().height() / 4;
        float margin = diameter / 4;
        float y = tableView.getRect().bottom + tableView.getStrokeWidth()/2 - diameter;
        float x = screenSize.x - diameter - margin;
        return new PassButtonView(new PointF(x, y), diameter, resourceManager);
    }

    private boolean isValidAceToThrow_MightChangeValueToOne(Card ace){
        if (tableContains(ace.suit, 13)) return true;
        else if (tableContains(ace.suit, 2)){
            ace.value = 1;
            return true;
        }
        else return false;
    }

    private boolean tableContains(String suit, int value){
        for (TableCard tableCard : tableCards){
            Card card = tableCard.getCardView().getCard();
            if (card.value == value && card.suit.equals(suit))
                return true;
        }
        return false;
    }

    private boolean tableContainsSuitAbove(String suit, int value){
        for (TableCard tableCard : tableCards){
            Card card = tableCard.getCardView().getCard();
            if (card.value > value && card.suit.equals(suit))
                return true;
        }
        return false;
    }

    private void removeUnnecessaryCardFromTable(Card newCard){
        if (newCard.value == 7 || 6 == newCard.value) return;

        int valueToRemove;
        if (newCard.value > 7) valueToRemove = newCard.value - 1;
        else valueToRemove = newCard.value + 1;

        for (int i = 0; i < tableCards.size(); i++){
            Card card = tableCards.get(i).getCardView().getCard();
            if (card.value == valueToRemove && newCard.suit.equals(card.suit)){
                tableCards.remove(i);
                return;
            }
        }
    }

}
