package no.esotericgames.kanin.android.networking.duplex;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;

import com.microsoft.signalr.TypeReference;

import java.lang.reflect.Type;
import java.util.List;

import io.reactivex.rxjava3.disposables.Disposable;
import no.esotericgames.kanin.android.models.AvailableLobbyModel;

public class LobbiesRepository extends BaseSignalRRepository<LobbiesRepository.Listener> {

    public interface Listener extends BaseSignalRRepository.Listener{
        void onGotAvailableLobbies(List<AvailableLobbyModel> lobbies);
        void onLobbyCreated(AvailableLobbyModel lobby);
        void onLobbyUnavailable(String gameName);
    }

    public LobbiesRepository(Context bindingContext, Handler uiHandler) {
        super(bindingContext, uiHandler);
    }

    //------------Server API-------------------------------------

    public void getAvailableLobbiesAndNotify(){
        Type lobbiesList = new TypeReference<List<AvailableLobbyModel>>(){}.getType();
        Disposable subscription = hubConnection.<List<AvailableLobbyModel>>invoke(lobbiesList,"GetAvailableLobbies")
                .retry(RETRY_COUNT)
                .subscribe(this::gotAvailableLobbies, super::logError);
        disposables.add(subscription);
    }

    //-------------Server Events----------------------------------------

    @Override
    protected void setUpRpc() {
        super.setUpRpc();
        hubConnection.on("lobbyCreated", this::lobbyCreated, AvailableLobbyModel.class);
        hubConnection.on("lobbyUnavailable", this::lobbyUnavailable, String.class);
    }

    @Override
    protected void removeRpc() {
        super.removeRpc();
        hubConnection.remove("lobbyCreated");
        hubConnection.remove("lobbyUnavailable");
    }

    private void gotAvailableLobbies(List<AvailableLobbyModel> lobbies){
        uiHandler.post(()->{
            listener.onGotAvailableLobbies(lobbies);
        });
    }

    private void lobbyCreated(AvailableLobbyModel lobby) {
        uiHandler.post(()->{
            listener.onLobbyCreated(lobby);
        });
    }

    private void lobbyUnavailable(String gameName){
        uiHandler.post(()->{
            listener.onLobbyUnavailable(gameName);
        });
    }
}
