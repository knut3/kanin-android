package no.esotericgames.kanin.android.screens.lobby;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.LobbyPlayer;
import no.esotericgames.kanin.android.screens.common.viewmvc.BaseObservableViewMvc;

public class LobbyPlayerItemViewMvc extends BaseObservableViewMvc<LobbyPlayerItemViewMvc.Listener> {

    public interface Listener{
        void onBotClicked(String name);
    }

    private LobbyPlayer model;
    private final TextView txtUsername, txtBotIntelligence;
    private final ImageView imageRemoveBot;
    private final boolean iAmCreator;

    public LobbyPlayerItemViewMvc(LayoutInflater inflater, @Nullable ViewGroup parent, boolean iAmCreator){
        setRootView(inflater.inflate(R.layout.lobby_players_item, parent, false));
        this.iAmCreator = iAmCreator;
        txtUsername = findViewById(R.id.txtUsername);
        txtBotIntelligence = findViewById(R.id.txtBotIntelligence);
        imageRemoveBot = findViewById(R.id.imageRemoveBot);
    }

    public void bindPlayer(LobbyPlayer model){
        this.model = model;
        txtUsername.setText(model.name);
        if (model.isBot){
            String iqId = model.intelligence.substring(0, 1).toUpperCase();
            txtBotIntelligence.setText(iqId);
            txtBotIntelligence.setVisibility(View.VISIBLE);
            if (iAmCreator){
                getRootView().setOnClickListener(view -> {
                    for (Listener listener : getListeners())
                        listener.onBotClicked(model.name);
                });
                imageRemoveBot.setVisibility(View.VISIBLE);
            }


        }
        else {
            txtBotIntelligence.setVisibility(View.INVISIBLE);
            imageRemoveBot.setVisibility(View.INVISIBLE);
        }
    }
}
