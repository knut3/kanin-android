package no.esotericgames.kanin.android.networking.duplex;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;

import com.microsoft.signalr.TypeReference;

import java.lang.reflect.Type;

import io.reactivex.rxjava3.disposables.Disposable;
import no.esotericgames.kanin.android.models.CardPlayedAndTrickEndedModel;
import no.esotericgames.kanin.android.models.CardPlayedModel;
import no.esotericgames.kanin.android.models.GameEndedAfterKabal;
import no.esotericgames.kanin.android.models.GameEndedAfterTrickRound;
import no.esotericgames.kanin.android.models.GameState;
import no.esotericgames.kanin.android.models.KabalPassModel;
import no.esotericgames.kanin.android.models.KabalRoundEnded;
import no.esotericgames.kanin.android.models.TrickRoundEnded;
import no.esotericgames.kanin.android.models.TrumpSuitSelectedModel;
import no.esotericgames.kanin.android.models.Card;

public class GameRepository extends BaseSignalRRepository<GameRepository.Listener> {

    public interface Listener extends BaseSignalRRepository.Listener{
        void onGotGameState(GameState gameState);
        void onCardPlayed(CardPlayedModel model);
        void onCardPlayedAndTrickEnded(CardPlayedAndTrickEndedModel model);
        void onTrickRoundEnded(TrickRoundEnded model);
        void onKabalPass(KabalPassModel model);
        void onKabalRoundEnded(KabalRoundEnded model);
        void onTrumpSuitSelected(TrumpSuitSelectedModel model);
        void onPlayerConnected(String username);
        void onPlayerDisconnected(String username);
        void onGameEndedAfterKabal(GameEndedAfterKabal model);

        void onGameEndedAfterTrickRound(GameEndedAfterTrickRound model);
    }

    private final String gameName;

    public GameRepository(Context bindingContext, Handler uiHandler, String gameName) {
        super(bindingContext, uiHandler);
        this.gameName = gameName;
    }

    public void getGameStateAndNotify(){
        Type gameStateType = new TypeReference<GameState>(){}.getType();
        Disposable subscription =
                hubConnection.<GameState>invoke(gameStateType, "GetGameState", gameName)
                        .retry(RETRY_COUNT)
                .subscribe(this::onGotGameState, super::logError);
        disposables.add(subscription);
    }

    public void playCard(Card card){
        Disposable subscription = hubConnection.invoke("PlayCard", gameName, card)
                .retry(RETRY_COUNT)
                .subscribe(super::noAction, super::logError);
        disposables.add(subscription);
    }

    public void kabalPass(){
        Disposable subscription = hubConnection.invoke("KabalPass", gameName)
                .retry(RETRY_COUNT)
                .subscribe(super::noAction, super::logError);
        disposables.add(subscription);
    }

    public void performTrumpSuitDraw(int pickedIndex){
        Disposable subscription = hubConnection.invoke("PerformTrumpSuitDraw", gameName, pickedIndex)
                .retry(RETRY_COUNT)
                .subscribe(super::noAction, super::logError);
        disposables.add(subscription);
    }

    @Override
    protected void setUpRpc() {
        super.setUpRpc();
        hubConnection.on("playerConnected", this::onPlayerConnected, String.class);
        hubConnection.on("playerDisconnected", this::onPlayerDisconnected, String.class);
        hubConnection.on("cardPlayed", this::onCardPlayed, CardPlayedModel.class);
        hubConnection.on("cardPlayedAndTrickEnded", this::onCardPlayedAndTrickEnded, CardPlayedAndTrickEndedModel.class);
        Type trickRoundEndedType = new TypeReference<TrickRoundEnded>(){}.getType();
        hubConnection.on("cardPlayedAndRoundEnded", this::onTrickRoundEnded, trickRoundEndedType);
        Type kabalRoundEndedType = new TypeReference<KabalRoundEnded>(){}.getType();
        hubConnection.on("kabalCardPlayedAndRoundEnded", this::onKabalRoundEnded, kabalRoundEndedType);
        hubConnection.on("kabalPass", this::onKabalPass, KabalPassModel.class);
        hubConnection.on("trumpSuitSelected", this::onTrumpSuitSelected, TrumpSuitSelectedModel.class);
        hubConnection.on("gameEndedAfterKabal", this::onGameEndedAfterKabal, GameEndedAfterKabal.class);
        hubConnection.on("gameEndedAfterTrickRound", this::onGameEndedAfterTrickRound, GameEndedAfterTrickRound.class);
    }

    @Override
    protected void removeRpc() {
        super.removeRpc();
        hubConnection.remove("playerConnected");
        hubConnection.remove("playerDisconnected");
        hubConnection.remove("cardPlayed");
        hubConnection.remove("cardPlayedAndTrickEnded");
        hubConnection.remove("cardPlayedAndRoundEnded");
        hubConnection.remove("kabalCardPlayedAndRoundEnded");
        hubConnection.remove("kabalPass");
        hubConnection.remove("trumpSuitSelected");
        hubConnection.remove("gameEndedAfterKabal");
        hubConnection.remove("gameEndedAfterTrickRound");
    }

    // Event Handlers

    private void onGotGameState(GameState gameState) {
            uiHandler.post(()->listener.onGotGameState(gameState));
    }

    private void onCardPlayed(CardPlayedModel model) {
            uiHandler.post(()->listener.onCardPlayed(model));
    }

    private void onCardPlayedAndTrickEnded(CardPlayedAndTrickEndedModel model) {
            uiHandler.post(()->listener.onCardPlayedAndTrickEnded(model));
    }

    private void onTrickRoundEnded(TrickRoundEnded model) {
            uiHandler.post(()->listener.onTrickRoundEnded(model));
    }

    private void onKabalPass(KabalPassModel model) {
            uiHandler.post(()->listener.onKabalPass(model));
    }

    private void onKabalRoundEnded(KabalRoundEnded model) {
            uiHandler.post(()->listener.onKabalRoundEnded(model));
    }

    private void onTrumpSuitSelected(TrumpSuitSelectedModel model) {
            uiHandler.post(()->listener.onTrumpSuitSelected(model));
    }


    private void onPlayerConnected(String username) {
            uiHandler.post(()->listener.onPlayerConnected(username));
    }


    private void onPlayerDisconnected(String username) {
            uiHandler.post(()->listener.onPlayerDisconnected(username));
    }

    private void onGameEndedAfterTrickRound(GameEndedAfterTrickRound model) {
            uiHandler.post(()->listener.onGameEndedAfterTrickRound(model));
    }

    private void onGameEndedAfterKabal(GameEndedAfterKabal model) {
            uiHandler.post(()->listener.onGameEndedAfterKabal(model));
    }
}
