package no.esotericgames.kanin.android.models;

public class Round {
    public String name;
    public int number;
    public boolean hasStarted;
}
