package no.esotericgames.kanin.android.screens.game.rounds.kabal;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.PointFEvaluator;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.Log;
import android.view.View;

public class SpotLightView {
    private final Paint bgPaint, spotlightPaint;
    private final View drawingView;
    private PointF position;
    private float radius;
    private boolean shouldDraw;

    public SpotLightView(int overlayColor, View drawingView) {
        this.drawingView = drawingView;
        shouldDraw = false;
        bgPaint = new Paint();
        bgPaint.setColor(overlayColor);
        spotlightPaint = new Paint();
        spotlightPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
    }
    
    public void shouldDraw(boolean should){
        this.shouldDraw = should;
    }
    
    public boolean getShouldDraw(){
        return shouldDraw;
    }

    public void animateTo(PointF source, PointF destination, Float endRadius, int duration_ms, int delay_ms){
        shouldDraw = true;
        position = source;
        AnimatorSet set = new AnimatorSet();
        ValueAnimator positionAnim = ValueAnimator.ofObject(new PointFEvaluator(), source, destination);
        positionAnim.addUpdateListener(valueAnimator -> {
            position = (PointF) valueAnimator.getAnimatedValue();
            drawingView.postInvalidate();
        });

        set.setDuration(duration_ms);
        set.setStartDelay(delay_ms);

        if (endRadius != null && radius != endRadius){
            ValueAnimator radiusAnim = ValueAnimator.ofFloat(radius, endRadius);
            radiusAnim.addUpdateListener(animator -> {
                radius = (float) animator.getAnimatedValue();
            });
            set.playTogether(positionAnim, radiusAnim);
        }
        else set.play(positionAnim);

        set.start();
    }

    public void setRadius(float radius){
        this.radius = radius;
    }

    public void draw(Canvas canvas) {
        int layerId = canvas.saveLayer(0, 0, canvas.getWidth(), canvas.getHeight(), null, Canvas.ALL_SAVE_FLAG);
        canvas.drawRect(0, 0, canvas.getWidth(), canvas.getHeight(), bgPaint);
        canvas.drawCircle(position.x, position.y, radius, spotlightPaint);
        canvas.restoreToCount(layerId);
    }
}
