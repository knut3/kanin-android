package no.esotericgames.kanin.android.screens.game.common;

import android.graphics.PointF;

public class Vector {

    public static PointF add(PointF vector1, PointF vector2){
        return new PointF(vector1.x + vector2.x, vector1.y + vector2.y);
    }

    public static PointF divide(PointF vector, float by){
        return new PointF(vector.x / by, vector.y / by);
    }

    public static PointF subtract(PointF minuend, PointF subtrahend){
        return new PointF(minuend.x - subtrahend.x, minuend.y - subtrahend.y);
    }

    public static PointF multiply(PointF vector, float by){
        return new PointF(vector.x * by, vector.y * by);
    }

    public static PointF multiply(PointF vector1, PointF vector2){
        return new PointF(vector1.x * vector2.x, vector1.y * vector2.y);
    }

    public static float getAngle(PointF source, PointF destination) {
        return  (float) -Math.toDegrees(Math.atan2(destination.x - source.x, destination.y - source.y));
    }

    public static float getLength(PointF source, PointF destination) {
        float x = destination.x - source.x;
        float y = destination.y - source.y;

        return (float)Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public static PointF getNormalized(PointF vector){
        float length = getLength(new PointF(0,0), vector);
        PointF normalized = new PointF(vector.x / length, vector.y / length);
        return normalized;
    }

    public static PointF getPerpendicular(PointF vector){
        float y = -vector.x;
        float x = vector.y;
        return new PointF(x, y);
    }
}
