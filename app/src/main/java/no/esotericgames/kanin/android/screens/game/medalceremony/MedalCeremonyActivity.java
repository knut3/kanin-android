package no.esotericgames.kanin.android.screens.game.medalceremony;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.common.Constants;
import no.esotericgames.kanin.android.common.SharedPreferenceKeys;
import no.esotericgames.kanin.android.models.EarnedTarotCard;
import no.esotericgames.kanin.android.models.GameResultModel;
import no.esotericgames.kanin.android.models.ScoreModel;
import no.esotericgames.kanin.android.screens.common.BaseActivity;
import no.esotericgames.kanin.android.screens.common.tarot.TarotCardViewer;
import no.esotericgames.kanin.android.screens.common.dialogs.DialogsEventBus;
import no.esotericgames.kanin.android.screens.common.dialogs.DialogsManager;
import no.esotericgames.kanin.android.screens.common.dialogs.promptdialog.PromptDialogEvent;
import no.esotericgames.kanin.android.screens.common.helpers.ScreensNavigator;
import no.esotericgames.kanin.android.screens.common.tarot.TarotWiz;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedalCeremonyActivity extends BaseActivity implements View.OnClickListener, DialogsEventBus.Listener {

    public static final String EXTRA_KEY_GAME_ID = "gameId";

    private DialogsManager dialogsManager;
    private DialogsEventBus dialogsEventBus;
    private ScreensNavigator screensNavigator;
    private TextView winnerTextView, rabbitTextView;
    private ImageView winnerTarotImageView, rabbitTarotImageView;
    private Button btnMainMenu, btnQuit;
    private EarnedTarotCard earnedWinnerCard, earnedRabbitCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialogsManager = getCompositionRoot().getDialogsManager();
        dialogsEventBus = getCompositionRoot().getDialogsEventBus();
        screensNavigator = getCompositionRoot().getScreensNavigator();

        setContentView(R.layout.activity_medal_ceremony);

        winnerTextView = findViewById(R.id.winnerTextView);
        rabbitTextView = findViewById(R.id.rabbitTextView);
        winnerTarotImageView = findViewById(R.id.winnerTarotImageView);
        rabbitTarotImageView = findViewById(R.id.rabbitTarotImageView);

        btnQuit = findViewById(R.id.btnMedalCeremonyQuit);
        btnMainMenu = findViewById(R.id.btnMedalCeremonyMainMenu);
        btnQuit.setOnClickListener(this);
        btnMainMenu.setOnClickListener(this);

        int gameId = getIntent().getExtras().getInt(EXTRA_KEY_GAME_ID);
        String accessToken = getCompositionRoot().getSharedPreferences()
                        .getString(SharedPreferenceKeys.ACCESS_TOKEN, "");
        getCompositionRoot().getScoreApi().getTotalScores(gameId, accessToken).enqueue(new Callback<GameResultModel>() {
            @Override
            public void onResponse(Call<GameResultModel> call, Response<GameResultModel> response) {
                if (!response.isSuccessful()){
                    showDisconnectedOverlay();
                    return;
                }

                onGotTotalScores(response.body().totalScores);
                for (EarnedTarotCard card : response.body().earnedTarotCards)
                    displayMagicCard(card);
            }

            @Override
            public void onFailure(Call<GameResultModel> call, Throwable t) {
                showDisconnectedOverlay();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        dialogsEventBus.registerListener(this);
    }

    @Override
    protected void onStop() {
        dialogsEventBus.unregisterListener(this);
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        dialogsManager.showGotoMainMenuDialog();
    }

    private void displayMagicCard(EarnedTarotCard card) {

        ImageView cardImageView, surroundingCircleImageView;
        if (card.reason.equals("winner")){
            earnedWinnerCard = card;
            cardImageView = winnerTarotImageView;
            surroundingCircleImageView = findViewById(R.id.ivMedalCeremonyStarCircle);
        }
        else {
            earnedRabbitCard = card;
            cardImageView = rabbitTarotImageView;
            surroundingCircleImageView = findViewById(R.id.ivMedalCeremonyDiamondCircle);
        }

        String path = Constants.BASE_URL + "/" + card.card.relativePath;
        Picasso.get().load(path)
                .resize(740, 1000)
                .centerCrop()
                .into(cardImageView);

        cardImageView.setVisibility(View.VISIBLE);
        cardImageView.setOnClickListener(this);

        surroundingCircleImageView.setVisibility(View.VISIBLE);
        findViewById(R.id.tvMedalCeremonyTarotEarned).setVisibility(View.VISIBLE);
    }

    private void onGotTotalScores(List<ScoreModel> scores){
        List<String> winners = new ArrayList<>(1);
        List<String> rabbits = new ArrayList<>(1);
        int winnerScore;
        int rabbitScore = winnerScore = scores.get(0).score;

        for (ScoreModel model : scores){
            if (model.score == winnerScore){
                winners.add(model.username);
            }
            else if (model.score < winnerScore){
                winnerScore = model.score;
                winners.clear();
                winners.add(model.username);
            }

            if (model.score == rabbitScore){
                rabbits.add(model.username);
            }
            else if (model.score > rabbitScore){
                rabbitScore = model.score;
                rabbits.clear();
                rabbits.add(model.username);
            }
        }

        StringBuilder winnerNames = new StringBuilder();
        StringBuilder rabbitNames = new StringBuilder();

        for (int i = 0; i < winners.size(); i++){
            winnerNames.append(winners.get(i));
            if (i < winners.size()-1)
                winnerNames.append("\n");
        }

        for (int i = 0; i < rabbits.size(); i++){
            rabbitNames.append(rabbits.get(i));
            if (i < rabbits.size()-1)
                rabbitNames.append("\n");
        }

        winnerTextView.setText(winnerNames.toString());
        rabbitTextView.setText(rabbitNames.toString());
    }

    @Override
    public void onClick(View view) {
        if (view == winnerTarotImageView || view == rabbitTarotImageView){
            EarnedTarotCard earnedCard = view == winnerTarotImageView ? earnedWinnerCard : earnedRabbitCard;
            Intent intent = new Intent(this, TarotCardViewer.class);
            intent.putExtra(TarotCardViewer.EXTRA_KEY_CARD_PATH, earnedCard.card.relativePath);
            intent.putExtra(TarotCardViewer.EXTRA_KEY_CARD_NAME, TarotWiz.getName(earnedCard.card));
            startActivity(intent);
        }
        else if (view == btnMainMenu)
            screensNavigator.toMainMenu(false);
        else if (view == btnQuit)
            finishAndRemoveTask();
    }

    @Override
    public void onDialogEvent(Object event) {
        if (event instanceof PromptDialogEvent){
            PromptDialogEvent promptEvent = (PromptDialogEvent) event;
            switch (dialogsManager.getShownDialogTag()){
                case DialogsManager.TAG_GOTO_MAIN_MENU_PROMPT:
                    handleGotoMainMenuDialogEvent(promptEvent);
                    break;
            }
        }
    }

    private void handleGotoMainMenuDialogEvent(PromptDialogEvent event) {
        switch (event.getClickedButton()){
            case POSITIVE:
                screensNavigator.toMainMenu();
                break;
            case NEGATIVE:
        }
    }
}