package no.esotericgames.kanin.android.screens.game.preround;

public class PostLoadText {
    public String mainTitle;
    public String subTitle;
    public int mainTitleWidth;
    public int mainTitleX;
    public int subTitleWidth;
    public int subTitleX;

    public PostLoadText(String mainTitle, String subTitle) {
        this.mainTitle = mainTitle;
        this.subTitle = subTitle;
    }
}
