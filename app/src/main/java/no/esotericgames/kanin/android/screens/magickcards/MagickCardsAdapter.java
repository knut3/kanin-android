package no.esotericgames.kanin.android.screens.magickcards;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.common.Constants;
import no.esotericgames.kanin.android.models.EarnedTarotCardStatistic;
import no.esotericgames.kanin.android.models.TarotCard;

public class MagickCardsAdapter extends RecyclerView.Adapter<MagickCardsAdapter.MyViewHolder>
{

    public interface Listener {

        void onCardClicked(TarotCard card);
    }

    private static final String TAG = "MagickCardsAdapter";
    private final List<EarnedTarotCardStatistic> cards;
    private final MagickCardsAdapter.Listener listener;
    private final LayoutInflater inflater;

    public MagickCardsAdapter(Context context, MagickCardsAdapter.Listener listener) {
        this.listener = listener;
        cards = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MagickCardsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.magick_card_list_item, parent, false);
        return new MagickCardsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MagickCardsAdapter.MyViewHolder holder, int position) {
        final EarnedTarotCardStatistic item = cards.get(position);
        holder.bind(item);
    }

    public void bindCards(List<EarnedTarotCardStatistic> cards){
        Log.d(TAG, "swapItems: ");
        this.cards.clear();
        this.cards.addAll(cards);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        private final ImageView cardImageView;
        private final TextView winnerCountTextView;
        private final TextView rabbitCountTextView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            cardImageView = itemView.findViewById(R.id.imageMagickCardListItem);
            winnerCountTextView = itemView.findViewById(R.id.txtMagickCardListItemWinnerCount);
            rabbitCountTextView = itemView.findViewById(R.id.txtMagickCardListItemRabbitCount);
            cardImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Element " + getAdapterPosition() + " clicked.");
                    if (MagickCardsAdapter.this.listener != null) {
                        TarotCard card = MagickCardsAdapter.this.cards.get(getAdapterPosition()).card;
                        listener.onCardClicked(card);
                    }
                }
            });
        }

        public void bind(EarnedTarotCardStatistic card){
            Picasso.get()
                    .load(Constants.BASE_URL + "/" + card.card.relativePath)
                    .placeholder(R.drawable.tarot_placeholder)
                    .into(cardImageView);
            winnerCountTextView.setText(""+card.reasonWinnerCount);
            rabbitCountTextView.setText(""+card.reasonRabbitCount);
        }

    }
}