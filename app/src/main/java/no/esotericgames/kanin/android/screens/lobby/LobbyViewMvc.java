package no.esotericgames.kanin.android.screens.lobby;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.AddBotModel;
import no.esotericgames.kanin.android.models.BotModel;
import no.esotericgames.kanin.android.models.LobbyPlayer;
import no.esotericgames.kanin.android.screens.common.viewmvc.BaseObservableViewMvc;
import no.esotericgames.kanin.android.screens.common.viewmvc.ViewMvcFactory;

public class LobbyViewMvc extends BaseObservableViewMvc<LobbyViewMvc.Listener> implements View.OnClickListener, LobbyPlayersRecyclerAdapter.Listener {


    public interface Listener{
        void onAddBotClicked(String intelligence);
        void onStartGameClicked();
        void onBotClicked(String name);
    }

    private final TextView tvHeader;
    private final LobbyPlayersRecyclerAdapter adapter;
    private final Button btnStartGame, btnAddDumbBot, btnAddMediumBot, btnCancelAddBotMenu;
    private final ImageButton btnAddBot;
    private final TextView tvPlayerCount;
    private final FrameLayout addBotMenu;
    private final ProgressBar progressLoading;
    public final static int NUM_MAX_PLAYERS = 5;
    private final static String PLAYER_COUNT_POSTFIX_MAX_PLAYERS = "/"+NUM_MAX_PLAYERS;
    private int numPlayers = 0;
    private boolean iAmCreator = false;

    public LobbyViewMvc(LayoutInflater inflater, @Nullable ViewGroup parent,
                        ViewMvcFactory viewMvcFactory) {
        setRootView(inflater.inflate(R.layout.activity_lobby, parent, false));

        addBotMenu = findViewById(R.id.addBotMenu);
        btnAddBot = findViewById(R.id.btnAddBot);
        btnAddDumbBot = addBotMenu.findViewById(R.id.btnBotSkillDumb);
        btnAddMediumBot = addBotMenu.findViewById(R.id.btnBotSkillMedium);
        btnCancelAddBotMenu = addBotMenu.findViewById(R.id.btnCancelBotMenu);
        btnStartGame = findViewById(R.id.btnStartGame);

        btnAddBot.setOnClickListener(this);
        btnAddDumbBot.setOnClickListener(this);
        btnAddMediumBot.setOnClickListener(this);
        btnCancelAddBotMenu.setOnClickListener(this);
        btnStartGame.setOnClickListener(this);

        tvHeader = findViewById(R.id.textLobbyTitle);
        RecyclerView recycler = findViewById(R.id.recyclerLobbyPlayers);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new LobbyPlayersRecyclerAdapter(viewMvcFactory, this);
        recycler.setAdapter(adapter);


        tvPlayerCount = findViewById(R.id.tvLobbyPlayerCount);
        progressLoading = findViewById(R.id.progress_loading);
    }

    public void setHeader(String text){
        tvHeader.setText(text);
    }

    public void bindPlayers(List<LobbyPlayer> players){
        adapter.bindPlayers(players);
        numPlayers = players.size();
        updatePlayerCount();
        progressLoading.setVisibility(View.INVISIBLE);
    }

    public void addPlayer(LobbyPlayer model){
        adapter.addItem(model);
        numPlayers++;
        updatePlayerCount();
        hideAddBotButtonIfApplicable();
    }

    public void removePlayer(String username){
        adapter.removeItem(username);
        numPlayers--;
        updatePlayerCount();
        if (iAmCreator && numPlayers == NUM_MAX_PLAYERS-1)
            btnAddBot.setVisibility(View.VISIBLE);
        progressLoading.setVisibility(View.INVISIBLE);
    }

    public void addBot(BotModel bot) {
        LobbyPlayer botViewModel = new LobbyPlayer(bot);
        botViewModel.intelligence = getLocalizedBotSkillLevel(bot.intelligence);
        adapter.addItem(botViewModel);
        numPlayers++;
        updatePlayerCount();
        hideAddBotButtonIfApplicable();
        progressLoading.setVisibility(View.INVISIBLE);
    }

    public void iAmCreator(){
        this.iAmCreator = true;
        btnStartGame.setVisibility(View.VISIBLE);
        btnAddBot.setVisibility(View.VISIBLE);
        adapter.iAmCreator();
    }

    public void setStartButtonEnabled(boolean enabled){
        btnStartGame.setEnabled(enabled);
    }

    @Override
    public void onClick(View view) {
        if (view == btnAddBot) addBotMenu.setVisibility(View.VISIBLE);
        else if (view == btnCancelAddBotMenu) addBotMenu.setVisibility(View.INVISIBLE);
        else if (view == btnAddDumbBot){
            for (Listener l : getListeners())
                l.onAddBotClicked(AddBotModel.DUMB);
            addBotMenu.setVisibility(View.INVISIBLE);
            progressLoading.setVisibility(View.VISIBLE);
        }
        else if (view == btnAddMediumBot){
            for (Listener l : getListeners())
                l.onAddBotClicked(AddBotModel.MEDIUM);
            addBotMenu.setVisibility(View.INVISIBLE);
            progressLoading.setVisibility(View.VISIBLE);
        }
        else if (view == btnStartGame){
            for (Listener l : getListeners())
                l.onStartGameClicked();
        }
    }

    @Override
    public void onBotClicked(String name) {
        for (Listener l : getListeners())
            l.onBotClicked(name);
    }


    private void updatePlayerCount(){
        String currentCount = ""+numPlayers;

        if (numPlayers < 3)
            currentCount = "<font color='#F07470'>" + numPlayers + "</font>";

        tvPlayerCount.setText(Html.fromHtml(
                currentCount+PLAYER_COUNT_POSTFIX_MAX_PLAYERS,
                Html.FROM_HTML_MODE_COMPACT));
    }

    private void hideAddBotButtonIfApplicable(){
        if (iAmCreator && numPlayers == NUM_MAX_PLAYERS)
            btnAddBot.setVisibility(View.INVISIBLE);
    }

    private String getLocalizedBotSkillLevel(String intelligence){
        if (intelligence.equals(BotModel.SKILL_LOW))
            return getContext().getString(R.string.botSkillDumb);
        else return getContext().getString(R.string.botSkillMedium);
    }

}
