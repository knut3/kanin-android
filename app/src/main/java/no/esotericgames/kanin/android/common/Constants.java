package no.esotericgames.kanin.android.common;

public class Constants {
    public static final String BASE_URL = "https://kanin.esotericgames.no:88";
    public static final String GAME_HUB_URL = BASE_URL + "/gameHub";
    public static final String ACRARIUM_URL = "https://acrarium.esotericgames.no:88/report";
}
