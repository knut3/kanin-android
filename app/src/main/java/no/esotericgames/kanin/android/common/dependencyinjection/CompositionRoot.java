package no.esotericgames.kanin.android.common.dependencyinjection;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import no.esotericgames.kanin.android.common.Constants;
import no.esotericgames.kanin.android.networking.halfduplex.ScoreAPI;
import no.esotericgames.kanin.android.networking.halfduplex.StatisticsAPI;
import no.esotericgames.kanin.android.networking.halfduplex.UserAPI;
import no.esotericgames.kanin.android.screens.common.dialogs.DialogsEventBus;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CompositionRoot {

    private Retrofit mRetrofit;
    private DialogsEventBus mDialogsEventBus;

    private Retrofit getRetrofit() {
        if (mRetrofit == null) {
            Gson gson = new GsonBuilder().setLenient().create();
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return mRetrofit;
    }

    public UserAPI getUserApi() {
        return getRetrofit().create(UserAPI.class);
    }

    public ScoreAPI getScoreApi(){
        return getRetrofit().create(ScoreAPI.class);
    }

    public StatisticsAPI getStatisticsApi() {
        return getRetrofit().create(StatisticsAPI.class);
    }

    public DialogsEventBus getDialogsEventBus() {
        if (mDialogsEventBus == null) {
            mDialogsEventBus = new DialogsEventBus();
        }
        return mDialogsEventBus;
    }
}
