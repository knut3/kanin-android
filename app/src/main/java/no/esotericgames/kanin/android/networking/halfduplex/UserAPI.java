package no.esotericgames.kanin.android.networking.halfduplex;

import no.esotericgames.kanin.android.models.LoginModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserAPI {

    @GET("user/is-available")
    Call<Boolean> isAvailable(@Query("userName") String userName);

    @GET("user/is-valid-token")
    Call<Boolean> isValidToken(@Query("accessToken") String accessToken);

    @POST("user/register")
    Call<String> register(@Body LoginModel model);

    @POST("user/login")
    Call<String> login(@Body LoginModel model);

}
