package no.esotericgames.kanin.android.networking;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class NetworkStateService extends Service {
    public static boolean isNetworkConnected = false;
    private final IBinder binder = new NetworkStateService.LocalBinder();
    private final List<Listener> listeners = new ArrayList<>(2);
    private ConnectivityManager connectivityManager;

    public interface Listener{
        void onConnectionLost();
        void onConnectionAvailable();
    }

    public class LocalBinder extends Binder {
        public void addListener(Listener listener){
            listeners.add(listener);
        }
        public void removeListener(Listener listener){
            listeners.remove(listener);
        }
    }

    private final ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback(){
        @Override
        public void onAvailable(Network network) {
            if (!isNetworkConnected)
                for (Listener l : listeners)
                    l.onConnectionAvailable();

            isNetworkConnected = true;

        }
        @Override
        public void onLost(Network network) {
            if (isNetworkConnected)
                for (Listener l : listeners)
                    l.onConnectionLost();

            isNetworkConnected = false;
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            connectivityManager = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            connectivityManager.registerDefaultNetworkCallback(networkCallback);
            isNetworkConnected = isInternetAvailable();
        }catch (Exception e){
            isNetworkConnected = false;
        }
    }

    @Override
    public void onDestroy() {
        connectivityManager.unregisterNetworkCallback(networkCallback);
        super.onDestroy();
    }

    private boolean isInternetAvailable() {
        try {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Exception e) {

            Log.e("isInternetAvailable:",e.toString());
            return false;
        }
    }
}
