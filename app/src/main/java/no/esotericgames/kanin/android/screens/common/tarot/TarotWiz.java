package no.esotericgames.kanin.android.screens.common.tarot;

import no.esotericgames.kanin.android.models.TarotCard;

public class TarotWiz {
    public static final String MajorArcana = "Major";
    public static final String MinorArcana = "Minor";

    public static String getName(TarotCard card){
        if (card.arcana.equals(MajorArcana))
            return card.name;

        return minorNumberToText(card.value)
                + " of " + card.suit;
    }

    private static String minorNumberToText(int value){
        switch (value){
            case 1 : return "Ace";
            case 2 : return "Two";
            case 3 : return "Three";
            case 4 : return "Four";
            case 5 : return "Five";
            case 6 : return "Six";
            case 7 : return "Seven";
            case 8 : return "Eight";
            case 9 : return "Nine";
            case 10 : return "Ten";
            case 11 : return "Page";
            case 12 : return "Knight";
            case 13 : return "Queen";
            default : return "King";
        }
    }

}
