package no.esotericgames.kanin.android.screens.game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.view.MotionEvent;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.CardPlayedAndTrickEndedModel;
import no.esotericgames.kanin.android.models.CardPlayedModel;
import no.esotericgames.kanin.android.models.GameEndedAfterKabal;
import no.esotericgames.kanin.android.models.GameEndedAfterTrickRound;
import no.esotericgames.kanin.android.models.GameState;
import no.esotericgames.kanin.android.models.KabalPassModel;
import no.esotericgames.kanin.android.models.KabalRoundEnded;
import no.esotericgames.kanin.android.models.Round;
import no.esotericgames.kanin.android.models.TrickRoundEnded;
import no.esotericgames.kanin.android.models.Card;
import no.esotericgames.kanin.android.models.Player;
import no.esotericgames.kanin.android.models.Rounds;
import no.esotericgames.kanin.android.screens.common.ResourceManager;
import no.esotericgames.kanin.android.screens.common.helpers.ToastMaster;
import no.esotericgames.kanin.android.screens.game.common.InfoDrawerContentUpdater;
import no.esotericgames.kanin.android.screens.game.common.ViewAnimator;
import no.esotericgames.kanin.android.screens.game.preround.PostLoadText;
import no.esotericgames.kanin.android.screens.game.preround.PreRoundView;
import no.esotericgames.kanin.android.screens.game.rounds.common.RoundView;
import no.esotericgames.kanin.android.screens.game.rounds.kabal.KabalRoundView;
import no.esotericgames.kanin.android.screens.game.rounds.kabal.SpotLightView;
import no.esotericgames.kanin.android.screens.game.rounds.trick.TrickRoundView;
import no.esotericgames.kanin.android.screens.game.rounds.trick.TrumpRoundView;
import no.esotericgames.kanin.android.screens.game.turnindicator.TurnIndicatorAnimator;

public class GameView extends View implements PreRoundView.Listener, RoundView.Listener {

    private static final int SECONDS_BETWEEN_ROUNDS = 5;
    private static final int DEFAULT_ANNOUNCEMENT_DURATION_MS = 5000;
    private static final int KABAL_ANNOUNCEMENT_DURATION_MS = 7000;

    public interface Listener{
        void onPreRoundViewEnded();
        void onMyCardThrown(Card card);
        void onRoundFinished();
        void onValidKabalPassClick();
        void onTrumpSuitDrawn(int index);
        void onGameFinished(int gameId);
        void showAnnouncement(String text);
        void hideAnnouncement();
    }

    private enum ScreenState { PRE_ROUND, PLAYING }

    private final ResourceManager resourceManager;
    private final Handler uiHandler;

    private final float screenDensity;
    private final ToastMaster toastMaster;

    // states
    private boolean hasGameState = false;
    private boolean hasScreenSize = false;
    private ScreenState screenState = ScreenState.PRE_ROUND;

    private GameState gameState;
    private Point screenSize;

    private final Player me;
    private PreRoundView preRoundView;
    private RoundView roundView;
    private final String nextRoundTimerText;
    private final Listener listener;

    public GameView(Context context, boolean isNewRound, Player me, Listener gameListener) {
        super(context);
        this.me = me;
        this.listener = gameListener;
        resourceManager = new ResourceManager(context);
        toastMaster = new ToastMaster(context, resourceManager);
        screenDensity = context.getResources().getDisplayMetrics().density;
        nextRoundTimerText = resourceManager.getString(R.string.countdown_between_rounds);
        uiHandler = new Handler(Looper.getMainLooper());
        preRoundView = new PreRoundView(isNewRound, resourceManager.getColor(R.color.light_green),
                this, "");
        preRoundView.registerListener(this);
    }

    public void setGameState(GameState gameState){
        this.gameState = gameState;
        hasGameState = true;
        tryToCreateViews();
        setRoundIntroText(gameState.round);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w == 0 && h == 0) return;
        screenSize = new Point(w, h);
        preRoundView.setScreenDimensions(w, h);

        if (!hasScreenSize){
            hasScreenSize = true;
            tryToCreateViews();
        }

        hasScreenSize = true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (hasGameState) roundView.draw(canvas);

        if (screenState == ScreenState.PRE_ROUND)
            preRoundView.draw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (screenState != ScreenState.PLAYING) return true;

        float x = event.getX();
        float y = event.getY();

        if (event.getAction() != MotionEvent.ACTION_DOWN)
            return super.onTouchEvent(event);

        if (Rounds.Kabal.equals(gameState.round.name)){
            performPassIfValid(x, y);
        }
        else if (Rounds.Trumf.equals(gameState.round.name)){
            performTrumpSuitDrawIfValid(x, y);
        }

        roundView.onTouchDownEvent(x, y);

        return true;
    }

    public String getNextToPlay() {
        return gameState.nextToPlay;
    }

    private void performTrumpSuitDrawIfValid(float x, float y) {
        if (!gameState.playerToPerformTrumpDraw.equals(me.getUsername()))
            return;

        TrumpRoundView trumpRoundView = (TrumpRoundView) roundView;
        if (!trumpRoundView.hasRoundStarted()){
            Card cardHit = trumpRoundView.getTrumpDrawHit(x, y);
            if (cardHit != null) {
                listener.onTrumpSuitDrawn(cardHit.value);
            }
        }
    }

    private void performPassIfValid(float x, float y) {
        KabalRoundView kabalRoundView = (KabalRoundView) roundView;
        if (kabalRoundView.isValidPassButtonClick(x, y)) {
            listener.onValidKabalPassClick();
        }
    }

    //------------------- Pre-Round View Events------------------------------------

    @Override
    public void preRoundViewEnded() {
        screenState = ScreenState.PLAYING;
        roundView.announceFirstToPlay(gameState.nextToPlay, gameState.round.hasStarted);
        listener.onPreRoundViewEnded();
    }

    //------------------- Round View Events----------------------------------------

    @Override
    public void onMyCardThrown(Card card) {

        if (gameState.round.name.equals(Rounds.Kabal)){
            roundView.addRoundScore(-1, me.getUsername());
            if (card.value == 1)
                card.value = 14;
        }
        listener.onMyCardThrown(card);
    }

    //------------------- Server Events--------------------------------------------

    public void cardPlayed(CardPlayedModel model) {
        roundView.onCardPlayed(model);
    }

    public void kabalPass(KabalPassModel model) {
        ((KabalRoundView) roundView).pass(model);
    }

    public void cardPlayedAndTrickEnded(CardPlayedAndTrickEndedModel model) {
        roundView.setNextToPlay(model.trickResult.winner, 0);
        Runnable collectTrick = ()-> ((TrickRoundView)roundView)
                .collectTrick(model.trickResult, null);

        if (!model.card.owner.equals(me.getUsername()))
            roundView.throwOpponentCard(model.card, collectTrick);
        else
            collectTrick.run();
    }

    public void trickRoundEnded(TrickRoundEnded model, Runnable endGameAction) {
        Runnable endAction = endGameAction == null
                ? this::endRound
                : endGameAction;

        Runnable showAnnouncementIfNeeded = model.endedCuzNoPointCardsLeft
                ? ()-> {
                        String text = getTrickRoundEndedEarlyAnnouncement();
                        createAnnouncement(text, DEFAULT_ANNOUNCEMENT_DURATION_MS, endAction);
                    }
                : endAction;

        Runnable collectTrick = ()-> {
            roundView.clearNextToPlay();
            postInvalidate();
            ((TrickRoundView)roundView).collectTrick(model.trickResult, showAnnouncementIfNeeded);
        };

        if (!model.cardPlayed.owner.equals(me.getUsername()))
            roundView.throwOpponentCard(model.cardPlayed, collectTrick);
        else
            collectTrick.run();

    }

    public void kabalRoundEnded(KabalRoundEnded model, Runnable endGameAction) {
        Runnable endAction = endGameAction == null
                ? this::endRound
                : endGameAction;

        Runnable endThisRound = ()-> {
            roundView.clearNextToPlay();
            postInvalidate();
            String roundName = resourceManager
                    .getString(InfoDrawerContentUpdater.getRoundNameStringResId(Rounds.Kabal));
            String announcementText = resourceManager
                    .getString(R.string.announcement_on_kabal_end, model.card.owner, roundName);
            createAnnouncement(announcementText, KABAL_ANNOUNCEMENT_DURATION_MS,
                    ()-> ((KabalRoundView) roundView).endKabalRound(endAction)
            );
        };

        if (!model.card.owner.equals(me.getUsername())) {
            roundView.onOpponentThrowingCard(model.card.owner);
            roundView.throwOpponentCard(model.card, endThisRound);
        }
        else
            endThisRound.run();
    }

    public void trumpSuitSelected(String trumpSuit, int pickedIndex){
        TrumpRoundView trumpRoundView = (TrumpRoundView) roundView;
        trumpRoundView.trumpSuitSelected(trumpSuit, pickedIndex);
        invalidate();
    }


    public void playerConnected(String username) {
        if (!me.getUsername().equals(username)) {
            if (roundView != null) {
                roundView.playerConnected(username);
                invalidate();
            }
        }
    }

    public void playerDisconnected(String username) {
        if (!me.getUsername().equals(username)) {
            if (roundView != null) {
                roundView.playerDisconnected(username);
                invalidate();
            }
        }
    }

    public void gameEndedAfterTrickRound(GameEndedAfterTrickRound model) {
        trickRoundEnded(model, ()-> listener.onGameFinished(model.gameId));
    }

    public void gameEndedAfterKabal(GameEndedAfterKabal model) {
        kabalRoundEnded(model, ()-> listener.onGameFinished(model.gameId));
    }


    //----------Private Methods---------------------------

    private void endRound(){
        listener.showAnnouncement(nextRoundTimerText + " " + SECONDS_BETWEEN_ROUNDS);
        new CountDownTimer(SECONDS_BETWEEN_ROUNDS*1000, 1000) {

            public void onTick(long millisUntilFinished) {
                listener.showAnnouncement(nextRoundTimerText + " " + (int)(millisUntilFinished/1000+1));
            }

            public void onFinish() {
                listener.hideAnnouncement();
                preRoundView = new PreRoundView(true, resourceManager.getColor(R.color.light_green),
                        GameView.this, "");
                preRoundView.registerListener(GameView.this);
                preRoundView.setScreenDimensions(screenSize.x, screenSize.y);
                screenState = ScreenState.PRE_ROUND;
                invalidate();
                listener.onRoundFinished();
                this.cancel();
            }
        }.start();
    }

    private void tryToCreateViews(){
        if (!hasGameState || !hasScreenSize) return;

        me.setNumCardsOnHand(gameState.yourCards.size());
        ViewAnimator viewAnimator = new ViewAnimator(this, 300);
        TurnIndicatorAnimator turnIndicatorAnimator = new TurnIndicatorAnimator(this, screenDensity);

        if (gameState.round.name.equals(Rounds.Kabal)) {
            SpotLightView spotLightView = new SpotLightView(resourceManager.getColor(R.color.black_overlay), this);
            roundView = new KabalRoundView(screenSize, gameState, me, viewAnimator,
                    turnIndicatorAnimator, resourceManager, toastMaster, spotLightView);
        }
        else if (gameState.round.name.equals(Rounds.Trumf))
            roundView = new TrumpRoundView(screenSize, gameState, me, viewAnimator, turnIndicatorAnimator, resourceManager, toastMaster);
        else
            roundView = new TrickRoundView(screenSize, gameState, me, viewAnimator, turnIndicatorAnimator, resourceManager, toastMaster);

        roundView.registerListener(this);
        invalidate();
    }

    private void createAnnouncement(String text, int duration_ms, Runnable onEnd){
        listener.showAnnouncement(text);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                uiHandler.post(()-> {
                    listener.hideAnnouncement();
                    if (onEnd != null) onEnd.run();
                });
            }
        }, duration_ms);
    }

    private String getTrickRoundEndedEarlyAnnouncement(){
        if (gameState.round.name.equals(Rounds.Spar))
            return resourceManager.getString(R.string.announcement_on_spar_ended_early);
        else return resourceManager.getString(R.string.announcement_on_damer_ended_early);
    }

    private void setRoundIntroText(Round round) {
        int roundNameResId = InfoDrawerContentUpdater.getRoundNameStringResId(round.name);
        String localizedRoundName = resourceManager.getString(roundNameResId);
        String mainTitle = resourceManager.getString(R.string.round_header,
                round.number, localizedRoundName);
        int shortDescriptionResId = InfoDrawerContentUpdater.getShortDescriptionResId(round.name);
        String localizedShortDescription = resourceManager.getString(shortDescriptionResId);
        PostLoadText model = new PostLoadText(mainTitle, localizedShortDescription);
        preRoundView.setPostLoadText(model);
    }
}
