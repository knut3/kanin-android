package no.esotericgames.kanin.android.networking.duplex;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;

import no.esotericgames.kanin.android.models.GetOpenLobbyModel;
import no.esotericgames.kanin.android.models.GetRunningGameModel;

public class LaunchRepository extends BaseSignalRRepository<BaseSignalRRepository.Listener> {

    public LaunchRepository(Context bindingContext, Handler uiHandler) {
        super(bindingContext, uiHandler);
    }

    public String getRunningGameBlocking(){
        try {
            GetRunningGameModel result = hubConnection.invoke(GetRunningGameModel.class, "GetRunningGame")
                    .retry(RETRY_COUNT)
                    .blockingGet();

            return result.foundResult ? result.gameName : null;
        }
        catch (Exception e){
            super.logError(e);
            return null;
        }
    }

    public GetOpenLobbyModel getOpenLobbyBlocking(){
        try{
            GetOpenLobbyModel result = hubConnection.invoke(GetOpenLobbyModel.class, "GetOpenLobby")
                    .retry(RETRY_COUNT)
                    .blockingGet();

            return result.foundResult ? result : null;

        }
        catch (Exception e){
            logError(e);
            return null;
        }
    }
}
