package no.esotericgames.kanin.android.networking.halfduplex;

import no.esotericgames.kanin.android.models.AveragePlacementLeaderboardModel;
import no.esotericgames.kanin.android.models.GameResultModel;
import no.esotericgames.kanin.android.models.LowestScoreLeaderboardModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ScoreAPI {

    @GET("score/total")
    Call<GameResultModel> getTotalScores(@Query("gameId") int gameId, @Query("accessToken") String accessToken);

    @GET("score/leaderboard/average-placement")
    Call<AveragePlacementLeaderboardModel> getAveragePlacementLeaderboard(@Query("numResults") int numResults, @Query("accessToken") String accessToken);

    @GET("score/leaderboard/lowest-score")
    Call<LowestScoreLeaderboardModel> getLowestScoreLeaderboard(@Query("numResults") int numResults, @Query("accessToken") String accessToken);
}
