package no.esotericgames.kanin.android.screens.leaderboard;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.common.SharedPreferenceKeys;
import no.esotericgames.kanin.android.models.AveragePlacementLeaderboardModel;
import no.esotericgames.kanin.android.models.AveragePlacementModel;
import no.esotericgames.kanin.android.models.LowestScoreLeaderboardModel;
import no.esotericgames.kanin.android.models.LowestScoreModel;
import no.esotericgames.kanin.android.screens.common.BaseActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LeaderboardActivity extends BaseActivity {

    public static final int NUM_RESULTS_AVERAGE_PLACEMENT = 3;
    private RecyclerView avgPlaceRecycler;
    private RecyclerView lowestScoresRecycler;
    private TextView averagePlacementSubHeader;
    private ProgressBar averagePlacementLoadingProgress;
    private ProgressBar lowestScoresLoadingProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);
        avgPlaceRecycler = findViewById(R.id.recycler_leaderboard_average_placement);
        lowestScoresRecycler = findViewById(R.id.recycler_leaderboard_lowest_score);
        averagePlacementSubHeader = findViewById(R.id.txtAveragePlacementSubHeader);
        averagePlacementLoadingProgress = findViewById(R.id.progress_avg_place_loading);
        lowestScoresLoadingProgress = findViewById(R.id.progress_lowest_score_loading);

    }

    @Override
    protected void onStart() {
        super.onStart();
        String accessToken = getCompositionRoot().getSharedPreferences().getString(SharedPreferenceKeys.ACCESS_TOKEN, "");
        getCompositionRoot().getScoreApi().getAveragePlacementLeaderboard(NUM_RESULTS_AVERAGE_PLACEMENT, accessToken).enqueue(new Callback<AveragePlacementLeaderboardModel>() {
            @Override
            public void onResponse(Call<AveragePlacementLeaderboardModel> call, Response<AveragePlacementLeaderboardModel> response) {
                if (!response.isSuccessful()){
                    getCompositionRoot().getToastMaster().showFailureToast(getString(R.string.login_error_unknown));
                    return;
                }
                showAveragePlacementLeaderboard(response.body());
            }

            @Override
            public void onFailure(Call<AveragePlacementLeaderboardModel> call, Throwable t) {
                getCompositionRoot().getToastMaster().showFailureToast(getString(R.string.login_error_unknown));
            }
        });

        getCompositionRoot().getScoreApi().getLowestScoreLeaderboard(NUM_RESULTS_AVERAGE_PLACEMENT, accessToken).enqueue(new Callback<LowestScoreLeaderboardModel>() {
            @Override
            public void onResponse(Call<LowestScoreLeaderboardModel> call, Response<LowestScoreLeaderboardModel> response) {
                if (!response.isSuccessful()){
                    getCompositionRoot().getToastMaster().showFailureToast(getString(R.string.login_error_unknown));
                    return;
                }
                showLowestScoresLeaderboard(response.body());
            }

            @Override
            public void onFailure(Call<LowestScoreLeaderboardModel> call, Throwable t) {
                getCompositionRoot().getToastMaster().showFailureToast(getString(R.string.login_error_unknown));
            }
        });
    }

    private void showAveragePlacementLeaderboard(AveragePlacementLeaderboardModel model) {
        averagePlacementSubHeader.setText(getString(R.string.average_placement_sub_header, model.minimumGamesPlayed));
        List<AveragePlacementModel> leaderboard = new ArrayList<>(Arrays.asList(model.leaderboard));
        if (model.you != null && model.you.ranking > NUM_RESULTS_AVERAGE_PLACEMENT)
            leaderboard.add(model.you);
        AveragePlacementsRecyclerAdapter avgPlaceAdapter = new AveragePlacementsRecyclerAdapter(LeaderboardActivity.this, leaderboard);
        avgPlaceRecycler.setAdapter(avgPlaceAdapter);
        averagePlacementLoadingProgress.setVisibility(View.GONE);
    }

    private void showLowestScoresLeaderboard(LowestScoreLeaderboardModel model) {
        List<LowestScoreModel> leaderboard = new ArrayList<>(Arrays.asList(model.leaderboard));
        if (model.you != null && model.you.ranking > NUM_RESULTS_AVERAGE_PLACEMENT)
            leaderboard.add(model.you);
        LowestScoresRecyclerAdapter lowestScoresAdapter = new LowestScoresRecyclerAdapter(LeaderboardActivity.this, leaderboard);
        lowestScoresRecycler.setAdapter(lowestScoresAdapter);
        lowestScoresLoadingProgress.setVisibility(View.GONE);
    }
}