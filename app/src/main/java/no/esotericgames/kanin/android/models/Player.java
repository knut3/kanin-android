package no.esotericgames.kanin.android.models;

public class Player {
    private String username;
    private int numCardsOnHand;

    public Player(String username) {
        this.username = username;
    }

    public Player(String username, int numCardsOnHand) {
        this.username = username;
        this.numCardsOnHand = numCardsOnHand;
    }

    public String getUsername(){
        return username;
    }

    public void setNumCardsOnHand(int numCardsOnHand) {
        this.numCardsOnHand = numCardsOnHand;
    }
    public int getNumCardsOnHand() {
        return numCardsOnHand;
    }
}
