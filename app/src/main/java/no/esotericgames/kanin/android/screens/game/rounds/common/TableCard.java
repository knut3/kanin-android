package no.esotericgames.kanin.android.screens.game.rounds.common;


import no.esotericgames.kanin.android.screens.game.rounds.common.CardView;

public class TableCard {
    private CardView cardView;
    private String owner;

    public TableCard(CardView cardView, String owner) {
        this.cardView = cardView;
        this.owner = owner;
    }

    public CardView getCardView(){
        return cardView;
    }

    public String getOwner(){
        return this.owner;
    }
}
