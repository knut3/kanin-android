package no.esotericgames.kanin.android.screens.leaderboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.AveragePlacementModel;
import no.esotericgames.kanin.android.models.LowestScoreModel;

public class LowestScoresRecyclerAdapter extends RecyclerView.Adapter<LowestScoresRecyclerAdapter.MyViewHolder> {

    static class MyViewHolder extends RecyclerView.ViewHolder{

        public final TextView txtRanking, txtUsername, txtScore;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtRanking = itemView.findViewById(R.id.txt_leaderboard_ranking);
            txtUsername = itemView.findViewById(R.id.txtUsername);
            txtScore = itemView.findViewById(R.id.txt_leaderboard_lowest_score);
        }

    }

    private final List<LowestScoreModel> lowestScores;
    private final LayoutInflater inflater;

    public LowestScoresRecyclerAdapter(Context context, List<LowestScoreModel> data) {
        this.inflater = LayoutInflater.from(context);
        this.lowestScores = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.lowest_score_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LowestScoreModel item = lowestScores.get(position);
        holder.txtRanking.setText(""+item.ranking);
        if (item.ranking == 1)
            holder.txtRanking.setBackgroundResource(R.drawable.circle_solid_gold_stroke_black);
        if (item.ranking == 2)
            holder.txtRanking.setBackgroundResource(R.drawable.circle_solid_silver_stroke_black);
        if (item.ranking == 3)
            holder.txtRanking.setBackgroundResource(R.drawable.circle_solid_bronze_stroke_black);
        holder.txtUsername.setText(item.username);
        holder.txtScore.setText(""+item.score);
    }

    @Override
    public int getItemCount() {
        return lowestScores.size();
    }
}
