package no.esotericgames.kanin.android.screens.availablelobbies;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.AvailableLobbyModel;
import no.esotericgames.kanin.android.screens.common.viewmvc.BaseObservableViewMvc;
import no.esotericgames.kanin.android.screens.common.viewmvc.ViewMvcFactory;

public class LobbiesViewMvc extends BaseObservableViewMvc<LobbiesViewMvc.Listener>
    implements LobbiesRecyclerAdapter.Listener{

    public interface Listener{
        void onLobbyClicked(String gameName);
    }

    private final LobbiesRecyclerAdapter adapter;
    private final TextView txtNoAvailableLobbies;
    private final ProgressBar progressLoading;

    public LobbiesViewMvc(LayoutInflater inflater, @Nullable ViewGroup parent,
                          ViewMvcFactory viewMvcFactory) {
        setRootView(inflater.inflate(R.layout.activity_available_games, parent, false));

        RecyclerView recycler = findViewById(R.id.recycler_lobbies);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new LobbiesRecyclerAdapter(this, viewMvcFactory);
        recycler.setAdapter(adapter);
        txtNoAvailableLobbies = findViewById(R.id.txtNoAvailableGames);
        progressLoading = findViewById(R.id.progress_loading);
    }

    public void bindLobbies(List<AvailableLobbyModel> lobbies){
        if (lobbies.size() == 0)
            txtNoAvailableLobbies.setVisibility(View.VISIBLE);

        else adapter.bindLobbies(lobbies);

        progressLoading.setVisibility(View.GONE);
    }

    public void addLobby(AvailableLobbyModel lobby){
        if (adapter.getItemCount() == 0)
            txtNoAvailableLobbies.setVisibility(View.INVISIBLE);

        adapter.addItem(lobby);
    }

    public void removeLobby(String gameName){
        adapter.removeItem(gameName);

        if (adapter.getItemCount() == 0)
            txtNoAvailableLobbies.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLobbyClicked(String gameName) {
        for (Listener l : getListeners()){
            l.onLobbyClicked(gameName);
        }
    }
}
