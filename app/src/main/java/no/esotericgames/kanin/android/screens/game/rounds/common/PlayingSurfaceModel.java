package no.esotericgames.kanin.android.screens.game.rounds.common;

import android.graphics.PointF;

public class PlayingSurfaceModel {
    public float top, bottom;
    public PointF center;

    public PlayingSurfaceModel(float top, float bottom, PointF center) {
        this.top = top;
        this.bottom = bottom;
        this.center = center;
    }
}
