package no.esotericgames.kanin.android.models;

public class Rounds {
    public static final String Pass = "Pass";
    public static final String Spar = "Spar";
    public static final String Damer = "Damer";
    public static final String Kabal = "Kabal";
    public static final String Grand = "Grand";
    public static final String Trumf = "Trumf";
}
