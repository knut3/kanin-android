package no.esotericgames.kanin.android.screens.game.common;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageButton;

public class InfoButtonAttentionizer {

    public static final int PULSE_DURATION_MS = 800;
    public static final int REPEAT_COUNT = 4;
    private final ImageButton infoButton;
    private final int attentionColor;
    private int repeats;

    public InfoButtonAttentionizer(ImageButton infoButton, int attentionColor) {
        this.attentionColor = attentionColor;
        this.infoButton = infoButton;
    }

    public void drawAttention(){
        repeats = 0;
        AnimatorSet set = new AnimatorSet();
        set.playSequentially(
                getColorAnimator(Color.WHITE, attentionColor),
                getColorAnimator(attentionColor, Color.WHITE));
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (repeats < REPEAT_COUNT){
                    repeats++;
                    animation.start();
                }
            }
        });
        set.start();
    }

    private ValueAnimator getColorAnimator(int fromColor, int toColor){
        ValueAnimator colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), fromColor, toColor);
        colorAnimation.setDuration(PULSE_DURATION_MS/2); // milliseconds
        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                int currentColor = (int) animator.getAnimatedValue();
                infoButton.setBackgroundTintList(ColorStateList.valueOf(currentColor));
            }

        });
        return colorAnimation;
    }
}
