package no.esotericgames.kanin.android.screens.game.rounds.common;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;

import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;

public class TableView implements IDrawable {

    private static final float MARGIN_PERCENTAGE_TOP = 0.16f;
    private static final float MARGIN_PERCENTAGE_BOTTOM = 0.08f;
    private static final float MARGIN_PERCENTAGE_X = 0.08f;
    private final RectF rect;
    private final float curveRadius;
    private Paint fillPaint;
    private Paint strokePaint;
    private final float strokeWidth;
    private final Path path;

    public TableView(PointF allottedSpace) {
        float shrinkFactor_y = 1f - MARGIN_PERCENTAGE_TOP - MARGIN_PERCENTAGE_BOTTOM;
        float shrinkFactor_x = 1f - MARGIN_PERCENTAGE_X * 2;
        float width = allottedSpace.x * shrinkFactor_x;
        float height = allottedSpace.y * shrinkFactor_y;
        float x = allottedSpace.x * MARGIN_PERCENTAGE_X;
        float y = allottedSpace.y * MARGIN_PERCENTAGE_TOP;
        rect = new RectF(x, y, x+width, y+height);
        curveRadius = height / 2;
        strokeWidth = height / 11;
        createPaint(strokeWidth);
        path = createRoundRect(rect, curveRadius);
    }

    private Path createRoundRect(RectF rect, float curveRadius){
        //Path for a rounded rectangle.
        Path path = new Path();
        path.moveTo(rect.left + curveRadius, rect.top);
        path.lineTo(rect.right - curveRadius, rect.top);
        path.quadTo(rect.right, rect.top, rect.right, rect.top + curveRadius);
        path.lineTo(rect.right, rect.bottom-curveRadius);
        path.quadTo(rect.right, rect.bottom, rect.right-curveRadius, rect.bottom);
        path.lineTo(rect.left+curveRadius, rect.bottom);
        path.quadTo(rect.left, rect.bottom, rect.left, rect.bottom-curveRadius);
        path.lineTo(rect.left, rect.top + curveRadius);
        path.quadTo(rect.left, rect.top, rect.left+curveRadius, rect.top);
        return path;
    }

    @Override
    public void draw(Canvas canvas) {
        //canvas.drawRoundRect(rect, curveRadius, curveRadius, fillPaint);
        canvas.drawPath(path, fillPaint);
        canvas.drawPath(path, strokePaint);
        canvas.drawRoundRect(rect, curveRadius, curveRadius, strokePaint);
    }

    public PointF getMyScoresCenter(float scoreBoardHeight){
        float x = rect.centerX();
        float y = rect.bottom - strokeWidth - scoreBoardHeight / 2;
        return new PointF(x, y);
    }

    public PointF getSeatWest(){
        float radius = curveRadius;
        double curve_x = radius * Math.cos(Math.toRadians(45));
        double curve_y = radius * Math.sin(Math.toRadians(45));

        float x = rect.left + curveRadius - (float)curve_x;
        float y = rect.top + curveRadius - (float)curve_y;

        return new PointF(x, y);
    }

    public PointF getSeatNorthWest(){
        float straightEdgeWidth = rect.width() - 2 * curveRadius;
        float oneForth = straightEdgeWidth / 4;
        float x = rect.left + curveRadius + oneForth;
        float y = rect.top;
        return new PointF(x, y);
    }

    public PointF getSeatNorthEast(){
        float straightEdgeWidth = rect.width() - 2 * curveRadius;
        float oneForth = straightEdgeWidth / 4;
        float x = rect.right - curveRadius - oneForth;
        float y = rect.top;
        return new PointF(x, y);
    }

    public PointF getSeatNorth(){
        return new PointF(rect.centerX(), rect.top);
    }

    public PointF getSeatEast(){
        float radius = curveRadius;
        double curve_x = radius * Math.cos(Math.toRadians(135));
        double curve_y = radius * Math.sin(Math.toRadians(135));

        float x = rect.right - curveRadius - (float)curve_x;
        float y = rect.top + curveRadius - (float)curve_y;

        return new PointF(x, y);
    }

    public PointF getSeatSouth(){
        return new PointF(rect.centerX(), rect.bottom);
    }

    public RectF getRect(){
        return rect;
    }

    public float getCurveRadius(){
        return curveRadius;
    }

    public float getPlayingSurfaceTop(){
        return rect.top + strokeWidth / 2;
    }

    public float getStrokeWidth(){
        return strokeWidth;
    }

    private void createPaint(float strokeWidth){
        fillPaint = new Paint();
        fillPaint.setStyle(Paint.Style.FILL);
        fillPaint.setAntiAlias(true);
        fillPaint.setColor(Color.parseColor("#35654D"));
        strokePaint = new Paint();
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setAntiAlias(true);
        int color1 = Color.parseColor("#98633b");
        int color2 = Color.parseColor("#632A0D");
        strokePaint.setShader(new RadialGradient(rect.centerX(), rect.centerY(), rect.height(), color1, color2, Shader.TileMode.MIRROR));
        strokePaint.setStrokeWidth(strokeWidth);
    }

    public PointF getCenter() {
        return new PointF(rect.centerX(), rect.centerY());
    }
}
