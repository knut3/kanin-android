package no.esotericgames.kanin.android.screens.common.viewmvc;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import no.esotericgames.kanin.android.screens.availablelobbies.LobbiesItemViewMvc;
import no.esotericgames.kanin.android.screens.availablelobbies.LobbiesViewMvc;
import no.esotericgames.kanin.android.screens.common.dialogs.promptdialog.PromptViewMvc;
import no.esotericgames.kanin.android.screens.common.dialogs.promptdialog.PromptViewMvcImpl;
import no.esotericgames.kanin.android.screens.lobby.LobbyPlayerItemViewMvc;
import no.esotericgames.kanin.android.screens.lobby.LobbyViewMvc;

public class ViewMvcFactory {
    private final LayoutInflater layoutInflater;

    public ViewMvcFactory(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
    }

    public LobbiesItemViewMvc getLobbiesItemViewMvc(@Nullable ViewGroup parent){
        return new LobbiesItemViewMvc(layoutInflater, parent);
    }

    public LobbiesViewMvc getLobbiesViewMvc(@Nullable ViewGroup parent){
        return new LobbiesViewMvc(layoutInflater, parent, this);
    }

    public LobbyPlayerItemViewMvc getLobbyPlayerItemViewMvc(@Nullable ViewGroup parent, boolean iAmCreator){
        return new LobbyPlayerItemViewMvc(layoutInflater, parent, iAmCreator);
    }

    public LobbyViewMvc getLobbyViewMvc(@Nullable ViewGroup parent){
        return new LobbyViewMvc(layoutInflater, parent, this);
    }

    public PromptViewMvc getPromptViewMvc(@Nullable ViewGroup parent) {
        return new PromptViewMvcImpl(layoutInflater, parent);
    }
}
