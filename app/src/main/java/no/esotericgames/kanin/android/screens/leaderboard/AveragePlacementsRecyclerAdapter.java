package no.esotericgames.kanin.android.screens.leaderboard;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Locale;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.AveragePlacementModel;

public class AveragePlacementsRecyclerAdapter extends RecyclerView.Adapter<AveragePlacementsRecyclerAdapter.MyViewHolder> {

    public class MyViewHolder extends RecyclerView.ViewHolder{

        public final TextView txtRanking, txtUsername, avgPlacementPercentage;
        public final ProgressBar avgPlacementBar;
        private AveragePlacementModel data;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(view -> {
                data.inBarMode = !data.inBarMode;
                updateData(data);
            });
            txtRanking = itemView.findViewById(R.id.txt_leaderboard_ranking);
            txtUsername = itemView.findViewById(R.id.txtUsername);
            avgPlacementBar = itemView.findViewById(R.id.averagePlacementBar);
            avgPlacementPercentage = itemView.findViewById(R.id.averagePlacementPercentage);
        }

        public void updateData(AveragePlacementModel data){
            this.data = data;
            txtRanking.setText(""+data.ranking);
            if (data.ranking == 1)
                txtRanking.setBackgroundResource(R.drawable.circle_solid_gold_stroke_black);
            if (data.ranking == 2)
                txtRanking.setBackgroundResource(R.drawable.circle_solid_silver_stroke_black);
            if (data.ranking == 3)
                txtRanking.setBackgroundResource(R.drawable.circle_solid_bronze_stroke_black);
            txtUsername.setText(data.username);

            updatePlacement(data);
        }

        private void updatePlacement(AveragePlacementModel item) {
            float placePercentage = (1 - data.averagePlacement) * 100;
            if (item.inBarMode) {
                avgPlacementBar.setVisibility(View.VISIBLE);
                avgPlacementPercentage.setVisibility(View.GONE);
                avgPlacementBar.setProgress((int)placePercentage);
            }
            else{
                avgPlacementBar.setVisibility(View.GONE);
                avgPlacementPercentage.setVisibility(View.VISIBLE);
                avgPlacementPercentage.setText(String.format(Locale.ENGLISH, "%.2f%%", placePercentage));
            }
        }

    }

    private final List<AveragePlacementModel> averagePlacements;
    private final LayoutInflater inflater;

    public AveragePlacementsRecyclerAdapter(Context context, List<AveragePlacementModel> data) {
        this.inflater = LayoutInflater.from(context);
        this.averagePlacements = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.average_placement_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        AveragePlacementModel item = averagePlacements.get(position);
        holder.updateData(item);
    }

    @Override
    public int getItemCount() {
        return averagePlacements.size();
    }
}
