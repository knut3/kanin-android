package no.esotericgames.kanin.android.models;

import no.esotericgames.kanin.android.models.Card;

public class CardWithOwner {
    public Card card;
    public String owner;

    public CardWithOwner(Card card, String owner) {
        this.card = card;
        this.owner = owner;
    }
}
