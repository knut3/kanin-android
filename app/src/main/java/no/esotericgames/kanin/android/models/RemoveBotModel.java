package no.esotericgames.kanin.android.models;

public class RemoveBotModel {
    public String gameName;
    public String botName;

    public RemoveBotModel(String gameName, String botName) {
        this.gameName = gameName;
        this.botName = botName;
    }
}
