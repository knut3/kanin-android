package no.esotericgames.kanin.android.screens.game.scoreboard;

import android.graphics.Canvas;
import android.graphics.PointF;

import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;

public class MyScoresView implements IDrawable, IScoreBoard {
    private static final float HORIZONTAL_SPACE_RATIO = 0.1f;
    private PlayerScoreView roundScoreView, totalScoreView;
    private float top;
    private PointF centerPos;
    private float totalScore_x, roundScore_x;

    public MyScoresView(PointF centerPos, PointF scoreViewSize,
                        int roundScore, int totalScore,
                        ScoreViewAppearanceModel roundScoreAppearance,
                        ScoreViewAppearanceModel totalScoreAppearance) {
        this.centerPos = centerPos;

        roundScoreView = new PlayerScoreView(scoreViewSize.x, scoreViewSize.y,
                roundScoreAppearance, roundScore);
        totalScoreView = new PlayerScoreView(scoreViewSize.x, scoreViewSize.y,
                totalScoreAppearance, totalScore);

        float offsetFromCenter_x = scoreViewSize.x * HORIZONTAL_SPACE_RATIO / 2;
        roundScore_x = centerPos.x - offsetFromCenter_x - scoreViewSize.x;
        totalScore_x = centerPos.x + offsetFromCenter_x;
        float score_y = centerPos.y - scoreViewSize.y / 2;
        top = score_y;

        roundScoreView.setPosition(roundScore_x, score_y);
        totalScoreView.setPosition(totalScore_x, score_y);
    }

    @Override
    public void draw(Canvas canvas) {
        roundScoreView.draw(canvas);
        totalScoreView.draw(canvas);
    }

    public float getTop(){
        return top;
    }

    public PointF getCenterPosition(){
        return centerPos;
    }

    public PointF getTotalScoreIconCenter(){
        float x = totalScore_x + totalScoreView.getIconCenterOffsetFromLeft();
        return new PointF(x, totalScoreView.getCenter_y());
    }

    public PointF getRoundScoreIconCenter() {
        float x = roundScore_x + roundScoreView.getIconCenterOffsetFromLeft();
        return new PointF(x, roundScoreView.getCenter_y());
    }

    @Override
    public void addScore(int score) {
        roundScoreView.addScore(score);
        totalScoreView.addScore(score);
    }

    @Override
    public void addRoundScore(int score) {
        roundScoreView.addScore(score);
    }

    @Override
    public void addTotalScore(int score) {
        totalScoreView.addScore(score);
    }

    @Override
    public int getRoundScore() {
        return roundScoreView.getScore();
    }


}
