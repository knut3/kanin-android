package no.esotericgames.kanin.android.screens.common.helpers;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.screens.common.ResourceManager;

public class ToastMaster {
    private final Context context;
    private final ResourceManager resourceManager;
    private Toast currentToast;

    public ToastMaster(Context context, ResourceManager resourceManager) {
        this.context = context;
        this.resourceManager = resourceManager;
    }

    public void showFailureToast(String text){
        int bgColor = resourceManager.getColor(R.color.positive_score_bg);
        showToast(text, bgColor);
    }

    public void showSuccessToast(String text){
        int bgColor = resourceManager.getColor(R.color.negative_score_bg);
        showToast(text, bgColor);
    }

    private void showToast(String text, int bgColor){
        if (currentToast != null)
            currentToast.cancel();

        currentToast = Toast.makeText(context, text,
                Toast.LENGTH_SHORT);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            View view = currentToast.getView();

            view.getBackground().setColorFilter(
                    bgColor,
                    PorterDuff.Mode.SRC_IN);

            TextView textView = view.findViewById(android.R.id.message);
            textView.setTextColor(Color.BLACK);
        }

        currentToast.show();
    }
}
