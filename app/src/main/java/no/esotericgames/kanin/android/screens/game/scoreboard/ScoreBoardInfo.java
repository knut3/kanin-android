package no.esotericgames.kanin.android.screens.game.scoreboard;

import android.graphics.PointF;

public class ScoreBoardInfo {
    public enum Seat {WEST, NORTH_WEST, NORTH, NORTH_EAST, EAST, SOUTH }

    public int rotation;
    public PointF center;
    public Seat seat;

    public ScoreBoardInfo(int rotation, PointF center, Seat seat) {
        this.rotation = rotation;
        this.center = center;
        this.seat = seat;
    }
}
