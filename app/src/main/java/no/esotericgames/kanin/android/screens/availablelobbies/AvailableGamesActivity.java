package no.esotericgames.kanin.android.screens.availablelobbies;

import android.os.Bundle;
import android.util.Log;

import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.AvailableLobbyModel;
import no.esotericgames.kanin.android.networking.NetworkStateService;
import no.esotericgames.kanin.android.networking.duplex.LobbiesRepository;
import no.esotericgames.kanin.android.screens.common.BaseActivity;
import no.esotericgames.kanin.android.screens.common.helpers.GameNameEnhancer;
import no.esotericgames.kanin.android.screens.common.helpers.ScreensNavigator;
import no.esotericgames.kanin.android.screens.common.helpers.ToastMaster;
import no.esotericgames.kanin.android.screens.common.viewmvc.ViewMvcFactory;

public class AvailableGamesActivity extends BaseActivity
        implements LobbiesRepository.Listener, LobbiesViewMvc.Listener {
    private static final String TAG = "OpenLobbiesActivity";
    private ScreensNavigator screensNavigator;
    private ToastMaster toastMaster;
    private GameNameEnhancer gameNameEnhancer;
    private LobbiesRepository lobbiesRepository;
    private LobbiesViewMvc viewMvc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        screensNavigator = getCompositionRoot().getScreensNavigator();
        toastMaster = getCompositionRoot().getToastMaster();
        gameNameEnhancer = getCompositionRoot().getGameNameEnhancer();
        lobbiesRepository = getCompositionRoot().getLobbiesRepository();
        ViewMvcFactory viewMvcFactory = getCompositionRoot().getViewMvcFactory();
        viewMvc = viewMvcFactory.getLobbiesViewMvc(null);
        setContentView(viewMvc.getRootView());
    }

    @Override
    protected void onStart() {
        super.onStart();
        lobbiesRepository.bind(this);
        viewMvc.registerListener(this);
    }

    @Override
    protected void onStop() {
        lobbiesRepository.unbind();
        viewMvc.unregisterListener(this);
        super.onStop();
    }

    @Override
    public void onConnectionAvailable() {
        lobbiesRepository.tryReconnect();
    }

    //------------Repository Callbacks---------------------------------------

    @Override
    public void onConnectedToServer() {
        try {
            lobbiesRepository.getAvailableLobbiesAndNotify();
            hideDisconnectedOverlay();
        }
        catch (Exception e) {
            showDisconnectedOverlay();
        }
    }

    @Override
    public void onServerMaintenanceInProgress(int duration_min) {
        super.showAnnouncementOverlay(getString(R.string.announcement_server_maintenance, duration_min));
    }

    @Override
    public void onMessageFromAdmin(String message) {
        super.showAnnouncementOverlay(message);
    }

    @Override
    public void onGotAvailableLobbies(List<AvailableLobbyModel> lobbies) {
        Log.d(TAG, "onGotAvailableLobbies: size "+lobbies.size());
        for (AvailableLobbyModel lobby : lobbies)
            lobby.enhancedName = gameNameEnhancer.getEnhanced(lobby.name);
        viewMvc.bindLobbies(lobbies);
    }

    @Override
    public void onLobbyCreated(AvailableLobbyModel lobby){
        Log.d(TAG, "onNewLobbyCreated: " + lobby.name + " created!");
        lobby.enhancedName = gameNameEnhancer.getEnhanced(lobby.name);
        viewMvc.addLobby(lobby);
    }

    @Override
    public void onLobbyUnavailable(String gameName) {
        Log.d(TAG, "onLobbyUnavailable: " + gameName);
        viewMvc.removeLobby(gameName);
    }

    //------------View Callbacks------------------------------------------

    @Override
    public void onLobbyClicked(String gameName) {
        if (NetworkStateService.isNetworkConnected)
            screensNavigator.toLobby(gameName);
        else
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));

    }

    //-----------Private Methods-------------------------------------------

}