package no.esotericgames.kanin.android.models;

public class LoginModel {
    private final String userName;
    private String password;

    public LoginModel(String userName, String password) {
        this.userName = userName.trim();
        this.password = password;
    }

    public String getUsername() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
