package no.esotericgames.kanin.android.models;

public class TrickRoundEnded {

    public CardWithOwner cardPlayed;
    public TrickResult trickResult;
    public boolean endedCuzNoPointCardsLeft;
}
