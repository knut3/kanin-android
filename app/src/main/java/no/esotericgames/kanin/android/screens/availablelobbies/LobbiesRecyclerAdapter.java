package no.esotericgames.kanin.android.screens.availablelobbies;

import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.esotericgames.kanin.android.models.AvailableLobbyModel;
import no.esotericgames.kanin.android.screens.common.viewmvc.ViewMvcFactory;

public class LobbiesRecyclerAdapter extends RecyclerView.Adapter<LobbiesRecyclerAdapter.MyViewHolder>
        implements LobbiesItemViewMvc.Listener {

    public interface Listener {

        void onLobbyClicked(String gameName);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{

        private final LobbiesItemViewMvc viewMvc;

        public MyViewHolder(@NonNull LobbiesItemViewMvc itemView) {
            super(itemView.getRootView());
            viewMvc = itemView;
        }

    }

    private static final String TAG = "LobbiesRecyclerAdapter";
    private final List<AvailableLobbyModel> lobbies;
    private final Listener listener;
    private final ViewMvcFactory viewMvcFactory;

    public LobbiesRecyclerAdapter(Listener listener, ViewMvcFactory viewMvcFactory) {
        this.listener = listener;
        this.viewMvcFactory = viewMvcFactory;
        lobbies = new ArrayList<>();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LobbiesItemViewMvc viewMvc = viewMvcFactory.getLobbiesItemViewMvc(parent);
        viewMvc.registerListener(this);
        return new MyViewHolder(viewMvc);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final AvailableLobbyModel item = lobbies.get(position);
        holder.viewMvc.bindLobby(item);
    }

    public void bindLobbies(List<AvailableLobbyModel> lobbies){
        Log.d(TAG, "swapItems: ");
        this.lobbies.clear();
        this.lobbies.addAll(lobbies);
        this.notifyDataSetChanged();
    }

    public void addItem(AvailableLobbyModel lobby){
        Log.d(TAG, "addItem: " + lobby.name);
        lobbies.add(lobby);
        this.notifyItemInserted(lobbies.size()-1);
    }

    public void removeItem(String gameName){
        Iterator<AvailableLobbyModel> iterator = lobbies.iterator();
        int index = 0;
        while (iterator.hasNext()) {
            AvailableLobbyModel lobby = iterator.next();
            if (lobby.name.equals(gameName)) {
                this.notifyItemRemoved(index);
                iterator.remove();
                return;
            }
            index++;
        }
    }

    @Override
    public int getItemCount() {
        return lobbies.size();
    }

    @Override
    public void onLobbyClicked(String gameName) {
        listener.onLobbyClicked(gameName);
    }


}
