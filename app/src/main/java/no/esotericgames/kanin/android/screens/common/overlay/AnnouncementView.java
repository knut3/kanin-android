package no.esotericgames.kanin.android.screens.common.overlay;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import no.esotericgames.kanin.android.R;

public class AnnouncementView extends LinearLayout {
    private final TextView textView;

    public AnnouncementView(Context context) {
        super(context);
        inflate(context, R.layout.announcement_view, this);
        textView = findViewById(R.id.announcementTextView);
    }

    public void setText(String text){
        textView.setText(text);
    }
}
