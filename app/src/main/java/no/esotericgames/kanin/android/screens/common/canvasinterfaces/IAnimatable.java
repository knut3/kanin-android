package no.esotericgames.kanin.android.screens.common.canvasinterfaces;

import android.graphics.PointF;

public interface IAnimatable {
    void setPosition(PointF position);
    PointF getPosition();
    void setSize(PointF size);
    PointF getSize();
    void setRotation(float angle);
    float getRotation();
}
