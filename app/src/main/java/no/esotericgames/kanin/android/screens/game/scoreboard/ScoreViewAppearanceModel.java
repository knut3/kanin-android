package no.esotericgames.kanin.android.screens.game.scoreboard;

import android.graphics.drawable.Drawable;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.Rounds;
import no.esotericgames.kanin.android.screens.common.ResourceManager;

public class ScoreViewAppearanceModel {
    public Drawable icon;
    public int bgColor, scoreColor;
    public boolean removeMinusSign;

    public ScoreViewAppearanceModel(Drawable icon, int bgColor, int scoreColor,
                                    boolean removeMinusSign) {
        this.icon = icon;
        this.bgColor = bgColor;
        this.scoreColor = scoreColor;
        this.removeMinusSign = removeMinusSign;
    }

    public static ScoreViewAppearanceModel getRoundScoreAppearanceModel(
            String roundName, ResourceManager resourceManager){
        switch (roundName){
            case Rounds.Pass:
            case Rounds.Spar:
            case Rounds.Damer:
                return getPositiveScoreAppearanceModel(resourceManager);

            case Rounds.Grand:
            case Rounds.Trumf:
                return getNegativeScoreAppearanceModel(resourceManager);
            case Rounds.Kabal:
                return getCardsLeftAppearanceModel(resourceManager);

            default: throw new IllegalArgumentException(roundName +" is not valid");
        }
    }

    public static ScoreViewAppearanceModel getTotalScoreAppearanceModel(ResourceManager resourceManager){
        Drawable sumIcon = resourceManager.getDrawable(R.drawable.ic_the_sum_of_sum);
        int bgColor = resourceManager.getColor(R.color.total_score_bg);
        int scoreColor = resourceManager.getColor(R.color.total_score_text);
        return new ScoreViewAppearanceModel(sumIcon, bgColor, scoreColor, false);
    }

    private static ScoreViewAppearanceModel getPositiveScoreAppearanceModel(ResourceManager resourceManager){
        Drawable plusIcon = resourceManager.getDrawable(R.drawable.ic_plus);
        int bgColor = resourceManager.getColor(R.color.positive_score_bg);
        int scoreColor = resourceManager.getColor(R.color.positive_score_text);
        return new ScoreViewAppearanceModel(plusIcon, bgColor, scoreColor, false);
    }

    private static ScoreViewAppearanceModel getNegativeScoreAppearanceModel(ResourceManager resourceManager){
        Drawable minusIcon = resourceManager.getDrawable(R.drawable.ic_minus);
        int bgColor = resourceManager.getColor(R.color.negative_score_bg);
        int scoreColor = resourceManager.getColor(R.color.negative_score_text);
        return new ScoreViewAppearanceModel(minusIcon, bgColor, scoreColor, true);
    }

    private static ScoreViewAppearanceModel getCardsLeftAppearanceModel(ResourceManager resourceManager){
        Drawable cardsIcon = resourceManager.getDrawable(R.drawable.ic_cards);
        int bgColor = resourceManager.getColor(R.color.positive_score_bg);
        int scoreColor = resourceManager.getColor(R.color.positive_score_text);
        return new ScoreViewAppearanceModel(cardsIcon, bgColor, scoreColor, false);
    }
}
