package no.esotericgames.kanin.android.screens.common;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.IdRes;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;

import no.esotericgames.kanin.android.models.Card;

public class ResourceManager {
    private Context context;

    public ResourceManager(Context context) {
        this.context = context;
    }

    public Drawable getCardDrawable(Card card){
        int value = card.value == 1 ? 14 : card.value;
        String resourceName = "card_" + card.suit + "_" + value;
        int id = context.getResources().getIdentifier(resourceName, "drawable", context.getPackageName());
        return getDrawable(id);
    }

    public Drawable getDrawable(@DrawableRes int id){
        return ContextCompat.getDrawable(context, id);
    }

    public int getColor(@ColorRes int id){
        return ContextCompat.getColor(context, id);
    }

    public String getString(@StringRes int id) {
        return context.getString(id);
    }

    public String getString(@StringRes int id, Object... params) {
        return context.getString(id, params);
    }
}
