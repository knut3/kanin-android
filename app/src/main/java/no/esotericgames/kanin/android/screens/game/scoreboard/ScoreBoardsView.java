package no.esotericgames.kanin.android.screens.game.scoreboard;

import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;
import android.util.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.esotericgames.kanin.android.models.CurrentScore;
import no.esotericgames.kanin.android.models.Player;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;
import no.esotericgames.kanin.android.screens.game.common.Vector;
import no.esotericgames.kanin.android.screens.game.rounds.common.TableView;

public class ScoreBoardsView implements IDrawable {

    private final int playerNameSize;
    private final ScoreViewAppearanceModel roundScoreAppearance, totalScoreAppearance;
    private final Map<String, Pair<ScoreBoardView, ScoreBoardInfo>> scoreBoards = new HashMap<>();
    private float centerScoreBoardBottom;
    private final List<CurrentScore> initialScores;
    private final Drawable disconnectedIcon;

    public ScoreBoardsView(Player[] opponents, List<CurrentScore> currentScores,
                           boolean useCardsLeftAsRoundScore, int playerNameSize,
                           ScoreViewAppearanceModel roundScoreAppearance,
                           ScoreViewAppearanceModel totalScoreAppearance,
                           TableView tableView, Drawable disconnectedIcon) {
        this.playerNameSize = playerNameSize;
        this.roundScoreAppearance = roundScoreAppearance;
        this.totalScoreAppearance = totalScoreAppearance;
        this.initialScores = currentScores;
        this.disconnectedIcon = disconnectedIcon;

        if (useCardsLeftAsRoundScore)
            for (Player p : opponents){
                for (CurrentScore score : initialScores){
                    if (p.getUsername().equals(score.username))
                        score.roundScore = p.getNumCardsOnHand();
                }
            }

        switch (opponents.length){
            case 2:
                createScoreBoards_TwoOpponents(opponents, tableView);
                break;
            case 3:
                createScoreBoards_ThreeOpponents(opponents, tableView);
                break;
            case 4:
                createScoreBoards_FourOpponents(opponents, tableView);
                break;

            default: throw new IllegalArgumentException("Num opponents must be between 2 and 4");
        }
    }

    @Override
    public void draw(Canvas canvas) {
        for (Pair<ScoreBoardView, ScoreBoardInfo> board : scoreBoards.values()){
            int rotation = board.second.rotation;
            if (rotation == 0){
                board.first.draw(canvas);
            }
            else{
                PointF center = board.second.center;
                canvas.save();
                canvas.rotate(rotation, center.x, center.y);
                board.first.draw(canvas);
                canvas.restore();
            }
        }
    }

    public float getCenterBoardBottom(){
        return centerScoreBoardBottom;
    }

    public PointF getScoreBoardCenter(String opponentName){
        return scoreBoards.get(opponentName).second.center;
    }

    public float getScoreBoardHeight(String opponentName){
        return scoreBoards.get(opponentName).first.getHeight();
    }

    public PointF getTotalScoreIconCenter(String opponentName){
        Pair<ScoreBoardView, ScoreBoardInfo> boardPair = scoreBoards.get(opponentName);
        PointF centerBetweenScores = getPointBelowScoreBoardCenter(opponentName,
                boardPair.first.getDistanceFromCenterToCenterBetweenScores());
        float horizontalOffsetFromCenter = boardPair.first.getTotalScoreIconHorizontalOffsetFromCenter();

        if (boardPair.second.rotation == 0){
            centerBetweenScores.x += horizontalOffsetFromCenter;
            return centerBetweenScores;
        }
        else{
            PointF down = Vector.subtract(centerBetweenScores, boardPair.second.center);
            PointF normalizedLeft = Vector.getNormalized(Vector.getPerpendicular(down));
            PointF offsetFromCenterBetweenScores = Vector.multiply(normalizedLeft, horizontalOffsetFromCenter);
            return Vector.add(centerBetweenScores, offsetFromCenterBetweenScores);
        }
    }

    public PointF getRoundScoreIconCenter(String opponentName){
        Pair<ScoreBoardView, ScoreBoardInfo> boardPair = scoreBoards.get(opponentName);
        PointF centerBetweenScores = getPointBelowScoreBoardCenter(opponentName,
                boardPair.first.getDistanceFromCenterToCenterBetweenScores());
        float horizontalOffsetFromCenter = boardPair.first.getRoundScoreIconHorizontalOffsetFromCenter();

        if (boardPair.second.rotation == 0){
            centerBetweenScores.x += horizontalOffsetFromCenter;
            return centerBetweenScores;
        }
        else{
            PointF down = Vector.subtract(centerBetweenScores, boardPair.second.center);
            PointF normalizedLeft = Vector.getNormalized(Vector.getPerpendicular(down));
            PointF offsetFromCenterBetweenScores = Vector.multiply(normalizedLeft, horizontalOffsetFromCenter);
            return Vector.add(centerBetweenScores, offsetFromCenterBetweenScores);
        }
    }

    public PointF getCenterBetweenScores(String opponentName){
        return getPointBelowScoreBoardCenter(opponentName,
                scoreBoards.get(opponentName).first.getDistanceFromCenterToCenterBetweenScores());
    }

    public PointF getPointBelowScoreBoardCenter(String opponentName, float distanceFromCenter){
        Pair<ScoreBoardView, ScoreBoardInfo> boardPair = scoreBoards.get(opponentName);
        PointF boardCenter = boardPair.second.center;

        if (boardPair.second.rotation == 0)
            return new PointF(boardCenter.x, boardCenter.y + distanceFromCenter);

        else{
            float curve_x = (float) (distanceFromCenter * Math.cos(Math.toRadians(-boardPair.second.rotation)));
            float curve_y = (float) (distanceFromCenter * Math.sin(Math.toRadians(-boardPair.second.rotation)));
            return boardPair.second.seat == ScoreBoardInfo.Seat.WEST
                    ? new PointF(boardCenter.x + curve_x, boardCenter.y + curve_y)
                    : new PointF(boardCenter.x - curve_x, boardCenter.y - curve_y);
        }
    }

    public Map<String, ScoreBoardInfo.Seat> getActiveSeatDirections(){
        Map<String, ScoreBoardInfo.Seat> map = new HashMap<>(scoreBoards.size());
        for (Map.Entry<String, Pair<ScoreBoardView, ScoreBoardInfo>> scoreBoard : scoreBoards.entrySet()){
            map.put(scoreBoard.getKey(), scoreBoard.getValue().second.seat);
        }
        return map;
    }

    public void addScore(int score, String owner) {
        ScoreBoardView board = scoreBoards.get(owner).first;
        board.addScore(score);
    }

    public ScoreBoardView getScoreBoardView(String owner){
        return scoreBoards.get(owner).first;
    }

    public void clearMarkedScoreBoards(){
        scoreBoards.values().forEach(x-> x.first.hideStroke());
    }

    public void markPlayerTurn(String username){
        clearMarkedScoreBoards();
        scoreBoards.get(username).first.showStroke();
    }

    private void createScoreBoards_TwoOpponents(Player[] players, TableView table){
        scoreBoards.put(players[0].getUsername(), getWestSeatRotationPair(players[0], table));
        scoreBoards.put(players[1].getUsername(), getEastSeatRotationPair(players[1], table));
        centerScoreBoardBottom = table.getPlayingSurfaceTop();
    }

    private void createScoreBoards_ThreeOpponents(Player[] players, TableView table){
        scoreBoards.put(players[0].getUsername(), getWestSeatRotationPair(players[0], table));

        Pair<ScoreBoardView, ScoreBoardInfo> northRotationPair = getNorthSeatRotationPair(players[1], table);
        centerScoreBoardBottom = northRotationPair.first.getBottom();
        scoreBoards.put(players[1].getUsername(), northRotationPair);

        scoreBoards.put(players[2].getUsername(), getEastSeatRotationPair(players[2], table));

    }

    private void createScoreBoards_FourOpponents(Player[] players, TableView table){
        scoreBoards.put(players[0].getUsername(), getWestSeatRotationPair(players[0], table));

        Pair<ScoreBoardView, ScoreBoardInfo> northWestRotationPair = getNorthWestSeatRotationPair(players[1], table);
        centerScoreBoardBottom = northWestRotationPair.first.getBottom();
        scoreBoards.put(players[1].getUsername(), northWestRotationPair);

        scoreBoards.put(players[2].getUsername(), getNorthEastSeatRotationPair(players[2], table));
        scoreBoards.put(players[3].getUsername(), getEastSeatRotationPair(players[3], table));

    }

    private Pair<ScoreBoardView, ScoreBoardInfo> getWestSeatRotationPair(Player player, TableView table) {
        ScoreBoardView scoreBoard = createScoreBoard(table.getSeatWest(), player.getUsername(), true);
        return new Pair<>(scoreBoard, new ScoreBoardInfo(-45, table.getSeatWest(), ScoreBoardInfo.Seat.WEST));
    }

    private Pair<ScoreBoardView, ScoreBoardInfo> getNorthWestSeatRotationPair(Player player, TableView table) {
        ScoreBoardView scoreBoard = createScoreBoard(table.getSeatNorthWest(), player.getUsername(), false);
        return new Pair<>(scoreBoard, new ScoreBoardInfo(0, table.getSeatNorthWest(), ScoreBoardInfo.Seat.NORTH_WEST));
    }

    private Pair<ScoreBoardView, ScoreBoardInfo> getNorthSeatRotationPair(Player player, TableView table) {
        ScoreBoardView scoreBoard = createScoreBoard(table.getSeatNorth(), player.getUsername(), false);
        return new Pair<>(scoreBoard, new ScoreBoardInfo(0, table.getSeatNorth(), ScoreBoardInfo.Seat.NORTH));
    }

    private Pair<ScoreBoardView, ScoreBoardInfo> getNorthEastSeatRotationPair(Player player, TableView table) {
        ScoreBoardView scoreBoard = createScoreBoard(table.getSeatNorthEast(), player.getUsername(), false);
        return new Pair<>(scoreBoard, new ScoreBoardInfo(0, table.getSeatNorthEast(), ScoreBoardInfo.Seat.NORTH_EAST));
    }

    private Pair<ScoreBoardView, ScoreBoardInfo> getEastSeatRotationPair(Player player, TableView table) {
        ScoreBoardView scoreBoard = createScoreBoard(table.getSeatEast(), player.getUsername(), true);
        return new Pair<>(scoreBoard, new ScoreBoardInfo(45, table.getSeatEast(), ScoreBoardInfo.Seat.EAST));
    }

    private ScoreBoardView createScoreBoard(PointF centerPos, String username, boolean isCornerBoard){
        CurrentScore currentScore = initialScores.stream().filter(x-> x.username.equals(username)).findFirst().get();
        return new ScoreBoardView(centerPos, playerNameSize, username, isCornerBoard,
                currentScore.roundScore, currentScore.totalScore, roundScoreAppearance, totalScoreAppearance, disconnectedIcon);
    }
}
