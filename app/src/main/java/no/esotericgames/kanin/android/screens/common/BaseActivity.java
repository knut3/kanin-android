package no.esotericgames.kanin.android.screens.common;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import androidx.appcompat.app.AppCompatActivity;

import no.esotericgames.kanin.android.common.CustomApplication;
import no.esotericgames.kanin.android.common.dependencyinjection.ActivityCompositionRoot;
import no.esotericgames.kanin.android.common.dependencyinjection.ControllerCompositionRoot;
import no.esotericgames.kanin.android.networking.NetworkStateService;
import no.esotericgames.kanin.android.screens.common.overlay.AnnouncementView;
import no.esotericgames.kanin.android.screens.common.overlay.DisconnectedView;

public class BaseActivity extends AppCompatActivity implements NetworkStateService.Listener {

    private ActivityCompositionRoot mActivityCompositionRoot;
    private ControllerCompositionRoot mControllerCompositionRoot;
    private AnnouncementView announcementView;
    private DisconnectedView disconnectedView;
    private NetworkStateService.LocalBinder networkStateBinder;

    private final ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            networkStateBinder = (NetworkStateService.LocalBinder) service;
            networkStateBinder.addListener(BaseActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        this.bindService(
                new Intent(this, NetworkStateService.class),
                serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        if (networkStateBinder != null)
            networkStateBinder.removeListener(this);
        unbindService(serviceConnection);
        super.onStop();
    }

    @Override
    public void setContentView(int layoutResID) {
        LayoutInflater li = LayoutInflater.from(this);
        View theView = li.inflate(layoutResID, null);
        this.setContentView(theView);
    }

    @Override
    public void setContentView(View view) {
        FrameLayout rootView = new FrameLayout(this);
        rootView.addView(view);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        announcementView = new AnnouncementView(this);
        announcementView.setLayoutParams(params);
        hideAnnouncementOverlay();
        rootView.addView(announcementView);
        disconnectedView = new DisconnectedView(this);
        disconnectedView.setLayoutParams(params);
        hideDisconnectedOverlay();
        rootView.addView(disconnectedView);
        super.setContentView(rootView);
    }

    ///--------- NetworkStateService events-----------------------------

    @Override
    public void onConnectionLost() {
        showDisconnectedOverlay();
    }

    @Override
    public void onConnectionAvailable() {
        hideDisconnectedOverlay();
    }

    ///------------------------------------------------------------------

    public ActivityCompositionRoot getActivityCompositionRoot() {
        if (mActivityCompositionRoot == null) {
            mActivityCompositionRoot = new ActivityCompositionRoot(
                    ((CustomApplication) getApplication()).getCompositionRoot(),
                    this
            );
        }
        return mActivityCompositionRoot;
    }

    protected ControllerCompositionRoot getCompositionRoot() {
        if (mControllerCompositionRoot == null) {
            mControllerCompositionRoot = new ControllerCompositionRoot(getActivityCompositionRoot());
        }
        return mControllerCompositionRoot;
    }

    protected void hideAnnouncementOverlay(){
        runOnUiThread(()-> announcementView.setVisibility(View.GONE));
    }

    protected void showAnnouncementOverlay(String text){
        runOnUiThread(()-> {
            announcementView.setText(text);
            announcementView.setVisibility(View.VISIBLE);
        });
    }

    protected void hideDisconnectedOverlay(){
        runOnUiThread(()-> disconnectedView.setVisibility(View.GONE));
    }

    protected void showDisconnectedOverlay(){
        runOnUiThread(()-> disconnectedView.setVisibility(View.VISIBLE));
    }
}
