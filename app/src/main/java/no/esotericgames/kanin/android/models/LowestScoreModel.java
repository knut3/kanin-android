package no.esotericgames.kanin.android.models;

public class LowestScoreModel {
    public int ranking;
    public String username;
    public int score;
}
