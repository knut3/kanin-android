package no.esotericgames.kanin.android.common;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import org.acra.ACRA;
import org.acra.BuildConfig;
import org.acra.config.CoreConfigurationBuilder;
import org.acra.config.HttpSenderConfigurationBuilder;
import org.acra.config.ToastConfigurationBuilder;
import org.acra.data.StringFormat;
import org.acra.sender.HttpSender;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.common.dependencyinjection.CompositionRoot;

public class CustomApplication extends Application {

    private CompositionRoot mCompositionRoot;

    @Override
    public void onCreate() {
        super.onCreate();
        mCompositionRoot = new CompositionRoot();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        ACRA.init(this, new CoreConfigurationBuilder()
                //core configuration:
                .withBuildConfigClass(BuildConfig.class)
                .withReportFormat(StringFormat.JSON)
                .withPluginConfigurations(
                        new ToastConfigurationBuilder()
                                .withText(getString(R.string.acra_toast_text))
                                .withLength(Toast.LENGTH_LONG)
                                .build(),
                        new HttpSenderConfigurationBuilder()
                                //required. Https recommended
                                .withUri(Constants.ACRARIUM_URL)
                                //optional. Enables http basic auth
                                .withBasicAuthLogin("7rotx7p6RgXSu21L")
                                //required if above set
                                .withBasicAuthPassword("TOED8b9LQSzPXnwP")
                                // defaults to POST
                                .withHttpMethod(HttpSender.Method.POST)
                                .build()
                )
        );
    }

    public CompositionRoot getCompositionRoot() {
        return mCompositionRoot;
    }
}