package no.esotericgames.kanin.android.screens.common.viewmvc;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class BaseObservableViewMvc<ListenerType> extends BaseViewMvc
        implements ObservableViewMvc<ListenerType>{

    private Set<ListenerType> mListeners = new HashSet<>();

    public final void registerListener(ListenerType listener) {
        mListeners.add(listener);
    }

    public final void unregisterListener(ListenerType listener) {
        mListeners.remove(listener);
    }

    protected final Set<ListenerType> getListeners() {
        return Collections.unmodifiableSet(mListeners);
    }
}
