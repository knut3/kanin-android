package no.esotericgames.kanin.android.screens.common.dialogs;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.screens.common.dialogs.infodialog.InfoDialog;
import no.esotericgames.kanin.android.screens.common.dialogs.promptdialog.PromptDialog;

public class DialogsManager {
    public static final String TAG_DISCONNECTED_FROM_SERVER_PROMPT = "TAG_DISCONNECTED_FROM_SERVER_PROMPT";
    public static final String TAG_LEAVE_LOBBY_AS_CREATOR_PROMPT = "TAG_LEAVE_LOBBY_AS_CREATOR_PROMPT";
    public static final String TAG_LEAVE_LOBBY_AS_PLAYER_PROMPT = "TAG_LEAVE_LOBBY_AS_PLAYER_PROMPT";
    public static final String TAG_GOTO_RUNNING_GAME_PROMPT = "TAG_GOTO_RUNNING_GAME_PROMPT";
    public static final String TAG_GOTO_MAIN_MENU_PROMPT = "TAG_GOTO_MAIN_MENU_PROMPT";
    public static final String TAG_CLOSE_APP_PROMPT = "TAG_CLOSE_APP_PROMPT";


    private final Context context;
    private final FragmentManager fragmentManager;

    public DialogsManager(Context context, FragmentManager fragmentManager) {
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    public void showDisconnectedFromServerDialog() {
        DialogFragment dialogFragment = PromptDialog.newPromptDialog(
                getString(R.string.disconnected_from_server_dialog_title),
                getString(R.string.disconnected_from_server_dialog_message),
                getString(R.string.disconnected_from_server_dialog_positive_button_caption),
                getString(R.string.disconnected_from_server_dialog_negative_button_caption)
        );
        dialogFragment.show(fragmentManager, TAG_DISCONNECTED_FROM_SERVER_PROMPT);
    }

    public void showLeaveLobbyAsCreatorDialog(String gameName){
        DialogFragment dialogFragment = PromptDialog.newPromptDialog(
            getString(R.string.leave_lobby_as_creator_dialog_title_prefix, gameName),
                getString(R.string.leave_lobby_as_creator_dialog_message),
                getString(R.string.leave_lobby_as_creator_dialog_positive_button_caption),
                getString(R.string.leave_lobby_as_creator_dialog_negative_button_caption)
        );
        dialogFragment.show(fragmentManager, TAG_LEAVE_LOBBY_AS_CREATOR_PROMPT);
    }

    public void showLeaveLobbyAsPlayerDialog(String gameName){
        DialogFragment dialogFragment = PromptDialog.newPromptDialog(
                getString(R.string.leave_lobby_as_player_dialog_title_prefix, gameName),
                getString(R.string.leave_lobby_as_player_dialog_message),
                getString(R.string.leave_lobby_as_player_dialog_positive_button_caption),
                getString(R.string.leave_lobby_as_player_dialog_negative_button_caption)
        );
        dialogFragment.show(fragmentManager, TAG_LEAVE_LOBBY_AS_PLAYER_PROMPT);
    }

    public void showGotoRunningGameDialog(String gameName){
        DialogFragment dialogFragment = PromptDialog.newPromptDialog(
                getString(R.string.goto_running_game_dialog_title),
                getString(R.string.goto_running_game_dialog_message_prefix, gameName),
                getString(R.string.goto_running_game_dialog_positive_button_caption_prefix, gameName),
                getString(R.string.goto_running_game_dialog_negative_button_caption)
        );
        dialogFragment.show(fragmentManager, TAG_GOTO_RUNNING_GAME_PROMPT);
    }

    public void showGotoMainMenuDialog(){
        DialogFragment dialogFragment = PromptDialog.newPromptDialog(
                getString(R.string.goto_main_menu_dialog_title),
                getString(R.string.goto_main_menu_dialog_message),
                getString(R.string.goto_main_menu_dialog_positive_button_caption),
                getString(R.string.goto_main_menu_dialog_negative_button_caption)
        );
        dialogFragment.show(fragmentManager, TAG_GOTO_MAIN_MENU_PROMPT);
    }

    public void showCloseAppDialog(){
        DialogFragment dialogFragment = PromptDialog.newPromptDialog(
                getString(R.string.close_game_dialog_title),
                getString(R.string.close_game_dialog_message, getString(R.string.app_name)),
                getString(R.string.goto_main_menu_dialog_positive_button_caption),
                getString(R.string.goto_main_menu_dialog_negative_button_caption)
        );
        dialogFragment.show(fragmentManager, TAG_CLOSE_APP_PROMPT);
    }

    public void showLobbyMaybeFullErrorMessage(@Nullable Runnable onDismiss) {
        DialogFragment dialogFragment = InfoDialog.newInfoDialog(
                getString(R.string.lobby_maybe_full_dialog_title),
                getString(R.string.lobby_maybe_full_dialog_message),
                getString(R.string.lobby_maybe_full_btn_caption),
                onDismiss
        );
        dialogFragment.show(fragmentManager, null);
    }

    private String getString(int stringId) {
        return context.getString(stringId);
    }

    private String getString(int stringId, String argument) {
        return context.getString(stringId, argument);
    }

    public @Nullable String getShownDialogTag() {
        for (Fragment fragment : fragmentManager.getFragments()) {
            if (fragment instanceof BaseDialog) {
                return fragment.getTag();
            }
        }
        return null;
    }

}
