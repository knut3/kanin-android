package no.esotericgames.kanin.android.models;

public class EarnedTarotCardStatistic {
    public TarotCard card;
    public int reasonWinnerCount;
    public int reasonRabbitCount;
}
