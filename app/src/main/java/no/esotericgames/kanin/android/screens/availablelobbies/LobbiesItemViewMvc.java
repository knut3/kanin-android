package no.esotericgames.kanin.android.screens.availablelobbies;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.AvailableLobbyModel;
import no.esotericgames.kanin.android.screens.common.viewmvc.BaseObservableViewMvc;

public class LobbiesItemViewMvc extends BaseObservableViewMvc<LobbiesItemViewMvc.Listener> {

    public interface Listener{
        void onLobbyClicked(String gameName);
    }

    private AvailableLobbyModel lobby;
    private final TextView txtGameName;
    private final TextView txtPlayerCount;

    public LobbiesItemViewMvc(LayoutInflater inflater, @Nullable ViewGroup parent){
        setRootView(inflater.inflate(R.layout.lobbies_item, parent, false));

        txtGameName = findViewById(R.id.txtGameName);
        txtPlayerCount = findViewById(R.id.txtPlayerCount);
        getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (Listener listener : getListeners()) {
                    listener.onLobbyClicked(lobby.name);
                }
            }
        });
    }

    public void bindLobby(AvailableLobbyModel lobby){
        this.lobby = lobby;
        txtGameName.setText(lobby.enhancedName);
        txtPlayerCount.setText(lobby.playerCount + "/5");
    }
}
