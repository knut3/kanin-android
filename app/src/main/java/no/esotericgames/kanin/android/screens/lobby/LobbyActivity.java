package no.esotericgames.kanin.android.screens.lobby;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.BotModel;
import no.esotericgames.kanin.android.models.GameSetup;
import no.esotericgames.kanin.android.common.SharedPreferenceKeys;
import no.esotericgames.kanin.android.models.LobbyPlayer;
import no.esotericgames.kanin.android.networking.NetworkStateService;
import no.esotericgames.kanin.android.networking.duplex.LobbyRepository;
import no.esotericgames.kanin.android.screens.common.BaseActivity;
import no.esotericgames.kanin.android.screens.common.helpers.GameNameEnhancer;
import no.esotericgames.kanin.android.screens.common.helpers.ScreensNavigator;
import no.esotericgames.kanin.android.screens.common.dialogs.DialogsEventBus;
import no.esotericgames.kanin.android.screens.common.dialogs.DialogsManager;
import no.esotericgames.kanin.android.screens.common.dialogs.promptdialog.PromptDialogEvent;
import no.esotericgames.kanin.android.screens.common.helpers.ToastMaster;

public class LobbyActivity extends BaseActivity
        implements LobbyRepository.Listener, DialogsEventBus.Listener {


    private enum ScreenState{
        ON_CREATION, NORMAL
    }

    private static final String TAG = "LobbyActivity";
    public static final String GAME_NAME_EXTRA_KEY = "gameName";
    protected String username;
    protected String gameName;

    private GameNameEnhancer gameNameEnhancer;
    protected ScreensNavigator screensNavigator;
    protected DialogsManager dialogsManager;
    protected ToastMaster toastMaster;
    private DialogsEventBus dialogsEventBus;
    protected LobbyRepository lobbyRepository;
    protected LobbyViewMvc viewMvc;
    protected int numPlayers = 0;
    private ScreenState screenState = ScreenState.ON_CREATION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        username = getCompositionRoot().getSharedPreferences()
                .getString(SharedPreferenceKeys.USERNAME, "");
        gameName = getGameName();

        gameNameEnhancer = getCompositionRoot().getGameNameEnhancer();
        screensNavigator = getCompositionRoot().getScreensNavigator();
        toastMaster = getCompositionRoot().getToastMaster();
        dialogsManager = getCompositionRoot().getDialogsManager();
        dialogsEventBus = getCompositionRoot().getDialogsEventBus();
        lobbyRepository = getCompositionRoot().getLobbyRepository();
        viewMvc = getCompositionRoot().getViewMvcFactory().getLobbyViewMvc(null);
        setContentView(viewMvc.getRootView());
    }

    @Override
    protected void onStart() {
        super.onStart();
        lobbyRepository.bind(this);
        dialogsEventBus.registerListener(this);
    }

    @Override
    protected void onStop() {
        lobbyRepository.unbind();
        dialogsEventBus.unregisterListener(this);
        super.onStop();
    }

    @Override
    public void onConnectionAvailable() {
        lobbyRepository.tryReconnect();
    }

    @Override
    public void onBackPressed() {
        if (NetworkStateService.isNetworkConnected)
            dialogsManager.showLeaveLobbyAsPlayerDialog(gameName);
        else
            dialogsManager.showCloseAppDialog();
    }

    protected String getGameName(){
        Bundle extras = getIntent().getExtras();
        if (extras == null) return null;
        else return extras.getString(GAME_NAME_EXTRA_KEY);
    }

    protected boolean createOrJoinLobby(){
        try {
            return lobbyRepository.joinLobbyBlocking(gameName);
        }
        catch (Exception e) {
            showDisconnectedOverlay();
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
            return false;
        }
    }


    //----------Repository Callbacks--------------------------------------

    @Override
    public void onConnectedToServer() {
        hideDisconnectedOverlay();
        if (screenState == ScreenState.ON_CREATION) {
            boolean success = createOrJoinLobby();
            if (!success){
                dialogsManager.showLobbyMaybeFullErrorMessage(
                        ()-> screensNavigator.toAvailableLobbies()
                        );
                return;
            }
        }
        viewMvc.setHeader(gameNameEnhancer.getEnhanced(gameName));
        lobbyRepository.getGameSetupAndNotify(gameName);
    }

    @Override
    public void onGotGameSetup(GameSetup gameSetup){
        screenState = ScreenState.NORMAL;
        Log.d(TAG, "onGotGameSetup: " + gameSetup);
        if (gameSetup == null) {
            finish();
        }
        else if (gameSetup.gameHasStarted){
            screensNavigator.toGameScreen(gameSetup.gameName, true);
        }
        else{
            numPlayers = gameSetup.usernames.size() + gameSetup.bots.size();
            List<LobbyPlayer> players = new ArrayList<>(numPlayers);
            gameSetup.usernames.forEach(x -> players.add(new LobbyPlayer(x)));
            gameSetup.bots.forEach(x -> players.add(new LobbyPlayer(x)));
            viewMvc.bindPlayers(players);
        }
    }

    @Override
    public void onGameStarting(){
        Log.d("LobbyActivity", "onGameStarting: ");
        screensNavigator.toGameScreen(gameName, true);
    }

    @Override
    public void onServerMaintenanceInProgress(int duration_min) {
        super.showAnnouncementOverlay(getString(R.string.announcement_server_maintenance, duration_min));
    }

    @Override
    public void onMessageFromAdmin(String message) {
        super.showAnnouncementOverlay(message);
    }

    @Override
    public void onUserJoined(String name){
        numPlayers++;
        viewMvc.addPlayer(new LobbyPlayer(name));
    }

    @Override
    public void onUserLeft(String name){
        numPlayers--;
        viewMvc.removePlayer(name);
    }

    @Override
    public void onLobbyClosed() {
        screensNavigator.toMainMenu();

    }

    @Override
    public void onBotAdded(BotModel model) {
        numPlayers++;
        viewMvc.addBot(model);
    }

    @Override
    public void onBotRemoved(String name) {
        numPlayers--;
        viewMvc.removePlayer(name);
    }

    //----------View Callbacks----------------------------

    @Override
    public void onDialogEvent(Object event) {
        if (event instanceof PromptDialogEvent){
            PromptDialogEvent promptEvent = (PromptDialogEvent) event;
            switch (dialogsManager.getShownDialogTag()){
                case DialogsManager.TAG_LEAVE_LOBBY_AS_CREATOR_PROMPT:
                    handleLeaveLobbyAsCreatorDialogEvent(promptEvent);
                    break;
                case DialogsManager.TAG_LEAVE_LOBBY_AS_PLAYER_PROMPT:
                    handleLeaveLobbyAsPlayerDialogEvent(promptEvent);
                    break;
                case DialogsManager.TAG_CLOSE_APP_PROMPT:
                    handleCloseAppDialogEvent(promptEvent);
                    break;
            }
        }
    }

    //----------Private Methods---------------------------


    private void handleLeaveLobbyAsCreatorDialogEvent(PromptDialogEvent event) {
        switch (event.getClickedButton()){
            case POSITIVE:
                try{
                    lobbyRepository.leaveLobbyBlocking(gameName);
                }
                catch (Exception e) {
                    showDisconnectedOverlay();
                }
                break;
            case NEGATIVE:
        }
    }

    private void handleLeaveLobbyAsPlayerDialogEvent(PromptDialogEvent event) {
        switch (event.getClickedButton()){
            case POSITIVE:
                try{
                    lobbyRepository.leaveLobbyBlocking(gameName);
                }
                catch (Exception e) {
                    showDisconnectedOverlay();
                }
                screensNavigator.toMainMenu();
                break;
            case NEGATIVE:
        }
    }

    private void handleCloseAppDialogEvent(PromptDialogEvent event) {
        switch (event.getClickedButton()){
            case POSITIVE:
                finishAndRemoveTask();
                break;
            case NEGATIVE:
        }
    }
}