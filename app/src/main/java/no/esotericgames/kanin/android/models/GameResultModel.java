package no.esotericgames.kanin.android.models;

import java.util.List;

public class GameResultModel {
    public List<ScoreModel> totalScores;
    public List<EarnedTarotCard> earnedTarotCards;
}
