package no.esotericgames.kanin.android.screens.game.rounds.common.scorebubbles;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.Drawable;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.screens.common.ResourceManager;
import no.esotericgames.kanin.android.screens.game.rounds.common.scorebubbles.ScoreBubbleView;

public class SignScoreBubbleView extends ScoreBubbleView {
    private Drawable signDrawable;
    private Point position = new Point();
    private PointF contentSize = new PointF();


    public SignScoreBubbleView(int score, ResourceManager resourceManager) {
        super(score, resourceManager, false);

        if (score < 0)
            signDrawable = resourceManager.getDrawable(R.drawable.ic_minus);
        else
            signDrawable = resourceManager.getDrawable(R.drawable.ic_plus);

    }

    @Override
    protected void setContentAlpha(int alpha) {
        signDrawable.setAlpha(alpha);
    }

    @Override
    protected void setContentSize(PointF bubbleSize) {
        contentSize.x = bubbleSize.x * 1.1f;
        contentSize.y = contentSize.x;
        if (centerPosition != null)
            updateContentPosition(centerPosition);
    }

    @Override
    protected void setContentPosition(PointF bubbleCenter) {
        updateContentPosition(bubbleCenter);
    }

    @Override
    protected void drawContent(Canvas canvas) {
        int right = (int) (position.x + contentSize.x);
        int bottom = (int) (position.y + contentSize.y);
        signDrawable.setBounds(position.x, position.y, right, bottom);
        signDrawable.draw(canvas);
    }

    private void updateContentPosition(PointF bubbleCenter){
        int x = (int) (bubbleCenter.x - size.x / 2);
        int y = (int) (bubbleCenter.y - size.y / 2);
        position.x = x;
        position.y = y;
    }
}
