package no.esotericgames.kanin.android.screens.game.common;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.PointFEvaluator;
import android.animation.ValueAnimator;
import android.graphics.PointF;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import java.util.ArrayList;
import java.util.List;

import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IAnimatable;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IFadeable;
import no.esotericgames.kanin.android.screens.game.rounds.common.CardView;
import no.esotericgames.kanin.android.screens.game.rounds.common.scorebubbles.ScoreBubbleView;


public class ViewAnimator {
    private final View drawingView;
    private final int defaultDuration;

    public ViewAnimator(View drawingView, int defaultDuration_ms) {
        this.drawingView = drawingView;
        defaultDuration = defaultDuration_ms;
    }

    public View getDrawingView(){
        return drawingView;
    }

    public void animateScoreBubbles(List<ScoreBubbleView> bubbles,
                                    PointF destination, float maxRadius, int delay_ms,
                                    Runnable actionOnEachEnd, Runnable actionOnLastEnd){

        int ms_between = 400;

        for (int i = 0; i < bubbles.size(); i++){
            int delay = delay_ms + (i * ms_between);
            Runnable onEnd = i == bubbles.size()-1
                    ? ()->{
                        actionOnEachEnd.run();
                        actionOnLastEnd.run();
                    }
                    : actionOnEachEnd;
            animateScoreBubble(bubbles.get(i), destination, maxRadius, delay, onEnd);
        }

    }

    public void animateScoreBubble(ScoreBubbleView bubble, PointF destination, float maxRadius, int delay_ms, Runnable actionOnEnd){
        int popInDuration = 700;
        int moveDelay = 400;
        int moveDuration = 1100;
        int fadeOutDelay = 900;
        int fadeOutDuration = 700;

        PointF maxSize = new PointF(maxRadius, maxRadius);

        ValueAnimator popIn = getSizeAnimator(bubble, maxSize, popInDuration);
        popIn.setInterpolator(new DecelerateInterpolator());
        ValueAnimator move = getPositionAnimator(bubble, destination, moveDuration);
        move.setInterpolator(new AccelerateInterpolator());
        move.setStartDelay(moveDelay);
        ValueAnimator popOut = getSizeAnimator(bubble, maxSize, new PointF(0,0), fadeOutDuration);
        popOut.setInterpolator(new AccelerateInterpolator());
        popOut.setStartDelay(fadeOutDelay);

        AnimatorSet set = actionOnEnd != null
                ? getAnimatorSet(actionOnEnd)
                : new AnimatorSet();

        set.setStartDelay(delay_ms);
        set.playTogether(popIn, move, popOut);
        set.start();
    }

    public AnimatorSet animateTrickCollectCard(CardView cardView, PointF destination, PointF endSize,
                                        int delay_ms, int duration_ms, Runnable actionOnEnd){
        AnimatorSet set = new AnimatorSet();
        List<Animator> animations = new ArrayList<>(2);

        animations.add(getPositionAnimator(cardView, destination, duration_ms));

        if (endSize != null)
            animations.add(getSizeAnimator(cardView, endSize, duration_ms));

        set.setInterpolator(new AccelerateInterpolator());
        set.setStartDelay(delay_ms);
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                actionOnEnd.run();
            }
        });
        set.playTogether(animations);
        set.start();
        return set;
    }

    public void animate(IAnimatable object, PointF destination){
        animate(object, destination,null, object.getRotation(), 0, defaultDuration, null);
    }

    public void animate(IAnimatable object, PointF destination, Runnable actionOnEnd){
        animate(object, destination,null, object.getRotation(), 0, defaultDuration, actionOnEnd);
    }

    public void animate(IAnimatable object, PointF destination, int delay_ms, int duration_ms, Runnable actionOnEnd){
        animate(object, destination,null, object.getRotation(), delay_ms, duration_ms, actionOnEnd);
    }

    public void animate(IAnimatable object, PointF destination, float endRotation){
        animate(object, destination,null, endRotation, 0, defaultDuration, null);
    }

    public void animate(IAnimatable object, PointF destination, float endRotation, Runnable actionOnEnd){
        animate(object, destination,null, endRotation, 0, defaultDuration, actionOnEnd);
    }

    public void animate(IAnimatable object, PointF destination, PointF endDimensions){
        animate(object, destination, endDimensions, object.getRotation(), 0, defaultDuration, null);
    }

    public void animate(IAnimatable object, PointF destination, PointF endDimensions, int delay_ms,
                        int duration_ms){
        animate(object, destination, endDimensions, object.getRotation(), delay_ms, duration_ms , null);
    }

    public void animate(IAnimatable object, PointF destination, PointF endDimensions, int startDelay, int duration, Runnable actionOnEnd){
        animate(object, destination, endDimensions, object.getRotation(), startDelay, duration, actionOnEnd);
    }

    public void animate(IAnimatable object, PointF destination, PointF endDimensions, float endRotation, int startDelay, int duration, Runnable actionOnEnd){
        AnimatorSet animatorSet = new AnimatorSet();

        ValueAnimator positionAnimator = getPositionAnimator(object, destination, duration);
        positionAnimator.setInterpolator(new DecelerateInterpolator());

        // add optional end-action
        if (actionOnEnd != null){
            positionAnimator.addListener(new AnimatorListenerAdapter() {

                @Override
                public void onAnimationEnd(android.animation.Animator animator) {
                    actionOnEnd.run();
                }
            });
        }

        if (endDimensions == null && object.getRotation() == endRotation) {
            positionAnimator.setStartDelay(startDelay);
            positionAnimator.start();
            return;
        }

        // optionally animate object size
        ValueAnimator sizeAnimator = getSizeAnimator(object, endDimensions, duration);
        sizeAnimator.setInterpolator(new DecelerateInterpolator());

        ValueAnimator rotationAnimator = getRotationAnimator(object, endRotation, duration);
        rotationAnimator.setInterpolator(new DecelerateInterpolator());

        if (endDimensions != null && object.getRotation() == endRotation)
            animatorSet.play(positionAnimator).with(sizeAnimator);
        else if (endDimensions == null)
            animatorSet.play(positionAnimator).with(rotationAnimator);
        else
            animatorSet.play(positionAnimator).with(sizeAnimator).with(rotationAnimator);

        animatorSet.setStartDelay(startDelay);
        animatorSet.start();

    }

    public AnimatorSet getAnimatorSet(Runnable actionOnEnd) {
        AnimatorSet set = new AnimatorSet();
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                actionOnEnd.run();
                drawingView.postInvalidate();
            }
        });
        return set;
    }

    public ValueAnimator getRotationAnimator(IAnimatable object, float endRotation, int duration_ms){
        return getRotationAnimator(object, object.getRotation(), endRotation, duration_ms);
    }
    public ValueAnimator getRotationAnimator(IAnimatable object, float startRotation, float endRotation, int duration_ms){
        ValueAnimator rotationAnimator = ValueAnimator.ofFloat(object.getRotation(), endRotation);
        rotationAnimator.setDuration(duration_ms);
        rotationAnimator.addUpdateListener(animator-> {
            object.setRotation((float)animator.getAnimatedValue());
            drawingView.postInvalidate();
        });
        return rotationAnimator;
    }

    public ValueAnimator getPositionAnimator(IAnimatable object, PointF destination, int duration_ms){
        return getPositionAnimator(object, object.getPosition(), destination, duration_ms);
    }

    public ValueAnimator getPositionAnimator(IAnimatable object, PointF startPosition, PointF destination, int duration_ms){
        ValueAnimator positionAnimator =
                ValueAnimator.ofObject(new PointFEvaluator(), startPosition, destination);
        positionAnimator.setDuration(duration_ms);
        positionAnimator.addUpdateListener(animator -> {
            object.setPosition((PointF)animator.getAnimatedValue());
            drawingView.postInvalidate();
        });
        return positionAnimator;
    }

    public ValueAnimator getSizeAnimator(IAnimatable object, PointF endSize, int duration_ms){
        return getSizeAnimator(object, object.getSize(), endSize, duration_ms);
    }

    public ValueAnimator getSizeAnimator(IAnimatable object, PointF startSize, PointF endSize, int duration_ms){
        ValueAnimator sizeAnimator =
                ValueAnimator.ofObject(new PointFEvaluator(), startSize, endSize);
        sizeAnimator.setDuration(duration_ms);
        sizeAnimator.addUpdateListener(animator ->{
            object.setSize((PointF) animator.getAnimatedValue());
            drawingView.postInvalidate();
        });
        return sizeAnimator;
    }

    public ValueAnimator getAlphaAnimator(IFadeable object, int endAlpha, int duration_ms){
        return getAlphaAnimator(object, object.getAlpha(), endAlpha, duration_ms);
    }
    public ValueAnimator getAlphaAnimator(IFadeable object, int startAlpha, int endAlpha, int duration_ms){
        ValueAnimator alphaAnimator = ValueAnimator.ofInt(startAlpha, endAlpha);
        alphaAnimator.setDuration(duration_ms);
        alphaAnimator.addUpdateListener(animator-> {
            object.setAlpha((int)animator.getAnimatedValue());
            drawingView.postInvalidate();
        });
        return  alphaAnimator;
    }
}
