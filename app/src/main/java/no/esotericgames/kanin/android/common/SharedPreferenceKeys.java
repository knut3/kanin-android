package no.esotericgames.kanin.android.common;

public final class SharedPreferenceKeys {
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String USERNAME = "userName";
}
