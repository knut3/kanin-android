package no.esotericgames.kanin.android.screens.game.rounds.trick;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;

import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IFadeable;

public class TrumpDrawInfoView implements IDrawable, IFadeable {
    private final String text;
    private final Paint bgPaint, textPaint;
    private final float textWidth;
    private final PointF textPos;
    private final RectF bgRect;
    private int alpha;

    public TrumpDrawInfoView(PointF centerPos, float textSize, String text) {
        this.text = text;
        bgPaint = getBgPaint();
        textPaint = getTextPaint(textSize);
        textWidth = textPaint.measureText(text);
        textPos = new PointF(centerPos.x - textWidth / 2, centerPos.y + textSize/2);
        bgRect = new RectF(textPos.x - textSize, centerPos.y - textSize,
                textPos.x + textWidth + textSize, centerPos.y + textSize*1.5f);
        alpha = 255;
    }

    private Paint getBgPaint() {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.parseColor("#99000000"));
        paint.setStyle(Paint.Style.FILL);
        return paint;
    }

    private Paint getTextPaint(float textSize) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.parseColor("#FFFFFF"));
        paint.setTextSize(textSize);
        return paint;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawRoundRect(bgRect, 10, 10, bgPaint);
        canvas.drawText(text, textPos.x, textPos.y, textPaint);
    }

    @Override
    public void setAlpha(int alpha) {
        this.alpha = alpha;
        bgPaint.setAlpha(alpha);
        textPaint.setAlpha(alpha);
    }

    @Override
    public int getAlpha() {
        return alpha;
    }
}
