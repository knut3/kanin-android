package no.esotericgames.kanin.android.models;

import java.util.List;

public class GameSetup {
    public String gameName;
    public List<String> usernames;
    public List<BotModel> bots;
    public boolean gameHasStarted;
}
