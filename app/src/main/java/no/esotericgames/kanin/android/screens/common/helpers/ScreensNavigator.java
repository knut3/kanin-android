package no.esotericgames.kanin.android.screens.common.helpers;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;

import no.esotericgames.kanin.android.screens.game.GameActivity;
import no.esotericgames.kanin.android.screens.availablelobbies.AvailableGamesActivity;
import no.esotericgames.kanin.android.screens.game.medalceremony.MedalCeremonyActivity;
import no.esotericgames.kanin.android.screens.leaderboard.LeaderboardActivity;
import no.esotericgames.kanin.android.screens.lobby.LobbyManagerActivity;
import no.esotericgames.kanin.android.screens.login.LoginActivity;
import no.esotericgames.kanin.android.screens.magickcards.MagickCardsActivity;
import no.esotericgames.kanin.android.screens.mainmenu.MainMenuActivity;
import no.esotericgames.kanin.android.screens.lobby.LobbyActivity;
import no.esotericgames.kanin.android.screens.registration.RegistrationActivity;

public class ScreensNavigator {
    private final Activity contextActivity;

    public ScreensNavigator(Activity contextActivity) {
        this.contextActivity = contextActivity;
    }


    public void toLogin() {
        Intent intent = new Intent(contextActivity, LoginActivity.class);
        contextActivity.startActivity(intent,
                ActivityOptions.makeSceneTransitionAnimation(contextActivity)
                        .toBundle());
    }

    public void toMainMenu(){
        toMainMenu(false);
    }

    public void toMainMenu(boolean showWelcome){
        Intent intent = new Intent(contextActivity, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(MainMenuActivity.EXTRA_SHOW_WELCOME, showWelcome);
        contextActivity.startActivity(intent,
                ActivityOptions.makeSceneTransitionAnimation(contextActivity)
                        .toBundle());
    }

    public void toAvailableLobbies(){
        Intent intent = new Intent(contextActivity, AvailableGamesActivity.class);
        contextActivity.startActivity(intent,
                ActivityOptions.makeSceneTransitionAnimation(contextActivity).toBundle());
    }

    public void toLobbyManager(){
        Intent intent = new Intent(contextActivity, LobbyManagerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        contextActivity.startActivity(intent);
    }

    public void toOpenLobbyManager(String gameName){
        Intent intent = new Intent(contextActivity, LobbyManagerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(LobbyManagerActivity.GAME_NAME_EXTRA_KEY, gameName);
        contextActivity.startActivity(intent);
    }

    public void toLobby(String gameName){
        Intent intent = new Intent(contextActivity, LobbyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(LobbyActivity.GAME_NAME_EXTRA_KEY, gameName);
        contextActivity.startActivity(intent);
    }

    public void toGameScreen(String gameName, boolean showRoundIntro){
        Intent intent = new Intent(contextActivity, GameActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(GameActivity.EXTRA_KEY_GAME_NAME, gameName);
        intent.putExtra(GameActivity.EXTRA_KEY_SHOW_ROUND_INTRO, showRoundIntro);
        contextActivity.startActivity(intent);
    }

    public void toRegistration() {
        Intent intent = new Intent(contextActivity, RegistrationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        contextActivity.startActivity(intent);
    }

    public void toMedalCeremony(int gameId) {
        Intent intent = new Intent(contextActivity, MedalCeremonyActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(MedalCeremonyActivity.EXTRA_KEY_GAME_ID, gameId);
        contextActivity.startActivity(intent);
    }

    public void toMagickCards() {
        contextActivity.startActivity(
                new Intent(contextActivity, MagickCardsActivity.class),
                ActivityOptions.makeSceneTransitionAnimation(contextActivity)
                        .toBundle());
    }

    public void toLeaderboard() {
        contextActivity.startActivity(
                new Intent(contextActivity, LeaderboardActivity.class),
                ActivityOptions.makeSceneTransitionAnimation(contextActivity)
                        .toBundle());
    }
}
