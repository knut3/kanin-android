package no.esotericgames.kanin.android.screens.game.rounds.common;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import no.esotericgames.kanin.android.models.Card;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IAnimatable;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IFadeable;

public class CardView implements IDrawable, IAnimatable, IFadeable {
    private final Card card;
    private Drawable drawable;
    private PointF position, size;
    private float rotation;
    private int alpha;
    private final Paint shadowPaint;
    private final Rect rect;
    private float cornerRadius;

    public CardView(Card card, Drawable drawable) {
        this.card = card;
        this.drawable = drawable;
        shadowPaint = new Paint();
        position = new PointF();
        size = new PointF();
        rect = new Rect();
        alpha = 255;
    }

    @Override
    public void setPosition(PointF position){
        this.position = position;
        rect.set((int) position.x, (int) position.y,
                (int) (position.x + size.x),
                (int) (position.y + size.y));
    }

    @Override
    public void setSize(PointF size){
        this.size = size;
        rect.right = rect.left + (int)size.x;
        rect.bottom = rect.top + (int) size.y;
        float shadowRadius = size.x/11;
        float xOffset = -shadowRadius / 3;
        float yOffset = xOffset / 2;
        shadowPaint.setShadowLayer(shadowRadius, xOffset, yOffset, Color.BLACK);
        cornerRadius = size.x/8;
    }

    @Override
    public void draw(Canvas canvas){

        canvas.save();
        canvas.rotate(rotation, rect.left, rect.top);

        canvas.drawRoundRect(rect.left, rect.top, rect.right, rect.bottom, cornerRadius, cornerRadius, shadowPaint);
        drawable.setBounds(rect);
        drawable.setAlpha(alpha);
        drawable.draw(canvas);

        canvas.restore();
    }

    public boolean isClicked(float x, float y){
        if (x < position.x) return false;
        if (x > position.x + size.x) return false;
        if (y < position.y) return false;
        if (y > position.y + size.y) return false;

        return true;
    }

    public Card getCard(){
        return card;
    }

    @Override
    public PointF getPosition() {
        return position;
    }

    @Override
    public PointF getSize() {
        return size;
    }

    @Override
    public float getRotation() {
        return rotation;
    }

    @Override
    public void setRotation(float angle) {
        this.rotation = angle;
    }

    public void setDrawable(Drawable drawable){
        this.drawable = drawable;
    }

    @Override
    public void setAlpha(int alpha) {
        this.alpha = alpha;
        shadowPaint.setAlpha(alpha);
    }

    @Override
    public int getAlpha() {
        return alpha;
    }
}

