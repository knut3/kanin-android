package no.esotericgames.kanin.android.screens.registration;

import android.text.Editable;
import android.text.TextWatcher;

import com.google.android.material.textfield.TextInputLayout;


public class RemoveErrorMessage implements TextWatcher {
    TextInputLayout[] inputLayouts;

    public RemoveErrorMessage(TextInputLayout... inputLayouts) {
        this.inputLayouts = inputLayouts;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        for (TextInputLayout inputLayout : inputLayouts)
            inputLayout.setError(null);
    }
}
