package no.esotericgames.kanin.android.screens.mainmenu;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.networking.NetworkStateService;
import no.esotericgames.kanin.android.screens.common.BaseActivity;
import no.esotericgames.kanin.android.screens.common.helpers.ScreensNavigator;
import no.esotericgames.kanin.android.common.SharedPreferenceKeys;
import no.esotericgames.kanin.android.screens.common.helpers.ToastMaster;

public class MainMenuActivity extends BaseActivity {

    public static final String EXTRA_SHOW_WELCOME = "showWelcome";
    private ScreensNavigator screensNavigator;
    private ToastMaster toastMaster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        screensNavigator = getCompositionRoot().getScreensNavigator();
        toastMaster = getCompositionRoot().getToastMaster();

        setContentView(R.layout.activity_main_menu);
        Button btnCreateGame = findViewById(R.id.btnMainMenuCreateGame);
        btnCreateGame.setOnClickListener(v -> createGame());

        Button btnJoinGame = findViewById(R.id.btnMainMenuJoinGame);
        btnJoinGame.setOnClickListener(v -> joinGame());

        Button btnMagickCards = findViewById(R.id.btnMainMenuMagickCards);
        btnMagickCards.setOnClickListener(v -> toMagickCards());

        Button btnLeaderboard = findViewById(R.id.btnMainMenuLeaderboard);
        btnLeaderboard.setOnClickListener(v -> toLeaderboard());

        if (getIntent().getExtras().getBoolean(EXTRA_SHOW_WELCOME)){
            SharedPreferences prefs = getCompositionRoot().getSharedPreferences();
            String username = prefs.getString(SharedPreferenceKeys.USERNAME, "");
            toastMaster.showSuccessToast(getString(R.string.toast_welcome_message, username));
        }
    }

    private void toLeaderboard() {
        if (NetworkStateService.isNetworkConnected)
            screensNavigator.toLeaderboard();
        else
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
    }

    private void createGame(){
        if (NetworkStateService.isNetworkConnected)
            screensNavigator.toLobbyManager();
        else
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
    }

    private void joinGame(){
        if (NetworkStateService.isNetworkConnected)
            screensNavigator.toAvailableLobbies();
        else
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
    }
    private void toMagickCards(){
        if (NetworkStateService.isNetworkConnected)
            screensNavigator.toMagickCards();
        else
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
    }
}