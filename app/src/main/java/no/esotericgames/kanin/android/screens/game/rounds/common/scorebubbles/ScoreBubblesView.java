package no.esotericgames.kanin.android.screens.game.rounds.common.scorebubbles;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import no.esotericgames.kanin.android.models.Player;
import no.esotericgames.kanin.android.screens.common.ResourceManager;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;
import no.esotericgames.kanin.android.screens.game.common.ViewAnimator;
import no.esotericgames.kanin.android.screens.game.scoreboard.MyScoresView;
import no.esotericgames.kanin.android.screens.game.scoreboard.ScoreBoardsView;

public class ScoreBubblesView implements IDrawable {
    public static final int MS_BETWEEN_BUBBLES = 700;
    private final ViewAnimator viewAnimator;
    private final ResourceManager resourceManager;
    private final MyScoresView myScoresView;
    private final ScoreBoardsView scoreBoardsView;
    private final Player me;

    private final List<ScoreBubbleView> scoreBubbleViews;

    public ScoreBubblesView(ViewAnimator viewAnimator, ResourceManager resourceManager,
                            MyScoresView myScoresView, ScoreBoardsView scoreBoardsView,
                            Player me) {
        this.viewAnimator = viewAnimator;
        this.resourceManager = resourceManager;
        this.myScoresView = myScoresView;
        this.scoreBoardsView = scoreBoardsView;
        this.me = me;
        scoreBubbleViews = new ArrayList<>(6);
    }


    @Override
    public void draw(Canvas canvas) {
        for (ScoreBubbleView bubbleView : scoreBubbleViews)
            bubbleView.draw(canvas);
    }

    public List<AnimatorSet> createTrickScoreBubbles(int score, String owner, int delay_ms, PointF startPosition,
                                           float maxRadius, Runnable actionOnEnd){

        PointF startSize = new PointF(0, 0);
        PointF destination = owner.equals(me.getUsername()) ? myScoresView.getCenterPosition() : scoreBoardsView.getCenterBetweenScores(owner);

        int absoluteScore = Math.abs(score);
        int scorePerBubble = score < 0 ? -1 : 1;
        List<ScoreBubbleView> theseBubbles = new ArrayList<>(absoluteScore);

        for (int i = 0; i < absoluteScore; i++){
            ScoreBubbleView bubble = new SignScoreBubbleView(scorePerBubble, resourceManager);
            bubble.setSize(startSize);
            bubble.setPosition(startPosition);
            theseBubbles.add(bubble);
        }

        List<ScoreBubbleView> theseBubblesReversed = new ArrayList<>(theseBubbles);
        Collections.reverse(theseBubblesReversed);
        // reversed so that older bubbles overlap newer
        scoreBubbleViews.addAll(theseBubblesReversed);

        Runnable onEachEnd = ()->addScore(scorePerBubble, owner);
        Runnable onLastEnd = ()-> {
            scoreBubbleViews.removeAll(theseBubbles);
            if (actionOnEnd != null) actionOnEnd.run();
        };

        return animateScoreBubbles(theseBubbles, destination,
                maxRadius, delay_ms, onEachEnd, onLastEnd);
    }

    public void createKabalPassScoreBubble(String owner, float maxRadius, Runnable actionOnEnd){
        float startPositionMarginFraction = 0.25f;


        float startDistanceFromScoreBoard = (maxRadius * startPositionMarginFraction) + (maxRadius/2);
        PointF startPosition, destination;

        if (owner.equals(me.getUsername())){
            float start_x = myScoresView.getCenterPosition().x;
            float start_y = myScoresView.getTop() - startDistanceFromScoreBoard;
            startPosition = new PointF(start_x, start_y);
            destination = myScoresView.getTotalScoreIconCenter();
        }
        else{
            float distanceFromScoreBoardCenter = scoreBoardsView.getScoreBoardHeight(owner) / 2 + startDistanceFromScoreBoard;
            startPosition = scoreBoardsView.getPointBelowScoreBoardCenter(owner, distanceFromScoreBoardCenter);
            destination = scoreBoardsView.getTotalScoreIconCenter(owner);
        }

        PointF startSize = new PointF(0, 0);

        ScoreBubbleView bubble = new TextScoreBubbleView(1, "PASS", resourceManager);
        bubble.setSize(startSize);
        bubble.setPosition(startPosition);
        scoreBubbleViews.add(bubble);

        Runnable onEnd = ()-> {
            addTotalScore(1, owner);
            scoreBubbleViews.remove(bubble);
            if (actionOnEnd != null) actionOnEnd.run();
        };

        animateScoreBubble(bubble, destination,
                maxRadius, 0, onEnd);
    }

    public void createKabalEndCardCountScoreBubbles(int numCards, String owner, int delay_ms,
                                        float maxRadius, Runnable actionOnEnd){

        PointF startSize = new PointF(0, 0);

        PointF startPosition, destination;

        if (owner.equals(me.getUsername())){
            startPosition = myScoresView.getRoundScoreIconCenter();
            destination = myScoresView.getTotalScoreIconCenter();
        }
        else{
            startPosition = scoreBoardsView.getRoundScoreIconCenter(owner);
            destination = scoreBoardsView.getTotalScoreIconCenter(owner);
        }

        List<ScoreBubbleView> theseBubbles = new ArrayList<>(numCards);

        for (int i = 0; i < numCards; i++){
            ScoreBubbleView bubble = new PlainScoreBubbleView(1, resourceManager, true);
            bubble.setSize(startSize);
            bubble.setPosition(startPosition);
            theseBubbles.add(bubble);
        }

        List<ScoreBubbleView> theseBubblesReversed = new ArrayList<>(theseBubbles);
        Collections.reverse(theseBubblesReversed);
        // reversed so that older bubbles overlap newer
        scoreBubbleViews.addAll(theseBubblesReversed);

        Runnable onEachEnd = ()->{
            addRoundScore(-1, owner);
            addTotalScore(1, owner);
        };

        Runnable onLastEnd = ()-> {
            scoreBubbleViews.removeAll(theseBubbles);
            if (actionOnEnd != null) actionOnEnd.run();
        };

        animateScoreBubbles(theseBubbles, destination,
                maxRadius, delay_ms, onEachEnd, onLastEnd);
    }


    private List<AnimatorSet> animateScoreBubbles(List<ScoreBubbleView> bubbles,
                                    PointF destination, float maxRadius, int delay_ms,
                                    Runnable actionOnEachEnd, Runnable actionOnLastEnd){

        List<AnimatorSet> animators = new ArrayList<>(bubbles.size());

        for (int i = 0; i < bubbles.size(); i++){
            int delay = delay_ms + (i * MS_BETWEEN_BUBBLES);
            Runnable onEnd = i == bubbles.size()-1
                    ? ()->{
                        actionOnEachEnd.run();
                        actionOnLastEnd.run();
                        }
                    : actionOnEachEnd;

            AnimatorSet animator = animateScoreBubble(bubbles.get(i), destination, maxRadius, delay, onEnd);
            animators.add(animator);
        }

        return animators;
    }

    private AnimatorSet animateScoreBubble(ScoreBubbleView bubble, PointF destination, float maxRadius, int delay_ms, Runnable actionOnEnd){
        int popInDuration = 700;
        int moveDelay = 400;
        int moveDuration = 1100;
        int fadeOutDelay = 900;
        int fadeOutDuration = 700;

        PointF maxSize = new PointF(maxRadius, maxRadius);

        ValueAnimator popIn = viewAnimator.getSizeAnimator(bubble, maxSize, popInDuration);
        popIn.setInterpolator(new DecelerateInterpolator());
        ValueAnimator move = viewAnimator.getPositionAnimator(bubble, destination, moveDuration);
        move.setInterpolator(new AccelerateInterpolator());
        move.setStartDelay(moveDelay);
        ValueAnimator popOut = viewAnimator.getSizeAnimator(bubble, maxSize, new PointF(0,0), fadeOutDuration);
        popOut.setInterpolator(new AccelerateInterpolator());
        popOut.setStartDelay(fadeOutDelay);

        AnimatorSet set = actionOnEnd != null
                ? viewAnimator.getAnimatorSet(actionOnEnd)
                : new AnimatorSet();

        set.setStartDelay(delay_ms);
        set.playTogether(popIn, move, popOut);
        set.start();
        return set;
    }

    private void addScore(int score, String owner){
        if (owner.equals(me.getUsername())){
            myScoresView.addScore(score);
        }
        else {
            scoreBoardsView.addScore(score, owner);
        }
    }

    private void addTotalScore(int score, String owner){
        if (owner.equals(me.getUsername())){
            myScoresView.addTotalScore(score);
        }
        else {
            scoreBoardsView.getScoreBoardView(owner)
                    .addTotalScore(score);
        }
    }

    private void addRoundScore(int score, String owner){
        if (owner.equals(me.getUsername())){
            myScoresView.addRoundScore(score);
        }
        else {
            scoreBoardsView.getScoreBoardView(owner)
                    .addRoundScore(score);
        }
    }

}
