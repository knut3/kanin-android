package no.esotericgames.kanin.android.screens.launch;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.core.Observable;
import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.screens.common.helpers.ScreensNavigator;
import no.esotericgames.kanin.android.networking.halfduplex.ApiService;
import no.esotericgames.kanin.android.models.GetOpenLobbyModel;
import no.esotericgames.kanin.android.common.SharedPreferenceKeys;
import no.esotericgames.kanin.android.networking.duplex.BaseSignalRRepository;
import no.esotericgames.kanin.android.networking.duplex.LaunchRepository;
import no.esotericgames.kanin.android.screens.common.BaseActivity;
import no.esotericgames.kanin.android.screens.common.dialogs.DialogsEventBus;
import no.esotericgames.kanin.android.screens.common.dialogs.DialogsManager;
import no.esotericgames.kanin.android.screens.common.dialogs.promptdialog.PromptDialogEvent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LaunchActivity extends BaseActivity implements BaseSignalRRepository.Listener, DialogsEventBus.Listener {

    private static final String TAG = "LaunchActivity";
    private DialogsManager dialogsManager;
    private DialogsEventBus dialogsEventBus;
    private LaunchRepository launchRepository;
    private ScreensNavigator screensNavigator;
    private String runningGameName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialogsManager = getCompositionRoot().getDialogsManager();
        dialogsEventBus = getCompositionRoot().getDialogsEventBus();
        screensNavigator = getCompositionRoot().getScreensNavigator();
        setContentView(R.layout.activity_launch);
    }

    @Override
    protected void onStart() {
        super.onStart();

        dialogsEventBus.registerListener(this);

        SharedPreferences prefs = getCompositionRoot().getSharedPreferences();

        if (!prefs.contains(SharedPreferenceKeys.ACCESS_TOKEN)){
            Log.d(TAG, "onCreate: Goto Registration");
            screensNavigator.toRegistration();
        }
        else{
            String accessToken = prefs.getString(SharedPreferenceKeys.ACCESS_TOKEN, "");
            Log.d(TAG, "onCreate: found access token "+accessToken+" - Checking validity");
            new ApiService().getUserAPI().isValidToken(accessToken).enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    if (response.isSuccessful() && response.body()){
                        Log.d(TAG, "onResponse: Access token is VALID");
                        onValidAccessToken();
                    }
                    else{
                        Log.d(TAG, "onResponse: Access token NOT VALID. Goto registration");
                        screensNavigator.toRegistration();
                    }
                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Log.d(TAG, "onFailure to WebApi: Exception: " + t.getMessage());
                    showDisconnectedOverlay();
                    Observable
                            .interval(2, TimeUnit.SECONDS)
                            .subscribe(count -> closeApp());
                }
            });
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        dialogsEventBus.unregisterListener(this);
        if (launchRepository != null)
            launchRepository.unbind();
    }

    private void onValidAccessToken(){
        launchRepository = getCompositionRoot().getLaunchRepository();
        launchRepository.bind(this);
    }

    /// -------------------- Launch Repository events----------------------------


    @Override
    public void onConnectedToServer() {
        String gameName = launchRepository.getRunningGameBlocking();

        if (gameName != null){
            runningGameName = gameName;
            String enhancedGameName = getCompositionRoot().getGameNameEnhancer()
                            .getEnhanced(gameName);
            dialogsManager.showGotoRunningGameDialog(enhancedGameName);
            return;
        }

        GetOpenLobbyModel openLobby = launchRepository.getOpenLobbyBlocking();

        if (openLobby == null){
            screensNavigator.toMainMenu(true);
            return;
        }

        if (openLobby.isCreator)
            screensNavigator.toOpenLobbyManager(openLobby.gameName);
        else
            screensNavigator.toLobby(openLobby.gameName);
    }

    @Override
    public void onServerMaintenanceInProgress(int duration_min) {
        super.showAnnouncementOverlay(getString(R.string.announcement_server_maintenance, duration_min));
    }

    @Override
    public void onMessageFromAdmin(String message) {
        super.showAnnouncementOverlay(message);
    }

    /// -------------------- Dialog events----------------------------

    @Override
    public void onDialogEvent(Object event) {
        if (event instanceof PromptDialogEvent){
            PromptDialogEvent promptEvent = (PromptDialogEvent) event;
            switch (dialogsManager.getShownDialogTag()){
                case DialogsManager.TAG_GOTO_RUNNING_GAME_PROMPT:
                    handleGotoRunningGameDialogEvent(promptEvent);
                    break;
            }
        }
    }

    private void handleGotoRunningGameDialogEvent(PromptDialogEvent event) {
        switch (event.getClickedButton()){
            case POSITIVE:
                screensNavigator.toGameScreen(runningGameName, false);
                break;
            case NEGATIVE:
                screensNavigator.toMainMenu(true);
        }
    }

    /// -------------------- Private Methods----------------------------

    private void closeApp(){
        runOnUiThread(this::finishAndRemoveTask);
    }
}