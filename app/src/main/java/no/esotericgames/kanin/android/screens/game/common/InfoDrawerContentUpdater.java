package no.esotericgames.kanin.android.screens.game.common;

import no.esotericgames.kanin.android.R;

import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.StringRes;

import no.esotericgames.kanin.android.models.Card;
import no.esotericgames.kanin.android.models.GameState;
import no.esotericgames.kanin.android.models.Round;
import no.esotericgames.kanin.android.models.Rounds;
import no.esotericgames.kanin.android.screens.common.ResourceManager;

public class InfoDrawerContentUpdater {

    private final ResourceManager resourceManager;
    private final TextView tvRoundHeader, tvGameType, tvRoundGoal,
            tvMissingCards, tvScoring, tvTrumpSuit, tvRoundRules;

    public InfoDrawerContentUpdater(ViewGroup infoDrawer, ResourceManager resourceManager) {

        this.resourceManager = resourceManager;
        tvRoundHeader = infoDrawer.findViewById(R.id.info_round_header);
        tvGameType = infoDrawer.findViewById(R.id.info_game_type);
        tvRoundGoal = infoDrawer.findViewById(R.id.info_round_goal);
        tvMissingCards = infoDrawer.findViewById(R.id.info_missing_cards);
        tvScoring = infoDrawer.findViewById(R.id.info_scoring);
        tvTrumpSuit = infoDrawer.findViewById(R.id.info_trump_suit);
        tvRoundRules = infoDrawer.findViewById(R.id.info_round_rules);
    }

    public void update(GameState gameState){
        tvRoundHeader.setText(getRoundHeader(gameState.round));

        RoundInfoResources roundInfoResources = new RoundInfoResources(gameState);

        tvGameType.setText(roundInfoResources.gameType);
        tvRoundGoal.setText(roundInfoResources.roundGoal);
        tvScoring.setText(roundInfoResources.scoring);
        tvRoundRules.setText(roundInfoResources.roundRules);

        if (roundInfoResources.missingCards != null){
            tvMissingCards.setText(roundInfoResources.missingCards);
            tvMissingCards.setVisibility(View.VISIBLE);
        }
        else{
            tvMissingCards.setVisibility(View.GONE);
        }

        if (roundInfoResources.trumpSuitHeader != null){
            String trumpHeader = getTrumpHeader(roundInfoResources.trumpSuitHeader);
            if (gameState.trumpSuit != null)
                trumpHeader += getSuitIconHtmlCode(gameState.trumpSuit);
            tvTrumpSuit.setText(Html.fromHtml(trumpHeader, Html.FROM_HTML_MODE_COMPACT));
            tvTrumpSuit.setVisibility(View.VISIBLE);
        }
        else tvTrumpSuit.setVisibility(View.GONE);

    }

    public void setTrumpSuit(String suit){
        String text = getTrumpHeader(R.string.info_trump_suit_header)
                        + getSuitIconHtmlCode(suit);
        tvTrumpSuit.setText(Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT));
    }

    private String getTrumpHeader(@StringRes int trumpSuitHeader) {
        return "<b>" + resourceManager.getString(trumpSuitHeader) + "</b> ";
    }

    private String getRoundHeader(Round round){
        String roundNameLocalized = resourceManager.getString(getRoundNameStringResId(round.name));
        return resourceManager.getString(R.string.round_header, round.number,
                roundNameLocalized);
    }

    public static @StringRes int getRoundNameStringResId(String roundName){

        switch (roundName){
            case Rounds.Pass: return R.string.round_name_pass;
            case Rounds.Spar: return R.string.round_name_spar;
            case Rounds.Damer: return R.string.round_name_damer;
            case Rounds.Kabal: return R.string.round_name_kabal;
            case Rounds.Grand: return R.string.round_name_grand;
            default: return R.string.round_name_trumf;
        }
    }

    public static @StringRes int getShortDescriptionResId(String roundName){
        switch (roundName){
            case Rounds.Pass: return R.string.short_description_pass;
            case Rounds.Spar: return R.string.short_description_spar;
            case Rounds.Damer: return R.string.short_description_damer;
            case Rounds.Kabal: return R.string.short_description_kabal;
            case Rounds.Grand: return R.string.short_description_grand;
            default: return R.string.short_description_trumf;
        }
    }

    public static String getSuitIconHtmlCode(String suit){

        switch (suit){
            case Card.Suits.CLUBS: return "&#9827;";
            case Card.Suits.DIAMONDS: return "&#9830;";
            case Card.Suits.HEARTS: return "&#9829;";
            case Card.Suits.SPADES: return "&#9824;";
            default: return "";
        }
    }

    private static class RoundInfoResources{
        public @StringRes Integer gameType, roundGoal, missingCards,
                scoring, trumpSuitHeader, roundRules;

        public RoundInfoResources(GameState gameState) {

            if (gameState.opponents.length == 2){
                missingCards = R.string.info_missing_one_card;
            }
            if (gameState.opponents.length == 4)
                missingCards = R.string.info_missing_two_cards;



            switch (gameState.round.name){
                case Rounds.Pass:
                    gameType = R.string.info_gametype_trick;
                    roundGoal = R.string.info_round_goal_pass;
                    scoring = R.string.info_scoring_pass;
                    roundRules = R.string.info_trick_round_rules;
                    break;
                case Rounds.Spar:
                    gameType = R.string.info_gametype_trick;
                    roundGoal = R.string.info_round_goal_spar;
                    scoring = R.string.info_scoring_spar;
                    roundRules = R.string.info_trick_round_rules;
                    break;
                case Rounds.Damer:
                    gameType = R.string.info_gametype_trick;
                    roundGoal = R.string.info_round_goal_damer;
                    scoring = R.string.info_scoring_damer;
                    roundRules = R.string.info_trick_round_rules;
                    break;
                case Rounds.Kabal:
                    gameType = R.string.info_gametype_kabal;
                    roundGoal = R.string.info_round_goal_kabal;
                    scoring = R.string.info_scoring_kabal;
                    roundRules = R.string.info_cabal_round_rules;
                    break;
                case Rounds.Grand:
                    gameType = R.string.info_gametype_trick;
                    roundGoal = R.string.info_round_goal_grand;
                    scoring = R.string.info_scoring_grand;
                    roundRules = R.string.info_trick_round_rules;
                    break;
                case Rounds.Trumf:
                    gameType = R.string.info_gametype_trick;
                    roundGoal = R.string.info_round_goal_trumf;
                    scoring = R.string.info_scoring_trumf;
                    trumpSuitHeader = R.string.info_trump_suit_header;
                    roundRules = R.string.info_trump_round_rules;
                    break;
            }
        }


    }
}
