package no.esotericgames.kanin.android.screens.common.canvasinterfaces;

public interface IFadeable {
    void setAlpha(int alpha);
    int getAlpha();
}
