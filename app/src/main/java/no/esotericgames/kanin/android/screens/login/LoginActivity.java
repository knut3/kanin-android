package no.esotericgames.kanin.android.screens.login;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.networking.halfduplex.UserAPI;
import no.esotericgames.kanin.android.models.LoginModel;
import no.esotericgames.kanin.android.common.SharedPreferenceKeys;
import no.esotericgames.kanin.android.screens.registration.RemoveErrorMessage;
import no.esotericgames.kanin.android.screens.common.BaseActivity;
import no.esotericgames.kanin.android.screens.common.helpers.ScreensNavigator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {
    private static final String TAG = "LoginActivity";
    private UserAPI userAPI;
    private ScreensNavigator screensNavigator;
    private TextInputEditText edUsername;
    private TextInputEditText edPassword;
    private TextInputLayout layoutUsername;
    private TextInputLayout layoutPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userAPI = getActivityCompositionRoot().getUserApi();
        screensNavigator = getCompositionRoot().getScreensNavigator();

        layoutUsername = findViewById(R.id.layoutLoginUsername);
        layoutPassword = findViewById(R.id.layoutLoginPassword);
        edUsername = findViewById(R.id.edLoginUsername);
        edPassword = findViewById(R.id.edLoginPassword);

        RemoveErrorMessage removeErrorMessages = new RemoveErrorMessage(layoutUsername, layoutPassword);
        edUsername.addTextChangedListener(removeErrorMessages);
        edPassword.addTextChangedListener(removeErrorMessages);

        Button btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(v -> login());

    }

    private void login(){
        String username = edUsername.getText().toString();
        String password = edPassword.getText().toString();

        if (username.isEmpty()){
            layoutUsername.setError(getString(R.string.login_error_no_username));
            return;
        }

        if (password.isEmpty()){
            layoutPassword.setError(getString(R.string.login_error_no_password));
        }

        userAPI.login(new LoginModel(username, password)).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()){
                    // save username and access token locally
                    SharedPreferences sharedPreferences = getCompositionRoot().getSharedPreferences();
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(SharedPreferenceKeys.USERNAME, username);
                    editor.putString(SharedPreferenceKeys.ACCESS_TOKEN, response.body());
                    editor.apply();

                    screensNavigator.toMainMenu(true);
                }
                else if (response.code() == 400){
                    layoutUsername.setError(getString(R.string.login_error_wrong_credentials));
                }
                else{
                    Toast.makeText(LoginActivity.this,
                            "Request failed with HTTP status code " + response.code(),
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, t.getMessage());
                Toast.makeText(LoginActivity.this, getString(R.string.login_error_unknown), Toast.LENGTH_LONG).show();
            }
        });

    }
}