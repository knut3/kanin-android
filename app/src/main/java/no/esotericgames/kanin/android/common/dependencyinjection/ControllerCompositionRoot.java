package no.esotericgames.kanin.android.common.dependencyinjection;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import no.esotericgames.kanin.android.networking.duplex.GameRepository;
import no.esotericgames.kanin.android.networking.halfduplex.ScoreAPI;
import no.esotericgames.kanin.android.networking.halfduplex.StatisticsAPI;
import no.esotericgames.kanin.android.networking.halfduplex.UserAPI;
import no.esotericgames.kanin.android.networking.duplex.LaunchRepository;
import no.esotericgames.kanin.android.networking.duplex.LobbiesRepository;
import no.esotericgames.kanin.android.networking.duplex.LobbyRepository;
import no.esotericgames.kanin.android.screens.common.helpers.GameNameEnhancer;
import no.esotericgames.kanin.android.screens.common.helpers.ScreensNavigator;
import no.esotericgames.kanin.android.screens.common.helpers.ToastMaster;
import no.esotericgames.kanin.android.screens.common.viewmvc.ViewMvcFactory;
import no.esotericgames.kanin.android.screens.common.dialogs.DialogsEventBus;
import no.esotericgames.kanin.android.screens.common.dialogs.DialogsManager;
import no.esotericgames.kanin.android.screens.common.ResourceManager;

public class ControllerCompositionRoot {

    private final ActivityCompositionRoot mActivityCompositionRoot;
    private ResourceManager resourceManager;

    public ControllerCompositionRoot(ActivityCompositionRoot activityCompositionRoot) {
        mActivityCompositionRoot = activityCompositionRoot;
    }

    private FragmentActivity getActivity() {
        return mActivityCompositionRoot.getActivity();
    }

    private Context getContext() {
        return getActivity();
    }

    private FragmentManager getFragmentManager() {
        return getActivity().getSupportFragmentManager();
    }

    public UserAPI getUserApi() {
        return mActivityCompositionRoot.getUserApi();
    }

    public ScoreAPI getScoreApi() {
        return mActivityCompositionRoot.getScoreApi();
    }

    public StatisticsAPI getStatisticsApi() {
        return mActivityCompositionRoot.getStatisticsApi();
    }

    public ResourceManager getResourceManager(){
        if (resourceManager == null)
            resourceManager = new ResourceManager(getContext());

        return resourceManager;
    }

    private LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(getContext());
    }

    public ViewMvcFactory getViewMvcFactory() {
        return new ViewMvcFactory(getLayoutInflater());
    }

    public ScreensNavigator getScreensNavigator() {
        return new ScreensNavigator(getActivity());
    }

    public DialogsManager getDialogsManager() {
        return new DialogsManager(getContext(), getFragmentManager());
    }

    public DialogsEventBus getDialogsEventBus() {
        return mActivityCompositionRoot.getDialogsEventBus();
    }

    public SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(getContext());
    }

    public LaunchRepository getLaunchRepository() {
        return new LaunchRepository(getContext(), getUiHandler());
    }

    public LobbiesRepository getLobbiesRepository() {
        return new LobbiesRepository(getContext(), getUiHandler());
    }

    public LobbyRepository getLobbyRepository() {
        return new LobbyRepository(getContext(), getUiHandler());
    }

    public GameRepository getGameRepository(String gameName) {
        return new GameRepository(getContext(), getUiHandler(), gameName);
    }

    public Handler getUiHandler() {
        return new Handler(Looper.getMainLooper());
    }

    public ToastMaster getToastMaster() {
        return new ToastMaster(getContext(), getResourceManager());
    }

    public GameNameEnhancer getGameNameEnhancer(){
        return new GameNameEnhancer(getResourceManager());
    }
}