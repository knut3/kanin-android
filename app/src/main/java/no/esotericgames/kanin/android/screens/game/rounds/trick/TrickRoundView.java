package no.esotericgames.kanin.android.screens.game.rounds.trick;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;
import android.util.Pair;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.CardWithOwner;
import no.esotericgames.kanin.android.models.GameState;
import no.esotericgames.kanin.android.models.Rounds;
import no.esotericgames.kanin.android.models.TrickResult;
import no.esotericgames.kanin.android.models.Card;
import no.esotericgames.kanin.android.models.Player;
import no.esotericgames.kanin.android.screens.common.helpers.ToastMaster;
import no.esotericgames.kanin.android.screens.common.ResourceManager;
import no.esotericgames.kanin.android.screens.game.common.InfoDrawerContentUpdater;
import no.esotericgames.kanin.android.screens.game.common.ViewAnimator;
import no.esotericgames.kanin.android.screens.game.rounds.common.CardView;
import no.esotericgames.kanin.android.screens.game.rounds.common.MyCardsView;
import no.esotericgames.kanin.android.screens.game.rounds.common.PlayingSurfaceModel;
import no.esotericgames.kanin.android.screens.game.rounds.common.RoundView;
import no.esotericgames.kanin.android.screens.game.rounds.common.TableCard;
import no.esotericgames.kanin.android.screens.game.turnindicator.TurnIndicatorAnimator;

public class TrickRoundView extends RoundView {
    private static final int COLLECT_TRICK_DELAY_MS = 2700;
    private static final int SCORE_BUBBLE_DELAY_MS = 2300;
    private final float scoreBubbleMaxRadius;
    private final List<TableCard> collectingCards = new ArrayList<>(5);
    private final List<Pair<AnimatorSet, Runnable>> collectingCardAnimators = new ArrayList<>(5);
    private final List<AnimatorSet> scoreBubbleAnimators = new ArrayList<>();

    public TrickRoundView(Point screenSize, GameState gameState, Player me,
                          ViewAnimator cardAnimator, TurnIndicatorAnimator turnIndicatorAnimator,
                          ResourceManager resourceManager, ToastMaster toastMaster) {
        super(screenSize, gameState, me, cardAnimator, turnIndicatorAnimator, resourceManager, toastMaster);
        scoreBubbleMaxRadius = screenSize.y / 12f;
    }

    public void collectTrick(TrickResult result, Runnable actionOnEnd){
        collectingCardAnimators.clear();
        scoreBubbleAnimators.clear();
        collectingCards.clear();

        PointF cardDestination, endSize;

        if (result.winner.equals(me.getUsername())){
            cardDestination = getMyTrickDestination();
            endSize = null;
        }
        else{
            cardDestination = getOpponentTrickDestination(result.winner);
            PointF currentSize = getTableCardSize(playingSurfaceModel);
            int shrinkFactor = 3;
            endSize = new PointF(currentSize.x / shrinkFactor, currentSize.y / shrinkFactor);
        }

        Runnable actionAfterCardCollection;

        if (result.pointsGained == 0){
            actionAfterCardCollection = ()->{
                collectingCards.clear();
                if (actionOnEnd != null) actionOnEnd.run();
            };
        }
        else {
            actionAfterCardCollection = ()-> {
                collectingCards.clear();
                collectingCardAnimators.clear();
            };
            List<AnimatorSet> bubbleAnimators = scoreBubblesView.createTrickScoreBubbles(result.pointsGained, result.winner,
                    SCORE_BUBBLE_DELAY_MS, playingSurfaceModel.center, scoreBubbleMaxRadius, actionOnEnd);
            scoreBubbleAnimators.addAll(bubbleAnimators);
        }

        collectingCards.addAll(tableCards);
        tableCards.clear();

        for (int i = 0; i < collectingCards.size(); i++){
            TableCard collectedCard = collectingCards.get(i);
            AnimatorSet animatorSet = viewAnimator.animateTrickCollectCard(
                    collectedCard.getCardView(), cardDestination, endSize,
                    COLLECT_TRICK_DELAY_MS, 800, actionAfterCardCollection);
            Runnable endAction = i < collectingCards.size()-1 ? null : actionAfterCardCollection;
            collectingCardAnimators.add(new Pair<>(animatorSet, endAction));
        }

    }

    @Override
    protected void throwMyCard(CardView cardView) {
        super.throwMyCard(cardView);
        speedUpCollectTrickAnimations();
    }

    @Override
    public void throwOpponentCard(CardWithOwner cwo, Runnable actionOnEnd) {
        super.throwOpponentCard(cwo, actionOnEnd);
        speedUpCollectTrickAnimations();
    }

    @Override
    protected boolean useCardsLeftAsRoundScore() {
        return false;
    }

    @Override
    protected boolean isValidCardToThrow(Card card) {
        if (tableCards.isEmpty()) return true;
        String leadingSuit = tableCards.get(0).getCardView().getCard().suit;
        if (card.suit.equals(leadingSuit)) return true;
        else if (!myCardsView.containSuit(leadingSuit)) return true;
        else return false;
    }

    @Override
    protected String getInvalidCardReason(Card card) {
        String leadSuit = tableCards.get(0).getCardView().getCard().suit;
        String suitIcon = InfoDrawerContentUpdater.getSuitIconHtmlCode(leadSuit);
        return resourceManager.getString(R.string.illegal_move_follow_suit, suitIcon);
    }

    @Override
    protected PointF getTableCardPosition(Card card, String owner, PlayingSurfaceModel playingSurface) {
        return playingSurface.center;
    }

    @Override
    protected PointF getTableCardSize(PlayingSurfaceModel playingSurface) {
        float surfaceHeight = playingSurface.bottom - playingSurface.top;
        float height = surfaceHeight / 3;
        float width = height / MyCardsView.CARD_WIDTH_HEIGHT_RATIO;
        return new PointF(width, height);
    }

    @Override
    protected boolean shouldUseDefaultTableCardRotation() {
        return false;
    }

    @Override
    protected void cleanupAfterThrownCard(Card thrownCard) {
        // none needed. Abstract method for Kabal round
    }

    @Override
    protected void drawOnTable(Canvas canvas) {
        for (TableCard card : collectingCards){
            card.getCardView().draw(canvas);
        }
    }

    @NonNull
    private PointF getMyTrickDestination() {
        return new PointF(screenSize.x/2f,
                screenSize.y + getTableCardSize(playingSurfaceModel).y);
    }

    @NonNull
    private PointF getOpponentTrickDestination(String opponent) {
        return scoreBoardsView.getScoreBoardCenter(opponent);
    }

    private void speedUpCollectTrickAnimations(){
        if (collectingCardAnimators.size() > 0){
            for (AnimatorSet anim : scoreBubbleAnimators){
                anim.end();
            }
            scoreBubbleAnimators.clear();
        }

        for (Pair<AnimatorSet, Runnable> x : collectingCardAnimators){
            AnimatorSet animatorSet = x.first;
            if (animatorSet.isRunning()) continue;

            animatorSet.removeAllListeners();
            animatorSet.cancel();

            if (x.second != null){
                animatorSet.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        x.second.run();
                        collectingCardAnimators.clear();
                    }
                });
            }
            animatorSet.setStartDelay(0);
            animatorSet.start();
        }

    }
}
