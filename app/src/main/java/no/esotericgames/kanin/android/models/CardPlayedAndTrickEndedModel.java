package no.esotericgames.kanin.android.models;

public class CardPlayedAndTrickEndedModel {
    public CardWithOwner card;
    public TrickResult trickResult;
}
