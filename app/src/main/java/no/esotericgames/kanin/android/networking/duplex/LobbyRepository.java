package no.esotericgames.kanin.android.networking.duplex;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;

import com.microsoft.signalr.TypeReference;

import java.lang.reflect.Type;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import no.esotericgames.kanin.android.models.AddBotModel;
import no.esotericgames.kanin.android.models.BotModel;
import no.esotericgames.kanin.android.models.GameSetup;
import no.esotericgames.kanin.android.models.JoinLobbyModel;
import no.esotericgames.kanin.android.models.RemoveBotModel;

public class LobbyRepository extends BaseSignalRRepository<LobbyRepository.Listener>{

    public interface Listener extends BaseSignalRRepository.Listener{
        void onGotGameSetup(GameSetup setup);
        void onUserJoined(String username);
        void onUserLeft(String username);
        void onLobbyClosed();
        void onGameStarting();
        void onBotAdded(BotModel model);
        void onBotRemoved(String name);
    }

    public LobbyRepository(Context bindingContext, Handler uiHandler) {
        super(bindingContext, uiHandler);
    }

    //---------Server API-------------------------------

    /**
     *
     * @return game name
     */
    public String createLobbyBlocking(){
        try {
            return hubConnection.invoke(String.class, "CreateLobby")
                    .retry(RETRY_COUNT)
                    .blockingGet();
        }
        catch (Exception e){
            logError(e);
            return null;
        }
    }

    public boolean joinLobbyBlocking(String gameName){
        try{
            return hubConnection.invoke(Boolean.class,"JoinLobby", new JoinLobbyModel(gameName))
                    .retry(RETRY_COUNT)
                    .blockingGet();
        }
        catch (Exception e){
            logError(e);
            return false;
        }
    }

    public void getGameSetupAndNotify(String gameName){
        Type returnType = new TypeReference<GameSetup>(){}.getType();
        Disposable subscription = hubConnection.<GameSetup>invoke(returnType,"GetGameSetup", gameName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(RETRY_COUNT)
                .subscribe(this::gotGameSetup, super::logError);
        disposables.add(subscription);
    }

    public void leaveLobbyBlocking(String gameName){
        try {
            hubConnection.invoke("LeaveLobby", new JoinLobbyModel(gameName))
                    .retry(RETRY_COUNT)
                    .blockingAwait();
        }
        catch (Exception e){
            logError(e);
        }
    }

    public void addBot(AddBotModel model){
        Disposable subscription = hubConnection.invoke("AddBot", model)
                .subscribeOn(Schedulers.io())
                .retry(RETRY_COUNT)
                .subscribe(super::noAction, super::logError);
        disposables.add(subscription);
    }

    public void removeBot(RemoveBotModel model) {
        Disposable subscription = hubConnection.invoke("RemoveBot", model)
                .subscribeOn(Schedulers.io())
                .retry(RETRY_COUNT)
                .subscribe(super::noAction, super::logError);
        disposables.add(subscription);
    }

    public void startGame(String gameName){
        Disposable subscription = hubConnection.invoke("StartGame", gameName)
                .subscribeOn(Schedulers.io())
                .retry(RETRY_COUNT)
                .subscribe(super::noAction, super::logError);
        disposables.add(subscription);
    }


    //---------Server Events--------------------------------------------

    @Override
    protected void setUpRpc() {
        super.setUpRpc();
        hubConnection.on("userJoined", this::userJoined, String.class);
        hubConnection.on("userLeft", this::userLeft, String.class);
        hubConnection.on("lobbyClosed", this::lobbyClosed);
        hubConnection.on("gameStarting", this::gameStarting);
        hubConnection.on("botAdded", this::botAdded, BotModel.class);
        hubConnection.on("botRemoved", this::botRemoved, String.class);
    }

    @Override
    protected void removeRpc() {
        super.removeRpc();
        hubConnection.remove("userJoined");
        hubConnection.remove("userLeft");
        hubConnection.remove("lobbyClosed");
        hubConnection.remove("gameStarting");
        hubConnection.remove("botAdded");
        hubConnection.remove("botRemoved");
    }

    // Event Handlers

    private void gotGameSetup(GameSetup setup){
        uiHandler.post(()-> {
            listener.onGotGameSetup(setup);
        });
    }

    private void userJoined(String username) {
        uiHandler.post(()-> {
            listener.onUserJoined(username);
        });
    }

    private void userLeft(String username) {
        uiHandler.post(()->{
            listener.onUserLeft(username);
        });
    }

    private void lobbyClosed(){
        uiHandler.post(()->{
            listener.onLobbyClosed();
        });
    }

    private void gameStarting(){
        uiHandler.post(()->{
            listener.onGameStarting();
        });
    }

    private void botAdded(BotModel model) {
        uiHandler.post(()->{
            listener.onBotAdded(model);
        });
    }

    private void botRemoved(String name) {
        uiHandler.post(()->{
            listener.onBotRemoved(name);
        });
    }
}
