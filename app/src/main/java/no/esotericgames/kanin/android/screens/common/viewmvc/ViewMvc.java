package no.esotericgames.kanin.android.screens.common.viewmvc;

import android.view.View;

public interface ViewMvc {
    View getRootView();
}
