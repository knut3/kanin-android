package no.esotericgames.kanin.android.screens.common.tarot;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.common.Constants;
import no.esotericgames.kanin.android.screens.common.BaseActivity;
import no.esotericgames.kanin.android.screens.common.ResourceManager;

public class TarotCardViewer extends BaseActivity implements View.OnClickListener {

    public static final String EXTRA_KEY_CARD_PATH = "cardPath";
    public static final String EXTRA_KEY_CARD_NAME = "cardName";

    private ImageView cardImageView;
    private Button searchButton;
    private ImageButton closeButton;
    private String cardName;
    private ResourceManager resourceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        resourceManager = getCompositionRoot().getResourceManager();
        setContentView(R.layout.activity_tarot_card_viewer);

        String cardPath = getIntent().getExtras().getString(EXTRA_KEY_CARD_PATH);
        cardName = getIntent().getExtras().getString(EXTRA_KEY_CARD_NAME);
        cardImageView = findViewById(R.id.tarotCardViewer_ivTheCard);
        searchButton = findViewById(R.id.tarotCardViewer_btnSearch);
        searchButton.setOnClickListener(this);
        closeButton = findViewById(R.id.tarotCardViewer_btnClose);
        closeButton.setOnClickListener(this);

        Picasso.get()
                .load(Constants.BASE_URL + "/" + cardPath)
                .placeholder(R.drawable.tarot_placeholder)
                .into(cardImageView);


    }

    @Override
    public void onClick(View view) {
        if (view == searchButton){
            String query = "tarot " + cardName;
            String url = "http://www.google.com/search?q=" + query;
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }
        else if (view == closeButton)
            onBackPressed();
    }
}