package no.esotericgames.kanin.android.screens.game.rounds.common;

import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.text.Html;

import androidx.annotation.StringRes;

import java.util.ArrayList;
import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.common.BaseObservable;
import no.esotericgames.kanin.android.models.CardPlayedModel;
import no.esotericgames.kanin.android.models.CardWithOwner;
import no.esotericgames.kanin.android.models.CurrentScore;
import no.esotericgames.kanin.android.models.GameState;
import no.esotericgames.kanin.android.screens.game.rounds.common.scorebubbles.ScoreBubblesView;
import no.esotericgames.kanin.android.screens.game.common.Vector;
import no.esotericgames.kanin.android.models.Card;
import no.esotericgames.kanin.android.models.Player;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;
import no.esotericgames.kanin.android.screens.common.helpers.ToastMaster;
import no.esotericgames.kanin.android.screens.common.ResourceManager;
import no.esotericgames.kanin.android.screens.game.common.ViewAnimator;
import no.esotericgames.kanin.android.screens.game.scoreboard.MyScoresView;
import no.esotericgames.kanin.android.screens.game.scoreboard.ScoreBoardsView;
import no.esotericgames.kanin.android.screens.game.scoreboard.ScoreViewAppearanceModel;
import no.esotericgames.kanin.android.screens.game.turnindicator.TurnIndicatorAnimator;
import no.esotericgames.kanin.android.screens.game.turnindicator.TurnIndicatorView;

public abstract class RoundView extends BaseObservable<RoundView.Listener> implements IDrawable {

    public interface Listener{
        void onMyCardThrown(Card card);
    }

    protected enum State { MY_TURN, OPPONENT_TURN, PICKING_TRUMP }
    protected State state;

    private static final int CARD_ANIMATION_DEFAULT_DURATION_MS = 300;

    protected final Point screenSize;

    // Views
    protected final TableView tableView;
    protected final MyCardsView myCardsView;
    protected final ScoreBoardsView scoreBoardsView;
    protected final MyScoresView myScoresView;
    private final TurnIndicatorView turnIndicatorView;
    protected final ScoreBubblesView scoreBubblesView;

    protected List<TableCard> tableCards;
    protected final Player me;
    private final Drawable overlayImage;
    private final Paint bgPaint;
    protected final ViewAnimator viewAnimator;
    private final TurnIndicatorAnimator turnIndicatorAnimator;
    protected final ResourceManager resourceManager;
    protected final ToastMaster toastMaster;
    protected final PlayingSurfaceModel playingSurfaceModel;
    protected final float scoreBubbleRadius;

    public RoundView(Point screenSize, GameState gameState,
                     Player me, ViewAnimator viewAnimator,
                     TurnIndicatorAnimator turnIndicatorAnimator,
                     ResourceManager resourceManager,
                     ToastMaster toastMaster) {
        this.screenSize = screenSize;
        this.viewAnimator = viewAnimator;
        this.turnIndicatorAnimator = turnIndicatorAnimator;
        this.resourceManager = resourceManager;
        this.toastMaster = toastMaster;
        this.me = me;
        this.tableCards = new ArrayList<>(gameState.opponents.length+1);

        myCardsView = new MyCardsView(gameState.yourCards, resourceManager);
        myCardsView.setSize(screenSize);
        tableView = new TableView(new PointF(screenSize.x, myCardsView.getCardsTop()));

        bgPaint = getBackgroundPaint();
        overlayImage = resourceManager.getDrawable(R.drawable.dirty_overlay);

        int playerNameSize = screenSize.y / 24;
        scoreBubbleRadius = screenSize.y / 12f;

        scoreBoardsView = createOpponentScoreBoards(gameState, playerNameSize);
        myScoresView = createMyScoresView(gameState, playerNameSize, gameState.yourCards.size());
        turnIndicatorView = createTurnIndicator();
        playingSurfaceModel = new PlayingSurfaceModel(scoreBoardsView.getCenterBoardBottom(), myScoresView.getTop(), tableView.getCenter());
        scoreBubblesView = new ScoreBubblesView(viewAnimator, resourceManager, myScoresView, scoreBoardsView, me);
        placeTableCards(gameState.cardsOnTable);
        setFirstToPlay(gameState.nextToPlay);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawRect(0, 0, screenSize.x, screenSize.y, bgPaint);
        tableView.draw(canvas);
        turnIndicatorView.draw(canvas);
        myScoresView.draw(canvas);
        drawOnTable(canvas);
        for (TableCard card : tableCards)
            card.getCardView().draw(canvas);
        myCardsView.draw(canvas);
        scoreBoardsView.draw(canvas);
        scoreBubblesView.draw(canvas);

        //overlayImage.setBounds(0, 0, screenSize.x, screenSize.y);
        //overlayImage.draw(canvas);
    }

    public boolean onTouchDownEvent(float x, float y){

        if (state == State.PICKING_TRUMP)
            return false;

        CardView cardView = myCardsView.getClicked(x, y);
        if (cardView == null) return false;

        if (state != State.MY_TURN) {
            toastMaster.showFailureToast(resourceManager.getString(R.string.toast_not_your_turn));
            return true;
        }

        if (!isValidCardToThrow(cardView.getCard())){
            String reasonMsg = getInvalidCardReason(cardView.getCard());
            toastMaster.showFailureToast(Html.fromHtml(reasonMsg, Html.FROM_HTML_MODE_COMPACT).toString());
            return true;
        }

        state = State.OPPONENT_TURN;
        throwMyCard(cardView);
        return true;
    }

    public void onCardPlayed(CardPlayedModel model) {
        int delay_ms = 500;
        if (model.card.owner.equals(me.getUsername())){
            setNextToPlay(model.nextToPlay, delay_ms);
        }
        else {
            throwOpponentCard(model.card, ()-> setNextToPlay(model.nextToPlay, delay_ms));
            onOpponentThrowingCard(model.card.owner);
        }
    }

    public void playerDisconnected(String username) {
        scoreBoardsView.getScoreBoardView(username)
                .showDisconnectedIcon();
    }

    public void playerConnected(String username) {
        scoreBoardsView.getScoreBoardView(username)
                .hideDisconnectedIcon();
    }


    protected abstract boolean useCardsLeftAsRoundScore();

    public void onOpponentThrowingCard(String owner){}

    protected abstract boolean isValidCardToThrow(Card card);

    protected abstract String getInvalidCardReason(Card card);

    protected abstract PointF getTableCardPosition(Card card, String owner, PlayingSurfaceModel playingSurface);

    protected abstract PointF getTableCardSize(PlayingSurfaceModel playingSurface);

    protected abstract boolean shouldUseDefaultTableCardRotation();

    protected abstract void cleanupAfterThrownCard(Card thrownCard);

    protected abstract void drawOnTable(Canvas canvas);

    public void throwOpponentCard(CardWithOwner cwo, Runnable actionOnEnd) {
        CardView cardView = new CardView(cwo.card, resourceManager.getCardDrawable(cwo.card));
        cardView.setSize(getTableCardSize(playingSurfaceModel));
        PointF scoreBoardCenter = scoreBoardsView.getScoreBoardCenter(cwo.owner);
        float cardAngle = Vector.getAngle(scoreBoardCenter, playingSurfaceModel.center) + 180;
        cardView.setRotation(cardAngle);
        PointF startPosition = new PointF(scoreBoardCenter.x, scoreBoardCenter.y + cardView.getSize().y/2);
        cardView.setPosition(startPosition);
        tableCards.add(new TableCard(cardView, cwo.owner));
        Runnable onEnd = ()->{
            cleanupAfterThrownCard(cwo.card);
            if (actionOnEnd != null)
                actionOnEnd.run();
        };

        if (shouldUseDefaultTableCardRotation()){
            viewAnimator.animate(cardView,
                    getTableCardPosition(cwo.card, cwo.owner, playingSurfaceModel),
                    0, onEnd);
        }
        else {
            viewAnimator.animate(cardView,
                    getTableCardPosition(cwo.card, cwo.owner, playingSurfaceModel),
                    onEnd);
        }
    }

    protected void setFirstToPlay(String username){
        turnIndicatorAnimator.initialize(turnIndicatorView, scoreBoardsView.getActiveSeatDirections(), me, username);
        if (!username.equals(me.getUsername())){
            state = State.OPPONENT_TURN;
            scoreBoardsView.markPlayerTurn(username);
        }
        else{
            state = State.MY_TURN;
        }

    }

    public void announceFirstToPlay(String nextToPlay, boolean hasRoundStarted) {

        String toast;

        if (hasRoundStarted){
            toast = nextToPlay.equals(me.getUsername())
                    ? resourceManager.getString(R.string.toast_your_turn)
                    : resourceManager.getString(R.string.toast_someones_turn, nextToPlay);
        }
        else {
            toast = nextToPlay.equals(me.getUsername())
                    ? resourceManager.getString(R.string.toast_you_begin)
                    : resourceManager.getString(R.string.toast_who_goes_first, nextToPlay);

        }
        toastMaster.showSuccessToast(toast);
    }

    public void setNextToPlay(String username, int delay_ms){
        Runnable actionOnEnd = null;
        if (username.equals(me.getUsername())){
            state = State.MY_TURN;
            actionOnEnd = scoreBoardsView::clearMarkedScoreBoards;
        }
        else{
            actionOnEnd = ()-> scoreBoardsView.markPlayerTurn(username);
        }
        turnIndicatorAnimator.moveTo(username, delay_ms, actionOnEnd);
    }

    public void clearNextToPlay() {
        scoreBoardsView.clearMarkedScoreBoards();
        turnIndicatorView.hide();
    }

    protected void addScore(int score, String owner){
        if (owner.equals(me.getUsername())){
            myScoresView.addScore(score);
        }
        else {
            scoreBoardsView.addScore(score, owner);
        }
    }

    public void addRoundScore(int score, String owner){
        if (owner.equals(me.getUsername())){
            myScoresView.addRoundScore(score);
        }
        else {
            scoreBoardsView.getScoreBoardView(owner)
                    .addRoundScore(score);
        }
    }

    private void addTotalScore(int score, String owner){
        if (owner.equals(me.getUsername())){
            myScoresView.addTotalScore(score);
        }
        else {
            scoreBoardsView.getScoreBoardView(owner)
                    .addTotalScore(score);
        }
    }

    private void placeTableCards(List<CardWithOwner> cardsOnTable){
        for (CardWithOwner cwo : cardsOnTable){
            float rotation = 0;
            if (!cwo.owner.equals(me.getUsername()) && !shouldUseDefaultTableCardRotation())
                rotation = Vector.getAngle(scoreBoardsView.getScoreBoardCenter(cwo.owner),
                        playingSurfaceModel.center) + 180;

            Drawable cardDrawable = resourceManager.getCardDrawable(cwo.card);
            CardView cardView = new CardView(cwo.card, cardDrawable);
            cardView.setSize(getTableCardSize(playingSurfaceModel));
            cardView.setRotation(rotation);
            cardView.setPosition(getTableCardPosition(cwo.card, cwo.owner ,playingSurfaceModel));
            tableCards.add(new TableCard(cardView, cwo.owner));
        }
    }

    protected void throwMyCard(CardView cardView){
        PointF tablePosition = getTableCardPosition(cardView.getCard(), me.getUsername(), playingSurfaceModel);
        viewAnimator.animate(cardView, tablePosition, getTableCardSize(playingSurfaceModel), 0, CARD_ANIMATION_DEFAULT_DURATION_MS, ()-> {
            TableCard myTableCard = new TableCard(cardView, me.getUsername());
            tableCards.add(myTableCard);
            myCardsView.remove(cardView);
            cleanupAfterThrownCard(cardView.getCard());
            for (Listener l : getListeners())
                l.onMyCardThrown(cardView.getCard());
        });
    }

    private ScoreBoardsView createOpponentScoreBoards(GameState gameState, int playerNameSize){
        ScoreViewAppearanceModel roundScoreAppearance =
                ScoreViewAppearanceModel.getRoundScoreAppearanceModel(gameState.round.name, resourceManager);
        ScoreViewAppearanceModel totalScoreAppearance =
                ScoreViewAppearanceModel.getTotalScoreAppearanceModel(resourceManager);

        Drawable disconnectedIcon = resourceManager.getDrawable(R.drawable.disconnected);

        return new ScoreBoardsView(gameState.opponents, gameState.scores,
                useCardsLeftAsRoundScore(), playerNameSize,
                roundScoreAppearance, totalScoreAppearance, tableView, disconnectedIcon);
    }

    private MyScoresView createMyScoresView(GameState gameState, int playerNameSize, int cardsLeftOnHand){
        ScoreViewAppearanceModel roundScoreAppearance =
                ScoreViewAppearanceModel.getRoundScoreAppearanceModel(gameState.round.name, resourceManager);
        ScoreViewAppearanceModel totalScoreAppearance =
                ScoreViewAppearanceModel.getTotalScoreAppearanceModel(resourceManager);

        CurrentScore myScore = gameState.scores.stream().filter(x-> x.username.equals(me.getUsername())).findFirst().get();
        int visualRoundScore = useCardsLeftAsRoundScore() ? cardsLeftOnHand : myScore.roundScore;
        PointF myScoresCenterPos = tableView.getMyScoresCenter(playerNameSize);
        PointF myScoreBoardSize = new PointF(playerNameSize*3, playerNameSize);
        return new MyScoresView(myScoresCenterPos, myScoreBoardSize,
                visualRoundScore, myScore.totalScore,
                roundScoreAppearance, totalScoreAppearance);
    }

    private TurnIndicatorView createTurnIndicator(){
        TurnIndicatorView view = new TurnIndicatorView(
                resourceManager.getDrawable(R.drawable.ic_arrow_circle_down),
                tableView.getRect(), tableView.getCurveRadius());
        float turnIndicatorDiameter = tableView.getStrokeWidth() * 0.9f;
        view.setSize(new PointF(turnIndicatorDiameter, turnIndicatorDiameter));
        return view;
    }

    private Paint getBackgroundPaint(){
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        int topColor = resourceManager.getColor(R.color.game_bg_gradient_top);
        int bottomColor = resourceManager.getColor(R.color.game_bg_gradient_bottom);
        paint.setShader(new LinearGradient(0, 0, 0, screenSize.y,
                topColor, bottomColor, Shader.TileMode.CLAMP));
        return paint;
    }
}
