package no.esotericgames.kanin.android.models;

public class TarotCard {
    public int id;
    public String relativePath;
    public String arcana;
    public String name;
    public String suit;
    public int value;
}
