package no.esotericgames.kanin.android.screens.game;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.CardWithOwner;
import no.esotericgames.kanin.android.models.CurrentScore;
import no.esotericgames.kanin.android.models.GameState;
import no.esotericgames.kanin.android.models.KabalPassModel;
import no.esotericgames.kanin.android.models.Round;
import no.esotericgames.kanin.android.models.Card;
import no.esotericgames.kanin.android.models.Player;
import no.esotericgames.kanin.android.screens.common.ResourceManager;
import no.esotericgames.kanin.android.screens.game.common.InfoDrawerContentUpdater;

public class GameTestActivity extends AppCompatActivity implements GameView.Listener {

    private GameView gameView;
    GameState gameState;
    Player me;
    private DrawerLayout drawerLayout;
    private ScrollView infoDrawer;
    private ImageButton btnRoundInfo;
    private InfoDrawerContentUpdater infoDrawerContentUpdater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<Card> cards = new ArrayList<>();

        for (int i = 2; i <= 5; i++){
            cards.add(new Card("clubs", i));
        }

        for (int i = 5; i <= 8; i++){
            cards.add(new Card("diamonds", i));
        }

        for (int i = 8; i <= 11; i++){
            cards.add(new Card("spades", i));
        }

        for (int i = 10; i <= 14; i++){
            cards.add(new Card("hearts", i));
        }


        gameState = new GameState();
        gameState.round = new Round();
        gameState.round.name = "Kabal";
        gameState.round.hasStarted = true;
        //gameState.trumpSuit = Card.Suits.DIAMONDS;
        gameState.yourCards = cards;
        gameState.opponents = new Player[]{
                new Player("Jon", cards.size()),
                new Player("Kari", cards.size()),
                new Player("Martin", cards.size()),
                new Player("Hans Olav", cards.size())
        };
        gameState.cardsOnTable = new ArrayList<>();
        gameState.cardsOnTable.add(new CardWithOwner(new Card(Card.Suits.CLUBS, 7), "Kari"));
        List<CurrentScore> scores = new ArrayList<>();
        for (int i = 0; i < gameState.opponents.length; i++){
            CurrentScore score = new CurrentScore();
            score.username = gameState.opponents[i].getUsername();
            score.totalScore = i + 9;
            score.roundScore = (int)(i*2.5f);
            scores.add(score);
        }

        CurrentScore myScore = new CurrentScore();
        myScore.username = "Knut";
        myScore.roundScore = 2;
        myScore.totalScore = 77;
        scores.add(myScore);

        gameState.scores = scores;
        gameState.nextToPlay = "Knut";
        boolean isNewRound = true;
        me = new Player("Knut", cards.size());
        gameView = new GameView(this, isNewRound, me, this);

        setContentView(R.layout.activity_game);
        drawerLayout = findViewById(R.id.drawerLayout);
        FrameLayout contentLayout = drawerLayout.findViewById(R.id.contentFrameLayout);
        contentLayout.addView(gameView, 0);
        infoDrawer = findViewById(R.id.infoDrawer);
        btnRoundInfo = contentLayout.findViewById(R.id.btn_round_info);
        btnRoundInfo.setVisibility(View.INVISIBLE);
        gameView.setGameState(gameState);
        infoDrawerContentUpdater
                = new InfoDrawerContentUpdater(infoDrawer, new ResourceManager(this));
        infoDrawerContentUpdater.update(gameState);
    }

    public void onInfoIconClicked(View view){
        drawerLayout.openDrawer(infoDrawer);
    }

    @Override
    public void onPreRoundViewEnded() {
        btnRoundInfo.setVisibility(View.VISIBLE);
    }

    @Override
    public void onMyCardThrown(Card card) {

    }

    @Override
    public void onRoundFinished() {
        gameState.round.number = 2;
        gameState.round.name = "Kabal";
        gameView.setGameState(gameState);
    }

    @Override
    public void onValidKabalPassClick() {
        KabalPassModel model = new KabalPassModel();
        model.passer = me.getUsername();
        model.nextToPlay = "Kari";
        gameView.kabalPass(model);
    }

    @Override
    public void onTrumpSuitDrawn(int index) {
        String trump = Card.Suits.HEARTS;
        infoDrawerContentUpdater.setTrumpSuit(trump);
        gameView.trumpSuitSelected(trump, index);
    }

    @Override
    public void onGameFinished(int gameId) {

    }

    @Override
    public void showAnnouncement(String text) {

    }

    @Override
    public void hideAnnouncement() {

    }
}