package no.esotericgames.kanin.android.models;

public class AddBotModel {
    public static final String DUMB = "dumb";
    public static final String MEDIUM = "medium";

    public String gameName;
    public String intelligence;

    public AddBotModel(String gameName, String intelligence) {
        this.gameName = gameName;
        this.intelligence = intelligence;
    }
}
