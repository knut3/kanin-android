package no.esotericgames.kanin.android.models;

public class AveragePlacementModel {
    public String username;
    public float averagePlacement;
    public int ranking;
    public boolean inBarMode = true;
}
