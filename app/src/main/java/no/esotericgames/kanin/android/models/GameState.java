package no.esotericgames.kanin.android.models;

import java.util.List;

public class GameState {
    public Round round;
    public Player[] opponents;
    public List<Card> yourCards;
    public String nextToPlay;
    public List<CardWithOwner> cardsOnTable;
    public List<Card> previousTrick;
    public String trumpSuit;
    public String playerToPerformTrumpDraw;
    public List<CurrentScore> scores;
}
