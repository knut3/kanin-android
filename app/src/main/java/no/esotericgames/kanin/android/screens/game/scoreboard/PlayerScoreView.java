package no.esotericgames.kanin.android.screens.game.scoreboard;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;

public class PlayerScoreView implements IDrawable {

    private static final float CONTENT_PERCENTAGE_OF_HEIGHT = 0.7f;

    private int score;
    private Drawable icon;
    private float width, height;
    private RectF backgroundRect;
    private int backgroundRoundingFactor;
    private Rect iconRect;
    private float scoreOffset_x, contentPadding;
    private final int textSize;
    private final float textExactVerticalCenter;
    private Paint backgroundPaint, strokePaint, textPaint;
    private final boolean removeMinusSign;

    public PlayerScoreView(float width, float height, ScoreViewAppearanceModel appearanceModel, int score) {
        this.score = score;
        this.icon = appearanceModel.icon;
        this.width = width;
        this.height = height;
        this.removeMinusSign = appearanceModel.removeMinusSign;
        textSize = (int)(height * CONTENT_PERCENTAGE_OF_HEIGHT);
        contentPadding = (height - textSize) / 2;
        setupPaint(textSize, appearanceModel);
        setScoreOffset_x();
        backgroundRoundingFactor = (int)width / 7;
        Rect textBounds = new Rect();
        textPaint.getTextBounds("0",0, 1, textBounds);
        textExactVerticalCenter = textBounds.exactCenterY();
    }

    public void setPosition(float x, float y){
        backgroundRect = new RectF(x, y, x + width, y + height);
        int icon_x = (int)(x + contentPadding);
        int icon_y = (int)(y + contentPadding);
        iconRect = new Rect(icon_x, icon_y, icon_x+textSize, icon_y+textSize);
    }

    public float getIconCenterOffsetFromLeft(){
        return contentPadding + iconRect.width()/2f;
    }

    public float getIconCenterOffsetFromRight() {
        return -backgroundRect.width() + contentPadding + iconRect.width()/2f;
    }

    public float getCenter_y(){
        return backgroundRect.centerY();
    }

    private void setupPaint(int textSize, ScoreViewAppearanceModel appearanceModel){
        backgroundPaint = new Paint();
        backgroundPaint.setColor(appearanceModel.bgColor);
        backgroundPaint.setStyle(Paint.Style.FILL);
        backgroundPaint.setAntiAlias(true);
        textPaint = new Paint();
        textPaint.setColor(appearanceModel.scoreColor);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(textSize);
        textPaint.setAntiAlias(true);
        strokePaint = new Paint();
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setAntiAlias(true);
        strokePaint.setColor(appearanceModel.scoreColor);
        strokePaint.setStrokeWidth(height/11);
    }

    private void setScoreOffset_x(){
        float scoreSpace_x = width - textSize; // textsize is here the width of the icon
        scoreOffset_x = scoreSpace_x / 2;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawRoundRect(backgroundRect, backgroundRoundingFactor, backgroundRoundingFactor, backgroundPaint);
        canvas.drawRoundRect(backgroundRect, backgroundRoundingFactor, backgroundRoundingFactor, strokePaint);
        float score_x = backgroundRect.left + textSize + scoreOffset_x;
        float score_y = backgroundRect.centerY() - textExactVerticalCenter;
        int shownScore = removeMinusSign ? Math.abs(score) : score;
        canvas.drawText(""+shownScore, score_x, score_y, textPaint);
        icon.setBounds(iconRect);
        icon.draw(canvas);

    }

    public void addScore(int score) {
        this.score += score;
    }

    public int getScore() {
        return score;
    }


}
