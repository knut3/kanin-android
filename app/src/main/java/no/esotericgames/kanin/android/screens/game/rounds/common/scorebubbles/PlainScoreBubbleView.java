package no.esotericgames.kanin.android.screens.game.rounds.common.scorebubbles;

import android.graphics.Canvas;
import android.graphics.PointF;

import no.esotericgames.kanin.android.screens.common.ResourceManager;

public class PlainScoreBubbleView extends ScoreBubbleView {
    public PlainScoreBubbleView(int score, ResourceManager resourceManager, boolean drawStroke) {
        super(score, resourceManager, drawStroke);
    }

    @Override
    protected void setContentAlpha(int alpha) {

    }

    @Override
    protected void setContentSize(PointF bubbleSize) {

    }

    @Override
    protected void setContentPosition(PointF bubbleCenter) {

    }

    @Override
    protected void drawContent(Canvas canvas) {

    }
}
