package no.esotericgames.kanin.android.screens.game;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.view.WindowManager;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;

import java.util.Timer;
import java.util.TimerTask;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.CardPlayedAndTrickEndedModel;
import no.esotericgames.kanin.android.models.CardPlayedModel;
import no.esotericgames.kanin.android.models.GameEndedAfterKabal;
import no.esotericgames.kanin.android.models.GameEndedAfterTrickRound;
import no.esotericgames.kanin.android.models.GameState;
import no.esotericgames.kanin.android.models.KabalPassModel;
import no.esotericgames.kanin.android.models.KabalRoundEnded;
import no.esotericgames.kanin.android.models.TrickRoundEnded;
import no.esotericgames.kanin.android.models.TrumpSuitSelectedModel;
import no.esotericgames.kanin.android.common.SharedPreferenceKeys;
import no.esotericgames.kanin.android.models.Card;
import no.esotericgames.kanin.android.models.Player;
import no.esotericgames.kanin.android.networking.NetworkStateService;
import no.esotericgames.kanin.android.networking.duplex.GameRepository;
import no.esotericgames.kanin.android.screens.common.BaseActivity;
import no.esotericgames.kanin.android.screens.common.ResourceManager;
import no.esotericgames.kanin.android.screens.common.dialogs.DialogsEventBus;
import no.esotericgames.kanin.android.screens.common.dialogs.DialogsManager;
import no.esotericgames.kanin.android.screens.common.dialogs.promptdialog.PromptDialogEvent;
import no.esotericgames.kanin.android.screens.common.helpers.ScreensNavigator;
import no.esotericgames.kanin.android.screens.common.helpers.ToastMaster;
import no.esotericgames.kanin.android.screens.game.common.InfoButtonAttentionizer;
import no.esotericgames.kanin.android.screens.game.common.InfoDrawerContentUpdater;

public class GameActivity extends BaseActivity
        implements GameView.Listener, GameRepository.Listener, DialogsEventBus.Listener {

    private static final String TAG = "GameActivity";
    public static final String EXTRA_KEY_GAME_NAME = "gameName";
    public static final String EXTRA_KEY_SHOW_ROUND_INTRO = "showRoundIntro";
    private static final int IDLE_TIME_UNTIL_INFO_BTN_ATTENTION = 10000;
    private DrawerLayout drawerLayout;
    private ScrollView infoDrawer;
    private ImageButton btnRoundInfo;
    private InfoButtonAttentionizer infoBtnAttentionizer;
    private InfoDrawerContentUpdater infoDrawerContentUpdater;
    private DialogsManager dialogsManager;
    private DialogsEventBus dialogsEventBus;
    private ScreensNavigator screensNavigator;
    private ToastMaster toastMaster;
    private GameView gameView;
    private GameRepository gameRepository;
    private String gameName;
    private String myUsername;
    private Timer myTurnTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialogsManager = getCompositionRoot().getDialogsManager();
        dialogsEventBus = getCompositionRoot().getDialogsEventBus();
        screensNavigator = getCompositionRoot().getScreensNavigator();
        toastMaster = getCompositionRoot().getToastMaster();
        myUsername = getCompositionRoot().getSharedPreferences()
                .getString(SharedPreferenceKeys.USERNAME, "");

        boolean isNewRound;
        if (savedInstanceState == null){
            gameName = getIntent().getExtras().getString(EXTRA_KEY_GAME_NAME);
            isNewRound = getIntent().getExtras().getBoolean(EXTRA_KEY_SHOW_ROUND_INTRO);
        }
        else{
            gameName = savedInstanceState.getString(EXTRA_KEY_GAME_NAME);
            isNewRound = false;
        }

        gameRepository = getCompositionRoot().getGameRepository(gameName);
        gameView = new GameView(this, isNewRound, new Player(myUsername), this);
        setContentView(R.layout.activity_game);
        drawerLayout = findViewById(R.id.drawerLayout);
        FrameLayout contentLayout = drawerLayout.findViewById(R.id.contentFrameLayout);
        contentLayout.addView(gameView, 0);
        infoDrawer = findViewById(R.id.infoDrawer);
        btnRoundInfo = contentLayout.findViewById(R.id.btn_round_info);
        btnRoundInfo.setVisibility(View.INVISIBLE);
        infoBtnAttentionizer = new InfoButtonAttentionizer(btnRoundInfo, getColor(R.color.info_button_attention));
        infoDrawerContentUpdater
                = new InfoDrawerContentUpdater(infoDrawer, new ResourceManager(this));
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(EXTRA_KEY_GAME_NAME, gameName);
    }

    @Override
    protected void onStart() {
        super.onStart();
        gameRepository.bind(this);
        dialogsEventBus.registerListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        try{
            gameRepository.getGameStateAndNotify();
        }
        catch (Exception ignored){

        }
    }

    @Override
    protected void onStop() {
        gameRepository.unbind();
        dialogsEventBus.unregisterListener(this);
        super.onStop();
    }

    @Override
    public void onConnectionAvailable() {
        gameRepository.tryReconnect();
    }

    @Override
    public void onBackPressed() {
        dialogsManager.showGotoMainMenuDialog();
    }

    public void onInfoIconClicked(View view){
        drawerLayout.openDrawer(infoDrawer);
    }

    //-----------------View Events--------------------------------------------

    @Override
    public void onPreRoundViewEnded() {
        btnRoundInfo.setVisibility(View.VISIBLE);
        startInfoButtonAttentionTimerIfMyTurn(gameView.getNextToPlay());
    }

    @Override
    public void onMyCardThrown(Card card){
        if (!NetworkStateService.isNetworkConnected) {
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
            return;
        }

        try {
            gameRepository.playCard(card);
            if (myTurnTimer != null){
                myTurnTimer.cancel();
                myTurnTimer = null;
            }
        }
        catch (Exception e) {
            showDisconnectedOverlay();
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
        }
    }

    @Override
    public void onRoundFinished() {
        try {
            btnRoundInfo.setVisibility(View.INVISIBLE);
            gameRepository.getGameStateAndNotify();
        }
        catch (Exception e) {
            showDisconnectedOverlay();
        }
    }

    @Override
    public void onValidKabalPassClick() {
        if (!NetworkStateService.isNetworkConnected) {
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
            return;
        }

        try{
        gameRepository.kabalPass();
        }
        catch (Exception e) {
            showDisconnectedOverlay();
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
        }
    }

    @Override
    public void onTrumpSuitDrawn(int pickedIndex) {
        if (!NetworkStateService.isNetworkConnected){
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
            return;
        }

        try{
        gameRepository.performTrumpSuitDraw(pickedIndex);
        }
        catch (Exception e) {
            showDisconnectedOverlay();
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
        }
    }

    @Override
    public void onGameFinished(int gameId) {
        screensNavigator.toMedalCeremony(gameId);
    }

    @Override
    public void showAnnouncement(String text) {
        super.showAnnouncementOverlay(text);
    }

    @Override
    public void hideAnnouncement() {
        super.hideAnnouncementOverlay();
    }

    //-----------------Server Events------------------------------------------

    @Override
    public void onConnectedToServer() {
        try {
            gameRepository.getGameStateAndNotify();
            hideDisconnectedOverlay();
        }
        catch (Exception e) {
            showDisconnectedOverlay();
        }
    }

    @Override
    public void onServerMaintenanceInProgress(int duration_min) {
        super.showAnnouncementOverlay(getString(R.string.announcement_server_maintenance, duration_min));
    }

    @Override
    public void onMessageFromAdmin(String message) {
        super.showAnnouncementOverlay(message);
    }

    @Override
    public void onGotGameState(GameState gameState){
        gameView.setGameState(gameState);
        infoDrawerContentUpdater.update(gameState);
    }


    @Override
    public void onCardPlayed(CardPlayedModel model){
        Log.d(TAG, "onCardPlayed: " + model.card.owner);
        gameView.cardPlayed(model);
        startInfoButtonAttentionTimerIfMyTurn(model.nextToPlay);
    }

    @Override
    public void onKabalPass(KabalPassModel model){
        //Toast.makeText(this, model.passer + " måtte passe", Toast.LENGTH_SHORT).show();
        gameView.kabalPass(model);
        startInfoButtonAttentionTimerIfMyTurn(model.nextToPlay);
    }

    @Override
    public void onCardPlayedAndTrickEnded(CardPlayedAndTrickEndedModel model){
        gameView.cardPlayedAndTrickEnded(model);
        startInfoButtonAttentionTimerIfMyTurn(model.trickResult.winner);
    }

    @Override
    public void onTrickRoundEnded(TrickRoundEnded model){
       gameView.trickRoundEnded(model, null);
    }

    @Override
    public void onKabalRoundEnded(KabalRoundEnded model){
        gameView.kabalRoundEnded(model, null);
    }

    @Override
    public void onTrumpSuitSelected(TrumpSuitSelectedModel model) {
        infoDrawerContentUpdater.setTrumpSuit(model.suit);
        gameView.trumpSuitSelected(model.suit, model.pickedIndex);
    }

    @Override
    public void onPlayerConnected(String username) {
        gameView.playerConnected(username);
    }

    @Override
    public void onPlayerDisconnected(String username) {
        Log.d(TAG, "playerDisconnected: " + username);
        gameView.playerDisconnected(username);
    }

    @Override
    public void onGameEndedAfterTrickRound(GameEndedAfterTrickRound model) {
        gameView.gameEndedAfterTrickRound(model);
    }

    @Override
    public void onGameEndedAfterKabal(GameEndedAfterKabal model) {
        gameView.gameEndedAfterKabal(model);
    }

    //----------Dialog Events----------------------------

    @Override
    public void onDialogEvent(Object event) {
        if (event instanceof PromptDialogEvent){
            PromptDialogEvent promptEvent = (PromptDialogEvent) event;
            switch (dialogsManager.getShownDialogTag()){
                case DialogsManager.TAG_GOTO_MAIN_MENU_PROMPT:
                    handleGotoMainMenuDialogEvent(promptEvent);
                    break;
            }
        }
    }

    //----------Private Methods---------------------------


    private void handleGotoMainMenuDialogEvent(PromptDialogEvent event) {
        switch (event.getClickedButton()){
            case POSITIVE:
                screensNavigator.toMainMenu();
                break;
            case NEGATIVE:
        }
    }

    private void startInfoButtonAttentionTimerIfMyTurn(String nextToPlay){
        if (!nextToPlay.equals(myUsername))
            return;

        if (myTurnTimer != null)
            myTurnTimer.cancel();

        myTurnTimer = new Timer();
        myTurnTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(()->
                    infoBtnAttentionizer.drawAttention()
                );
            }
        }, IDLE_TIME_UNTIL_INFO_BTN_ATTENTION,
                IDLE_TIME_UNTIL_INFO_BTN_ATTENTION +
                        InfoButtonAttentionizer.PULSE_DURATION_MS*InfoButtonAttentionizer.REPEAT_COUNT);
    }

}