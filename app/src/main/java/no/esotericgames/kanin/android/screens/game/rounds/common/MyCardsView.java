package no.esotericgames.kanin.android.screens.game.rounds.common;

import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.PointF;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import no.esotericgames.kanin.android.models.Card;
import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;
import no.esotericgames.kanin.android.screens.common.ResourceManager;

public class MyCardsView implements IDrawable {
    public static final float CARD_WIDTH_HEIGHT_RATIO = 1.25f;
    private final ResourceManager resourceManager;
    private final List<CardView> cardViews;
    private float cardsTop;

    public MyCardsView(List<Card> myCards, ResourceManager resourceManager) {
        this.resourceManager = resourceManager;
        this.cardViews = new ArrayList<>(myCards.size());
        for (Card card : myCards){
            cardViews.add(new CardView(card, resourceManager.getCardDrawable(card)));
        }
    }

    public void setSize(Point screenSize) {
        PointF cardSize = getCardSize(screenSize.y);
        float cardOverlap = getCardOverlap(cardSize.x, cardViews.size(), screenSize.x);
        float cardOffsetX = cardSize.x - cardOverlap;
        float cardPositionY = screenSize.y - cardSize.y;
        setMyCardViewsSizeAndPositions(cardOffsetX, cardPositionY, cardSize);
        cardsTop = screenSize.y - cardSize.y;
    }

    public float getCardsTop(){
        return cardsTop;
    }

    @Override
    public void draw(Canvas canvas) {
        for (CardView cardView : cardViews)
            cardView.draw(canvas);
    }

    public CardView getClicked(float x, float y){
        List<CardView> cardViewsReversed = new ArrayList<>(cardViews);
        Collections.reverse(cardViewsReversed);
        for (CardView card : cardViewsReversed){
            if (card.isClicked(x, y))
                return card;
        }

        return null;
    }

    public boolean containSuit(String suit) {
        return cardViews.stream().anyMatch(card -> card.getCard().suit.equals(suit));
    }

    public void remove(CardView cardView) {
        cardViews.remove(cardView);
    }

    public List<Card> getCards(){
        return cardViews.stream().map(view-> view.getCard()).collect(Collectors.toList());
    }

    private void setMyCardViewsSizeAndPositions(float cardOffsetX, float cardPositionY, PointF cardSize){
        for (int i = 0; i < cardViews.size(); i++){
            CardView card = cardViews.get(i);
            float posX = i * cardOffsetX;
            card.setPosition(new PointF(posX, cardPositionY));
            card.setSize(cardSize);
        }
    }

    private PointF getCardSize(int screenHeight){
        float height = screenHeight / 5f;
        float width = height / CARD_WIDTH_HEIGHT_RATIO;
        return new PointF(width, height);
    }

    private float getCardOverlap(float cardWidth, int numCards, int screenWidth){
        float currentCardsWidth = cardWidth * numCards;

        if (currentCardsWidth <= screenWidth){
            return 0;
        }

        float difference = currentCardsWidth - screenWidth;

        return  difference / numCards;
    }

}
