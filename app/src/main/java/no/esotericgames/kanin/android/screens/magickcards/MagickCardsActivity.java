package no.esotericgames.kanin.android.screens.magickcards;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;

import java.util.List;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.common.SharedPreferenceKeys;
import no.esotericgames.kanin.android.models.EarnedTarotCardStatistic;
import no.esotericgames.kanin.android.models.TarotCard;
import no.esotericgames.kanin.android.screens.common.BaseActivity;
import no.esotericgames.kanin.android.screens.common.tarot.TarotCardViewer;
import no.esotericgames.kanin.android.screens.common.tarot.TarotWiz;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MagickCardsActivity extends BaseActivity implements MagickCardsAdapter.Listener {
    private MagickCardsAdapter adapter;
    private DrawerLayout drawerLayout;
    private ScrollView infoDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_magick_cards);

        drawerLayout = findViewById(R.id.drawerLayout);
        infoDrawer = findViewById(R.id.infoDrawer);

        RecyclerView recyclerView = findViewById(R.id.recyclerMagickCards);
        adapter = new MagickCardsAdapter(this, this);
        recyclerView.setAdapter(adapter);

        String accessToken = getCompositionRoot().getSharedPreferences()
                .getString(SharedPreferenceKeys.ACCESS_TOKEN, "");
        getCompositionRoot().getStatisticsApi().getEarnedMagickCards(accessToken).enqueue(new Callback<List<EarnedTarotCardStatistic>>() {
            @Override
            public void onResponse(Call<List<EarnedTarotCardStatistic>> call, Response<List<EarnedTarotCardStatistic>> response) {
                if (!response.isSuccessful()){
                    showDisconnectedOverlay();
                    return;
                }

                if (response.body().isEmpty()){
                    findViewById(R.id.txtMagickCardsEmpty).setVisibility(View.VISIBLE);
                }
                else{
                    findViewById(R.id.btn_tarot_info).setVisibility(View.VISIBLE);
                    adapter.bindCards(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<EarnedTarotCardStatistic>> call, Throwable t) {
                showDisconnectedOverlay();
            }
        });
    }

    @Override
    public void onCardClicked(TarotCard card) {
        Intent intent = new Intent(this, TarotCardViewer.class);
        intent.putExtra(TarotCardViewer.EXTRA_KEY_CARD_PATH, card.relativePath);
        intent.putExtra(TarotCardViewer.EXTRA_KEY_CARD_NAME, TarotWiz.getName(card));
        startActivity(intent);
    }

    public void onInfoIconClicked(View view){
        drawerLayout.openDrawer(infoDrawer);
    }
}