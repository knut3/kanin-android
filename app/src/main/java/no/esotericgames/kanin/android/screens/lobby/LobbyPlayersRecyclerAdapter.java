package no.esotericgames.kanin.android.screens.lobby;

import android.util.Log;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import no.esotericgames.kanin.android.models.LobbyPlayer;
import no.esotericgames.kanin.android.screens.common.viewmvc.ViewMvcFactory;

public class LobbyPlayersRecyclerAdapter extends RecyclerView.Adapter<LobbyPlayersRecyclerAdapter.MyViewHolder>
    implements LobbyPlayerItemViewMvc.Listener {

    public interface Listener{

        void onBotClicked(String name);
    }
    static class MyViewHolder extends RecyclerView.ViewHolder{

        private final LobbyPlayerItemViewMvc viewMvc;

        public MyViewHolder(@NonNull LobbyPlayerItemViewMvc itemView) {
            super(itemView.getRootView());
            viewMvc = itemView;
        }



    }
    private static final String TAG = "LobbyPlayersRecyclerAdapter";
    private final List<LobbyPlayer> players;

    private final ViewMvcFactory viewMvcFactory;
    private final Listener listener;
    private boolean iAmCreator = false;

    public LobbyPlayersRecyclerAdapter(ViewMvcFactory viewMvcFactory, Listener listener) {
        this.viewMvcFactory = viewMvcFactory;
        players = new ArrayList<>();
        this.listener = listener;
    }

    public void iAmCreator() {
        this.iAmCreator = true;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LobbyPlayerItemViewMvc viewMvc = viewMvcFactory.getLobbyPlayerItemViewMvc(parent, iAmCreator);
        viewMvc.registerListener(this);
        return new MyViewHolder(viewMvc);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final LobbyPlayer item = players.get(position);
        holder.viewMvc.bindPlayer(item);
    }

    public void bindPlayers(List<LobbyPlayer> players){
        Log.d(TAG, "swapItems: ");
        this.players.clear();
        this.players.addAll(players);
        this.notifyDataSetChanged();
    }

    public void addItem(LobbyPlayer player){
        Log.d(TAG, "addItem: " + player.name);
        players.add(player);
        this.notifyItemInserted(players.size()-1);
    }

    public void removeItem(String name){
        Optional<LobbyPlayer> player = players.stream()
                .filter(x -> x.name.equals(name))
                .findFirst();

        if (!player.isPresent()) return;

        int index = players.indexOf(player.get());
        players.remove(index);
        this.notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return players.size();
    }

    @Override
    public void onBotClicked(String name) {
        listener.onBotClicked(name);
    }


}
