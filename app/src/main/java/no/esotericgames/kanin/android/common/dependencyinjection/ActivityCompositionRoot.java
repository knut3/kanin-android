package no.esotericgames.kanin.android.common.dependencyinjection;

import androidx.fragment.app.FragmentActivity;

import no.esotericgames.kanin.android.networking.halfduplex.ScoreAPI;
import no.esotericgames.kanin.android.networking.halfduplex.StatisticsAPI;
import no.esotericgames.kanin.android.networking.halfduplex.UserAPI;
import no.esotericgames.kanin.android.screens.common.dialogs.DialogsEventBus;

public class ActivityCompositionRoot {

    private final CompositionRoot mCompositionRoot;
    private final FragmentActivity mActivity;


    public ActivityCompositionRoot(CompositionRoot compositionRoot, FragmentActivity activity) {
        mCompositionRoot = compositionRoot;
        mActivity = activity;
    }

    public FragmentActivity getActivity() {
        return mActivity;
    }

    public UserAPI getUserApi() {
        return mCompositionRoot.getUserApi();
    }

    public ScoreAPI getScoreApi() {
        return mCompositionRoot.getScoreApi();
    }

    public StatisticsAPI getStatisticsApi() {
        return mCompositionRoot.getStatisticsApi();
    }

    public DialogsEventBus getDialogsEventBus() {
        return mCompositionRoot.getDialogsEventBus();
    }

}
