package no.esotericgames.kanin.android.screens.lobby;

import android.os.Bundle;

import no.esotericgames.kanin.android.R;
import no.esotericgames.kanin.android.models.AddBotModel;
import no.esotericgames.kanin.android.models.RemoveBotModel;
import no.esotericgames.kanin.android.networking.NetworkStateService;

public class LobbyManagerActivity extends LobbyActivity implements LobbyViewMvc.Listener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        viewMvc.iAmCreator();
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewMvc.registerListener(this);
    }

    @Override
    protected void onStop() {
        viewMvc.unregisterListener(this);
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (NetworkStateService.isNetworkConnected)
            dialogsManager.showLeaveLobbyAsCreatorDialog(gameName);
        else
            dialogsManager.showCloseAppDialog();
    }

    @Override
    protected boolean createOrJoinLobby() {
        try {
            this.gameName = lobbyRepository.createLobbyBlocking();
            numPlayers = 1;
            return true;
        }
        catch (Exception e) {
            showDisconnectedOverlay();
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
            screensNavigator.toMainMenu(false);
            return false;
        }
    }

    //-----------View Callbacks-----------------------------

    @Override
    public void onAddBotClicked(String intelligence) {
        if (!NetworkStateService.isNetworkConnected)
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
        else if (numPlayers > 5)
            toastMaster.showFailureToast(getString(R.string.toast_max_5_players));
        else{
            try{
                lobbyRepository.addBot(new AddBotModel(gameName, intelligence));
            }
            catch (Exception e) {
                toastMaster.showFailureToast(getString(R.string.toast_need_internet));
            }
        }
    }

    @Override
    public void onStartGameClicked() {
        if (!NetworkStateService.isNetworkConnected)
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
        else if (numPlayers < 3)
            toastMaster.showFailureToast(getString(R.string.toast_need_3_players_to_start));
        else if (numPlayers > 5)
            toastMaster.showFailureToast(getString(R.string.toast_max_5_players));
        else
            try{
                viewMvc.setStartButtonEnabled(false);
                lobbyRepository.startGame(gameName);
            }
            catch (Exception e) {
                toastMaster.showFailureToast(getString(R.string.toast_need_internet));
                viewMvc.setStartButtonEnabled(true);
            }
    }

    @Override
    public void onBotClicked(String name) {
        if (!NetworkStateService.isNetworkConnected)
            toastMaster.showFailureToast(getString(R.string.toast_need_internet));
        else
            try{
                lobbyRepository.removeBot(new RemoveBotModel(gameName, name));
            }
            catch (Exception e) {
                toastMaster.showFailureToast(getString(R.string.toast_need_internet));
            }
    }


}