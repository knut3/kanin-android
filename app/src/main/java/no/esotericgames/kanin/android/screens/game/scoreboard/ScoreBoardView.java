package no.esotericgames.kanin.android.screens.game.scoreboard;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;


import no.esotericgames.kanin.android.screens.common.canvasinterfaces.IDrawable;

public class ScoreBoardView implements IDrawable, IScoreBoard {
    private static final int MAX_NAME_SIZE = 11;
    private static final float PADDING_RATIO = 0.1f;

    private final RectF rect;
    private final float curveSize;
    private final PlayerScoreView roundScoreView, totalScoreView;

    private final String username;
    private final PointF usernamePos;
    private final float distanceBetweenScores;
    private final float distanceFromCenterToCenterBetweenScores;

    private boolean showStroke = false;
    private boolean showDisconnectedIcon;
    private Paint backgroundPaint;
    private Paint namePaint;
    private Paint overlayPaint;
    private Paint strokePaint;
    private final Drawable disconnectedIcon;

    public ScoreBoardView(PointF centerPos, int nameTextSize, String username, boolean isCornerBoard, int roundScore, int totalScore,
                          ScoreViewAppearanceModel roundScoreAppearance, ScoreViewAppearanceModel totalScoreAppearance, Drawable disconnectedIcon) {
        createPaint(nameTextSize);

        float widthOfMaxUsername = namePaint.measureText("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", 0, MAX_NAME_SIZE);
        float widthOfUsername = namePaint.measureText(username);

        float padding = widthOfMaxUsername * PADDING_RATIO;
        float fullWidth = widthOfMaxUsername + (padding * 2);


        float scoreViewHeight = nameTextSize;
        distanceBetweenScores = padding / 2;
        float scoreViewWidth = widthOfMaxUsername / 2 - distanceBetweenScores;
        float fullHeight = padding + nameTextSize + nameTextSize + scoreViewHeight + padding;

        float x;
        float y;

        if (isCornerBoard){
            x = centerPos.x - (fullWidth/2);
            y = centerPos.y  - (fullHeight/4);
        }
        else{
            x = centerPos.x - (fullWidth/2);
            y = centerPos.y - (fullHeight/2);
        }
        float username_x = x + padding + ((widthOfMaxUsername - widthOfUsername) / 2);
        float username_y = y + padding + nameTextSize;

        float roundScoreView_x = x + padding;
        float totalScoreView_x = roundScoreView_x + scoreViewWidth + distanceBetweenScores;
        float scoreView_y = username_y + nameTextSize;

        rect = new RectF(x, y, x+fullWidth, y+fullHeight);
        curveSize = fullHeight / 7;
        this.username = username;
        usernamePos = new PointF(username_x, username_y);
        roundScoreView = new PlayerScoreView(scoreViewWidth, scoreViewHeight, roundScoreAppearance, roundScore);
        roundScoreView.setPosition(roundScoreView_x, scoreView_y);
        totalScoreView = new PlayerScoreView(scoreViewWidth, scoreViewHeight, totalScoreAppearance, totalScore);
        totalScoreView.setPosition(totalScoreView_x, scoreView_y);
        createOverlayPaint();
        distanceFromCenterToCenterBetweenScores = (scoreView_y + scoreViewHeight/2) - centerPos.y;

        this.disconnectedIcon = getDisconnectedIcon(disconnectedIcon, centerPos, fullHeight);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawRoundRect(rect, curveSize, curveSize, backgroundPaint);
        canvas.drawText(username, usernamePos.x, usernamePos.y, namePaint);
        roundScoreView.draw(canvas);
        totalScoreView.draw(canvas);
        canvas.drawRoundRect(rect, curveSize, curveSize, overlayPaint);
        if (showStroke)
            canvas.drawRoundRect(rect, curveSize, curveSize, strokePaint);

        if (showDisconnectedIcon)
            disconnectedIcon.draw(canvas);
    }

    public float getBottom(){
        return rect.bottom;
    }

    public float getHeight(){
        return rect.height();
    }

    @Override
    public void addScore(int score) {
        roundScoreView.addScore(score);
        totalScoreView.addScore(score);
    }

    @Override
    public void addRoundScore(int score) {
        roundScoreView.addScore(score);
    }

    @Override
    public void addTotalScore(int score) {
        totalScoreView.addScore(score);
    }

    @Override
    public int getRoundScore() {
        return roundScoreView.getScore();
    }

    public void showStroke() {
        this.showStroke = true;
    }

    public void hideStroke(){
        this.showStroke = false;
    }

    public void showDisconnectedIcon(){showDisconnectedIcon = true;}
    public void hideDisconnectedIcon(){showDisconnectedIcon = false;}

    public float getDistanceFromCenterToCenterBetweenScores() {
        return distanceFromCenterToCenterBetweenScores;
    }

    public float getTotalScoreIconHorizontalOffsetFromCenter(){
        return  distanceBetweenScores/2 + totalScoreView.getIconCenterOffsetFromLeft();
    }

    public float getRoundScoreIconHorizontalOffsetFromCenter() {
        return roundScoreView.getIconCenterOffsetFromRight() - distanceBetweenScores/2;
    }

    private void createPaint(int nameTextSize){
        backgroundPaint = new Paint();
        backgroundPaint.setStyle(Paint.Style.FILL);
        backgroundPaint.setColor(Color.parseColor("#FFFFFF"));
        float shadowRadius = nameTextSize / 3f;
        backgroundPaint.setShadowLayer(shadowRadius,0, 0, Color.BLACK);
        namePaint = new Paint();
        namePaint.setStyle(Paint.Style.FILL);
        namePaint.setAntiAlias(true);
        namePaint.setTextSize(nameTextSize);
        namePaint.setColor(Color.parseColor("#151a7b"));
        namePaint.setFakeBoldText(true);
        strokePaint = new Paint();
        strokePaint.setColor(Color.parseColor("#f57c00"));
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setAntiAlias(true);
        strokePaint.setStrokeWidth(nameTextSize/4f);
    }

    private void createOverlayPaint(){
        overlayPaint = new Paint();
        overlayPaint.setStyle(Paint.Style.FILL);
        int transparent = Color.TRANSPARENT;
        int shadow = Color.parseColor("#cc5C4033");
        int[] colors = new int[]{transparent, transparent, shadow};
        float[] stops = new float[]{0, 0.4f, 1};
        overlayPaint.setShader(
                new RadialGradient(rect.centerX(), rect.centerY(), rect.height(), colors, stops, Shader.TileMode.CLAMP));
    }

    private Drawable getDisconnectedIcon(Drawable disconnectedIcon, PointF centerOfBoard, float boardHeight) {
        Drawable drawable = disconnectedIcon.getConstantState().newDrawable().mutate();
        int width = (int)boardHeight / 2;
        int halfWidth = width/2;
        drawable.setBounds((int) (centerOfBoard.x-halfWidth), (int) (centerOfBoard.y-halfWidth),
                (int) (centerOfBoard.x+halfWidth), (int) (centerOfBoard.y+halfWidth));
        return drawable;
    }
}
