package no.esotericgames.kanin.android.models;

public class Card {

    public static class Suits{
        public static final String CLUBS = "clubs";
        public static final String HEARTS = "hearts";
        public static final String DIAMONDS = "diamonds";
        public static final String SPADES = "spades";
    }

    public final String suit;
    public int value;

    public Card(String suit, int value){
        this.suit = suit;
        this.value = value;
    }
}
