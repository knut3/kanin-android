package no.esotericgames.kanin.android.screens.common.dialogs;

import androidx.fragment.app.DialogFragment;

import no.esotericgames.kanin.android.common.dependencyinjection.ControllerCompositionRoot;
import no.esotericgames.kanin.android.screens.common.BaseActivity;

public abstract class BaseDialog extends DialogFragment {


    private ControllerCompositionRoot mControllerCompositionRoot;

    protected ControllerCompositionRoot getCompositionRoot() {
        if (mControllerCompositionRoot == null) {
            mControllerCompositionRoot = new ControllerCompositionRoot(
                    ((BaseActivity) requireActivity()).getActivityCompositionRoot()
            );
        }
        return mControllerCompositionRoot;
    }

}
